#!/usr/bin/env bash
TIMEFORMAT="%R"
#!/bin/bash
for file in $4/*.sql; do
    QUERY=$(basename -s .sql "$file")
    echo -en "${QUERY}\t"
    #echo "Running query ${file}"
   (time java -jar /shankar/git/presto/presto-cli/target/presto-cli-330-executable.jar --server $1 --catalog $2 --schema $3 --file $file ) 2<&1 > $5/${QUERY}.out
done

