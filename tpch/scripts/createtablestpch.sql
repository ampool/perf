drop table if exists  region;
drop table if exists  nation; 
drop table if exists  customer;
drop table if exists  part;
drop table if exists  partsupp; 
drop table if exists  supplier;
drop table if exists  orders;
drop table if exists  lineitem;

  
create table region with ("ampool.buckets"=16, "ampool.block.size"=10000, "ampool.block.format"='AMP_COLUMNAR_OFF_HEAP') as select * from tpch.sf2.region;
create table nation with ("ampool.buckets"=16, "ampool.block.size"=10000, "ampool.block.format"='AMP_COLUMNAR_OFF_HEAP') as select * from tpch.sf2.nation;
create table customer with ("ampool.buckets"=16, "ampool.block.size"=10000, "ampool.block.format"='AMP_COLUMNAR_OFF_HEAP') as select * from tpch.sf2.customer;
create table part with ("ampool.buckets"=16, "ampool.block.size"=10000, "ampool.block.format"='AMP_COLUMNAR_OFF_HEAP') as select * from tpch.sf2.part;
create table partsupp with ("ampool.buckets"=16, "ampool.block.size"=10000, "ampool.block.format"='AMP_COLUMNAR_OFF_HEAP') as select * from tpch.sf2.partsupp;
create table supplier with ("ampool.buckets"=16, "ampool.block.size"=10000, "ampool.block.format"='AMP_COLUMNAR_OFF_HEAP') as select * from tpch.sf2.supplier;
create table orders with ("ampool.buckets"=16, "ampool.block.size"=10000, "ampool.block.format"='AMP_COLUMNAR_OFF_HEAP') as select * from tpch.sf2.orders;
create table lineitem with ("ampool.buckets"=16, "ampool.block.size"=10000, "ampool.block.format"='AMP_COLUMNAR_OFF_HEAP') as select * from tpch.sf2.lineitem;

analyze  region;
analyze  nation; 
analyze  customer;
analyze  part;
analyze  partsupp; 
analyze  supplier;
analyze  orders;
analyze  lineitem;
