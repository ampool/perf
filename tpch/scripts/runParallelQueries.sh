#!/usr/bin/env bash
SERVERURL=localhost:9295
CATALOG=tpcds
SCHEMA=sf2
OUTDIR=/shankar/temp
PARALLELRUNS=10
for (( r=1; r<=${PARALLELRUNS}; r++ ))
do  
  echo "Starting Run  $r"
  TIMIMGFILE=$OUTDIR/tpcdssf2.timings.run$r
  RESULTDIR=$OUTDIR/shankar/temp/resultstpcdssf2/run$r
  QUERYDIR=/shankar/git/perf/tpcds/queries/prestotpcds
  rm -rf $RESULTDIR
  mkdir -p $RESULTDIR
  ./runtpcdsqueries.sh $SERVERURL $CATALOG  $SCHEMA $QUERYDIR $RESULTDIR | xargs -r -0 echo > $TIMIMGFILE &&
  sed -i 's/ Q/\nQ/g' $TIMIMGFILE
done


