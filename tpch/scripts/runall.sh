#!/usr/bin/env bash
SERVERURL=localhost:9295
CATALOG=awsglue
SCHEMA=std_tpch_partitioned_orc_2
OUTDIR=/shankar/temp
TIMIMGFILE=$OUTDIR/tpch2glue.timings
RESULTDIR=$OUTDIR/resultstpch2glue
QUERYDIR=/shankar/git/perf/tpch/queries/prestotpch
rm -rf $RESULTDIR
mkdir -p $RESULTDIR
./runtpchqueries.sh $SERVERURL $CATALOG  $SCHEMA $QUERYDIR $RESULTDIR | xargs -r -0 echo > $TIMIMGFILE
sed -i 's/ Q/\nQ/g' $TIMIMGFILE
