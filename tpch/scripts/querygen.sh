#!/usr/bin/env bash
SCALE=$1
QUERYDIR=genqueries/tpchsf$SCALE
mkdir -p $QUERYDIR
for ((i=1;i<=22;i++)); do
  #./qgen -a -v -c -s $SCALE ${i} -r 1231235959 -l reference/subparam_150 -b dists.dss > $QUERYDIR/q${i}.sql
  ./qgen -a -v -c -s $SCALE ${i}  > $QUERYDIR/q${i}.sql
  sed -i -e "s/\r//g" $QUERYDIR/q${i}.sql

done
