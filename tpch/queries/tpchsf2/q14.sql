-- TPC TPC-H Parameter Substitution (Version 2.18.0 build 0)
-- using 1607518940 as a seed to the RNG
-- $ID$
-- TPC-H/TPC-R Promotion Effect Query (Q14)
-- Functional Query Definition
-- Approved February 1998


select
	100.00 * sum(case
		when p_type like 'PROMO%'
			then l_extendedprice * (1 - l_discount)
		else 0
	end) / sum(l_extendedprice * (1 - l_discount)) as promo_revenue
from
	lineitem,
	part
where
	l_partkey = p_partkey
	and l_shipdate >= date '1997-06-01'
	and l_shipdate < date '1997-06-01' + interval '1' month;
--#SET ROWS_FETCH -1
