-- Query 22
SELECT MIN("date_dim"."d_month_seq") AS "temp_none_d_month_",   MAX("date_dim"."d_month_seq") AS "temp_none_d_month1",   COUNT(1) AS "x__alias__0" FROM "date_dim" "date_dim" HAVING (COUNT(1) > 0);

-- Query 22
SELECT AVG(CAST("inventory"."inv_quantity_on_hand" AS DOUBLE)) AS "avg_inv_quantity_o",   COUNT("inventory"."inv_quantity_on_hand") AS "cnt_inv_quantity_o",   "item"."i_brand" AS "i_brand",   "item"."i_category" AS "i_category",   "item"."i_class" AS "i_class",   "item"."i_product_name" AS "i_product_name",   SUM("inventory"."inv_quantity_on_hand") AS "sum_inv_quantity_o" FROM "inventory" "inventory"   INNER JOIN "warehouse" "warehouse" ON ("inventory"."inv_warehouse_sk" = "warehouse"."w_warehouse_sk")   INNER JOIN "item" "item" ON ("inventory"."inv_item_sk" = "item"."i_item_sk")   INNER JOIN "date_dim" "date_dim" ON ("inventory"."inv_date_sk" = "date_dim"."d_date_sk") WHERE (("date_dim"."d_month_seq" >= 1200) AND ("date_dim"."d_month_seq" <= 1211)) GROUP BY 3,   4,   5,   6;

-- Query 22
SELECT AVG(CAST("inventory"."inv_quantity_on_hand" AS DOUBLE)) AS "avg_inv_quantity_o",   "item"."i_product_name" AS "i_product_name" FROM "inventory" "inventory"   INNER JOIN "warehouse" "warehouse" ON ("inventory"."inv_warehouse_sk" = "warehouse"."w_warehouse_sk")   INNER JOIN "item" "item" ON ("inventory"."inv_item_sk" = "item"."i_item_sk")   INNER JOIN "date_dim" "date_dim" ON ("inventory"."inv_date_sk" = "date_dim"."d_date_sk") WHERE (("date_dim"."d_month_seq" >= 1200) AND ("date_dim"."d_month_seq" <= 1211)) GROUP BY 2;

-- Query 22 Date Calculation
SELECT AVG(CAST("inventory"."inv_quantity_on_hand" AS DOUBLE)) AS "avg_inv_quantity_o",   COUNT("inventory"."inv_quantity_on_hand") AS "cnt_inv_quantity_o",   "item"."i_brand" AS "i_brand",   "item"."i_category" AS "i_category",   "item"."i_class" AS "i_class",   "item"."i_product_name" AS "i_product_name",   SUM("inventory"."inv_quantity_on_hand") AS "sum_inv_quantity_o" FROM "inventory" "inventory"   INNER JOIN "warehouse" "warehouse" ON ("inventory"."inv_warehouse_sk" = "warehouse"."w_warehouse_sk")   INNER JOIN "item" "item" ON ("inventory"."inv_item_sk" = "item"."i_item_sk")   INNER JOIN "date_dim" "date_dim" ON ("inventory"."inv_date_sk" = "date_dim"."d_date_sk") WHERE ((DATE_TRUNC('month', CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) >= CAST('2000-01-01 00:00:00' AS TIMESTAMP)) AND (DATE_TRUNC('month', CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) < CAST('2001-01-01 00:00:00' AS TIMESTAMP))) GROUP BY 3,   4,   5,   6;

-- Query 22 Date Calculation
SELECT AVG(CAST("inventory"."inv_quantity_on_hand" AS DOUBLE)) AS "avg_inv_quantity_o",   "item"."i_product_name" AS "i_product_name" FROM "inventory" "inventory"   INNER JOIN "warehouse" "warehouse" ON ("inventory"."inv_warehouse_sk" = "warehouse"."w_warehouse_sk")   INNER JOIN "item" "item" ON ("inventory"."inv_item_sk" = "item"."i_item_sk")   INNER JOIN "date_dim" "date_dim" ON ("inventory"."inv_date_sk" = "date_dim"."d_date_sk") WHERE ((DATE_TRUNC('month', CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) >= CAST('2000-01-01 00:00:00' AS TIMESTAMP)) AND (DATE_TRUNC('month', CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) < CAST('2001-01-01 00:00:00' AS TIMESTAMP))) GROUP BY 2;






