-- Query 98
SELECT MAX("item"."i_current_price") AS "temp_attr_i_curren",
  MIN("item"."i_current_price") AS "temp_attr_i_curre1",
  "item"."i_category" AS "i_category",
  "item"."i_class" AS "i_class",
  "item"."i_item_desc" AS "i_item_desc",
  "item"."i_item_id" AS "i_item_id",
  SUM("store_sales"."ss_ext_sales_price") AS "sum_ss_ext_sales_p"
FROM "store_sales" "store_sales"
  INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")
  INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")
  LEFT JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")
WHERE (("item"."i_category" IN ('Books                                             ', 'Home                                              ', 'Sports                                            ')) AND ("date_dim"."d_date" >= CAST('1999-02-22 00:00:00' AS TIMESTAMP)) AND ("date_dim"."d_date" < CAST('1999-03-25 00:00:00' AS TIMESTAMP)))
GROUP BY 3,
  4,
  5,
  6;


-- Query 98 Date Calculation  
SELECT MAX("item"."i_current_price") AS "temp_attr_i_curren",
  MIN("item"."i_current_price") AS "temp_attr_i_curre1",
  "item"."i_category" AS "i_category",
  "item"."i_class" AS "i_class",
  "item"."i_item_desc" AS "i_item_desc",
  "item"."i_item_id" AS "i_item_id",
  SUM("store_sales"."ss_ext_sales_price") AS "sum_ss_ext_sales_p"
FROM "store_sales" "store_sales"
  INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")
  INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")
  LEFT JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")
WHERE (("item"."i_category" IN ('Books                                             ', 'Home                                              ', 'Sports                                            ')) AND (CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1999-02-22 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('1999-03-25 00:00:00' AS TIMESTAMP)))
GROUP BY 3,
  4,
  5,
  6;
