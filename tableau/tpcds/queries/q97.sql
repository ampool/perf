-- Query 97
SELECT "t3"."x_measure__3" AS "sum_calculation_10",
  "t0"."sum_calculation_12" AS "sum_calculation_12",
  "t6"."x_measure__7" AS "sum_store_only_cus"
FROM (
  SELECT SUM((CASE WHEN (("catalog sold date"."d_month_seq" >= 1200) AND ("catalog sold date"."d_month_seq" <= 1211) AND ("store sold date"."d_month_seq" >= 1200) AND ("store sold date"."d_month_seq" <= 1211)) THEN 1 ELSE CAST(NULL AS BIGINT) END)) AS "sum_calculation_12",
    COUNT(1) AS "x__alias__0"
  FROM "store_sales" "store_sales"
    LEFT JOIN "date_dim" "store sold date" ON ("store_sales"."ss_sold_date_sk" = "store sold date"."d_date_sk")
    FULL JOIN "catalog_sales" "catalog_sales" ON (("store_sales"."ss_customer_sk" = "catalog_sales"."cs_bill_customer_sk") AND ("store_sales"."ss_item_sk" = "catalog_sales"."cs_item_sk"))
    LEFT JOIN "date_dim" "catalog sold date" ON ("catalog_sales"."cs_sold_date_sk" = "catalog sold date"."d_date_sk")
  HAVING (COUNT(1) > 0)
) "t0"
  INNER JOIN (
  SELECT SUM("t2"."x_measure__2") AS "x_measure__3",
    COUNT(1) AS "x__alias__1"
  FROM (
    SELECT "store_sales"."ss_customer_sk" AS "ss_customer_sk",
      "store_sales"."ss_item_sk" AS "ss_item_sk"
    FROM "store_sales" "store_sales"
      FULL JOIN "catalog_sales" "catalog_sales" ON (("store_sales"."ss_customer_sk" = "catalog_sales"."cs_bill_customer_sk") AND ("store_sales"."ss_item_sk" = "catalog_sales"."cs_item_sk"))
    GROUP BY 1,
      2
  ) "t1"
    INNER JOIN (
    SELECT "store_sales"."ss_customer_sk" AS "ss_customer_sk",
      "store_sales"."ss_item_sk" AS "ss_item_sk",
      MIN((CASE WHEN ((("catalog sold date"."d_month_seq" < 1200) OR ("catalog sold date"."d_month_seq" > 1211) OR ("catalog sold date"."d_month_seq" IS NULL)) AND ("store sold date"."d_month_seq" >= 1200) AND ("store sold date"."d_month_seq" <= 1211) AND (NOT ("store_sales"."ss_customer_sk" IS NULL))) THEN 1 ELSE CAST(NULL AS BIGINT) END)) AS "x_measure__2"
    FROM "store_sales" "store_sales"
      LEFT JOIN "date_dim" "store sold date" ON ("store_sales"."ss_sold_date_sk" = "store sold date"."d_date_sk")
      FULL JOIN "catalog_sales" "catalog_sales" ON (("store_sales"."ss_customer_sk" = "catalog_sales"."cs_bill_customer_sk") AND ("store_sales"."ss_item_sk" = "catalog_sales"."cs_item_sk"))
      LEFT JOIN "date_dim" "catalog sold date" ON ("catalog_sales"."cs_sold_date_sk" = "catalog sold date"."d_date_sk")
    GROUP BY 1,
      2
  ) "t2" ON (((COALESCE("t1"."ss_customer_sk", 0) = COALESCE("t2"."ss_customer_sk", 0)) AND (COALESCE("t1"."ss_customer_sk", 1) = COALESCE("t2"."ss_customer_sk", 1))) AND ((COALESCE("t1"."ss_item_sk", 0) = COALESCE("t2"."ss_item_sk", 0)) AND (COALESCE("t1"."ss_item_sk", 1) = COALESCE("t2"."ss_item_sk", 1))))
  HAVING (COUNT(1) > 0)
) "t3" ON 1=1
  INNER JOIN (
  SELECT SUM("t5"."x_measure__6") AS "x_measure__7",
    COUNT(1) AS "x__alias__2"
  FROM (
    SELECT "catalog_sales"."cs_bill_customer_sk" AS "cs_bill_customer_s",
      "catalog_sales"."cs_item_sk" AS "cs_item_sk"
    FROM "store_sales" "store_sales"
      FULL JOIN "catalog_sales" "catalog_sales" ON (("store_sales"."ss_customer_sk" = "catalog_sales"."cs_bill_customer_sk") AND ("store_sales"."ss_item_sk" = "catalog_sales"."cs_item_sk"))
    GROUP BY 1,
      2
  ) "t4"
    INNER JOIN (
    SELECT "catalog_sales"."cs_bill_customer_sk" AS "cs_bill_customer_s",
      "catalog_sales"."cs_item_sk" AS "cs_item_sk",
      MIN((CASE WHEN ((("store sold date"."d_month_seq" < 1200) OR ("store sold date"."d_month_seq" > 1211) OR ("store sold date"."d_month_seq" IS NULL)) AND ("catalog sold date"."d_month_seq" >= 1200) AND ("catalog sold date"."d_month_seq" <= 1211) AND (NOT ("catalog_sales"."cs_bill_customer_sk" IS NULL))) THEN 1 ELSE CAST(NULL AS BIGINT) END)) AS "x_measure__6"
    FROM "store_sales" "store_sales"
      LEFT JOIN "date_dim" "store sold date" ON ("store_sales"."ss_sold_date_sk" = "store sold date"."d_date_sk")
      FULL JOIN "catalog_sales" "catalog_sales" ON (("store_sales"."ss_customer_sk" = "catalog_sales"."cs_bill_customer_sk") AND ("store_sales"."ss_item_sk" = "catalog_sales"."cs_item_sk"))
      LEFT JOIN "date_dim" "catalog sold date" ON ("catalog_sales"."cs_sold_date_sk" = "catalog sold date"."d_date_sk")
    GROUP BY 1,
      2
  ) "t5" ON (((COALESCE("t4"."cs_bill_customer_s", 0) = COALESCE("t5"."cs_bill_customer_s", 0)) AND (COALESCE("t4"."cs_bill_customer_s", 1) = COALESCE("t5"."cs_bill_customer_s", 1))) AND ((COALESCE("t4"."cs_item_sk", 0) = COALESCE("t5"."cs_item_sk", 0)) AND (COALESCE("t4"."cs_item_sk", 1) = COALESCE("t5"."cs_item_sk", 1))))
  HAVING (COUNT(1) > 0)
) "t6" ON 1=1;
