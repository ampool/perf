-- Query 13
SELECT AVG("t0"."ss_ext_sales_price") AS "avg_ss_ext_sales_p",   AVG("t0"."ss_ext_wholesale_c") AS "avg_ss_ext_wholesa",   AVG(CAST("t0"."ss_quantity" AS DOUBLE)) AS "avg_ss_quantity_ok",   SUM("t0"."ss_ext_wholesale_c") AS "sum_ss_ext_wholesa",   COUNT(1) AS "x__alias__0" FROM "item" "item"   INNER JOIN (   SELECT "store_sales"."ss_item_sk" AS "ss_item_sk",     "bought address"."ca_country" AS "ca_country",     "bought address"."ca_state" AS "ca_state",     "customer_demographics"."cd_education_status" AS "cd_education_statu",     "customer_demographics"."cd_marital_status" AS "cd_marital_status",     "household_demographics"."hd_dep_count" AS "hd_dep_count",     "store_sales"."ss_ext_sales_price" AS "ss_ext_sales_price",     "store_sales"."ss_ext_wholesale_cost" AS "ss_ext_wholesale_c",     "store_sales"."ss_net_profit" AS "ss_net_profit",     "store_sales"."ss_promo_sk" AS "ss_promo_sk",     "store_sales"."ss_quantity" AS "ss_quantity",     "store_sales"."ss_sales_price" AS "ss_sales_price",     "store_sales"."ss_sold_date_sk" AS "ss_sold_date_sk",     "store_sales"."ss_sold_time_sk" AS "ss_sold_time_sk"   FROM "store_sales" "store_sales"     INNER JOIN "customer_address" "bought address" ON ("store_sales"."ss_addr_sk" = "bought address"."ca_address_sk")     LEFT JOIN "customer_demographics" "customer_demographics" ON ("store_sales"."ss_cdemo_sk" = "customer_demographics"."cd_demo_sk")     LEFT JOIN "household_demographics" "household_demographics" ON ("store_sales"."ss_hdemo_sk" = "household_demographics"."hd_demo_sk")     LEFT JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")     LEFT JOIN "customer" "customer" ON ("store_sales"."ss_customer_sk" = "customer"."c_customer_sk")     LEFT JOIN "customer_address" "current address" ON ("customer"."c_current_addr_sk" = "current address"."ca_address_sk") ) "t0" ON ("item"."i_item_sk" = "t0"."ss_item_sk")   LEFT JOIN "promotion" "promotion" ON ("t0"."ss_promo_sk" = "promotion"."p_promo_sk")   LEFT JOIN "time_dim" "time_dim" ON ("t0"."ss_sold_time_sk" = "time_dim"."t_time_sk")   INNER JOIN "date_dim" "date_dim" ON ("t0"."ss_sold_date_sk" = "date_dim"."d_date_sk") WHERE ((((FLOOR("t0"."ss_net_profit" / 50) = 1) AND ("t0"."ca_state" IN ('MS', 'TX', 'VA'))) OR ((FLOOR("t0"."ss_net_profit" / 50) = 2) AND ("t0"."ca_state" IN ('MS', 'OH', 'TX', 'VA'))) OR ((FLOOR("t0"."ss_net_profit" / 50) = 3) AND ("t0"."ca_state" IN ('KY', 'MS', 'NM', 'OH', 'OR', 'TX', 'VA'))) OR ((FLOOR("t0"."ss_net_profit" / 50) = 4) AND ("t0"."ca_state" IN ('KY', 'MS', 'NM', 'OR', 'TX', 'VA'))) OR ((FLOOR("t0"."ss_net_profit" / 50) = 5) AND ("t0"."ca_state" IN ('KY', 'NM', 'OR')))) AND (((FLOOR("t0"."ss_sales_price" / 50) = 1) AND ("t0"."cd_education_statu" = 'College             ') AND ("t0"."cd_marital_status" = 'S') AND ("t0"."hd_dep_count" = 1)) OR ((FLOOR("t0"."ss_sales_price" / 50) = 2) AND ("t0"."cd_education_statu" = 'Advanced Degree     ') AND ("t0"."cd_marital_status" = 'M') AND ("t0"."hd_dep_count" = 3)) OR ((FLOOR("t0"."ss_sales_price" / 50) = 3) AND ("t0"."cd_education_statu" = '2 yr Degree         ') AND ("t0"."cd_marital_status" = 'W') AND ("t0"."hd_dep_count" = 1))) AND ("t0"."ca_country" = 'United States') AND ("date_dim"."d_year" = 2001)) HAVING (COUNT(1) > 0);

-- Query 13
SELECT FLOOR("store_sales"."ss_sales_price" / 50) AS "ss_sales_price__bi",   "customer_demographics"."cd_education_status" AS "cd_education_statu",   "customer_demographics"."cd_marital_status" AS "cd_marital_status",   "household_demographics"."hd_dep_count" AS "hd_dep_count" FROM "store_sales" "store_sales"   LEFT JOIN "customer_demographics" "customer_demographics" ON ("store_sales"."ss_cdemo_sk" = "customer_demographics"."cd_demo_sk")   LEFT JOIN "household_demographics" "household_demographics" ON ("store_sales"."ss_hdemo_sk" = "household_demographics"."hd_demo_sk")   RIGHT JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk") WHERE (((FLOOR("store_sales"."ss_sales_price" / 50) = 1) AND ("customer_demographics"."cd_education_status" = 'College             ') AND ("customer_demographics"."cd_marital_status" = 'S') AND ("household_demographics"."hd_dep_count" = 1)) OR ((FLOOR("store_sales"."ss_sales_price" / 50) = 2) AND ("customer_demographics"."cd_education_status" = 'Advanced Degree     ') AND ("customer_demographics"."cd_marital_status" = 'M') AND ("household_demographics"."hd_dep_count" = 3)) OR ((FLOOR("store_sales"."ss_sales_price" / 50) = 3) AND ("customer_demographics"."cd_education_status" = '2 yr Degree         ') AND ("customer_demographics"."cd_marital_status" = 'W') AND ("household_demographics"."hd_dep_count" = 1))) GROUP BY 1,   2,   3,   4 ORDER BY "cd_marital_status" ASC,   "cd_education_statu" ASC,   "ss_sales_price__bi" ASC,   "hd_dep_count" ASC;

-- Query 13
SELECT FLOOR("store_sales"."ss_net_profit" / 50) AS "ss_net_profit__bin",   "bought address"."ca_state" AS "ca_state" FROM "store_sales" "store_sales"   LEFT JOIN "customer_address" "bought address" ON ("store_sales"."ss_addr_sk" = "bought address"."ca_address_sk")   RIGHT JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk") WHERE (((FLOOR("store_sales"."ss_net_profit" / 50) = 1) AND ("bought address"."ca_state" IN ('MS', 'TX', 'VA'))) OR ((FLOOR("store_sales"."ss_net_profit" / 50) = 2) AND ("bought address"."ca_state" IN ('MS', 'OH', 'TX', 'VA'))) OR ((FLOOR("store_sales"."ss_net_profit" / 50) = 3) AND ("bought address"."ca_state" IN ('KY', 'MS', 'NM', 'OH', 'OR', 'TX', 'VA'))) OR ((FLOOR("store_sales"."ss_net_profit" / 50) = 4) AND ("bought address"."ca_state" IN ('KY', 'MS', 'NM', 'OR', 'TX', 'VA'))) OR ((FLOOR("store_sales"."ss_net_profit" / 50) = 5) AND ("bought address"."ca_state" IN ('KY', 'NM', 'OR')))) GROUP BY 1,   2 ORDER BY "ca_state" ASC,   "ss_net_profit__bin" ASC;


--Query 13 Date Calculation
SELECT AVG("t0"."ss_ext_sales_price") AS "avg_ss_ext_sales_p",
  AVG("t0"."ss_ext_wholesale_c") AS "avg_ss_ext_wholesa",
  AVG(CAST("t0"."ss_quantity" AS DOUBLE)) AS "avg_ss_quantity_ok",
  SUM("t0"."ss_ext_wholesale_c") AS "sum_ss_ext_wholesa",
  COUNT(1) AS "x__alias__0"
FROM "item" "item"
  INNER JOIN (
  SELECT "store_sales"."ss_item_sk" AS "ss_item_sk",
    "bought address"."ca_country" AS "ca_country",
    "bought address"."ca_state" AS "ca_state",
    "customer_demographics"."cd_education_status" AS "cd_education_statu",
    "customer_demographics"."cd_marital_status" AS "cd_marital_status",
    "household_demographics"."hd_dep_count" AS "hd_dep_count",
    "store_sales"."ss_ext_sales_price" AS "ss_ext_sales_price",
    "store_sales"."ss_ext_wholesale_cost" AS "ss_ext_wholesale_c",
    "store_sales"."ss_net_profit" AS "ss_net_profit",
    "store_sales"."ss_promo_sk" AS "ss_promo_sk",
    "store_sales"."ss_quantity" AS "ss_quantity",
    "store_sales"."ss_sales_price" AS "ss_sales_price",
    "store_sales"."ss_sold_date_sk" AS "ss_sold_date_sk",
    "store_sales"."ss_sold_time_sk" AS "ss_sold_time_sk"
  FROM "store_sales" "store_sales"
    INNER JOIN "customer_address" "bought address" ON ("store_sales"."ss_addr_sk" = "bought address"."ca_address_sk")
    LEFT JOIN "customer_demographics" "customer_demographics" ON ("store_sales"."ss_cdemo_sk" = "customer_demographics"."cd_demo_sk")
    LEFT JOIN "household_demographics" "household_demographics" ON ("store_sales"."ss_hdemo_sk" = "household_demographics"."hd_demo_sk")
    LEFT JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")
    LEFT JOIN "customer" "customer" ON ("store_sales"."ss_customer_sk" = "customer"."c_customer_sk")
    LEFT JOIN "customer_address" "current address" ON ("customer"."c_current_addr_sk" = "current address"."ca_address_sk")
) "t0" ON ("item"."i_item_sk" = "t0"."ss_item_sk")
  LEFT JOIN "promotion" "promotion" ON ("t0"."ss_promo_sk" = "promotion"."p_promo_sk")
  LEFT JOIN "time_dim" "time_dim" ON ("t0"."ss_sold_time_sk" = "time_dim"."t_time_sk")
  LEFT JOIN "date_dim" "date_dim" ON ("t0"."ss_sold_date_sk" = "date_dim"."d_date_sk")
WHERE ((((FLOOR("t0"."ss_net_profit" / 50) = 1) AND ("t0"."ca_state" IN ('MS', 'TX', 'VA'))) OR ((FLOOR("t0"."ss_net_profit" / 50) = 2) AND ("t0"."ca_state" IN ('MS', 'OH', 'TX', 'VA'))) OR ((FLOOR("t0"."ss_net_profit" / 50) = 3) AND ("t0"."ca_state" IN ('KY', 'MS', 'NM', 'OH', 'OR', 'TX', 'VA'))) OR ((FLOOR("t0"."ss_net_profit" / 50) = 4) AND ("t0"."ca_state" IN ('KY', 'MS', 'NM', 'OR', 'TX', 'VA'))) OR ((FLOOR("t0"."ss_net_profit" / 50) = 5) AND ("t0"."ca_state" IN ('KY', 'NM', 'OR')))) AND (((FLOOR("t0"."ss_sales_price" / 50) = 1) AND ("t0"."cd_education_statu" = 'College             ') AND ("t0"."cd_marital_status" = 'S') AND ("t0"."hd_dep_count" = 1)) OR ((FLOOR("t0"."ss_sales_price" / 50) = 2) AND ("t0"."cd_education_statu" = 'Advanced Degree     ') AND ("t0"."cd_marital_status" = 'M') AND ("t0"."hd_dep_count" = 3)) OR ((FLOOR("t0"."ss_sales_price" / 50) = 3) AND ("t0"."cd_education_statu" = '2 yr Degree         ') AND ("t0"."cd_marital_status" = 'W') AND ("t0"."hd_dep_count" = 1))) AND ("t0"."ca_country" = 'United States') AND (YEAR(CAST(DATE_ADD('DAY', CAST(("t0"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) = 2001))
HAVING (COUNT(1) > 0);

-- Query 13 Status Price and Dep Set
SELECT FLOOR("t0"."ss_sales_price" / 50) AS "ss_sales_price__bi",
  "t0"."cd_education_statu" AS "cd_education_statu",
  "t0"."cd_marital_status" AS "cd_marital_status",
  "t0"."hd_dep_count" AS "hd_dep_count",
  SUM(1) AS "sum_number_of_reco"
FROM "item" "item"
  INNER JOIN (
  SELECT "store_sales"."ss_item_sk" AS "ss_item_sk",
    "customer_demographics"."cd_education_status" AS "cd_education_statu",
    "customer_demographics"."cd_marital_status" AS "cd_marital_status",
    "household_demographics"."hd_dep_count" AS "hd_dep_count",
    "store_sales"."ss_promo_sk" AS "ss_promo_sk",
    "store_sales"."ss_sales_price" AS "ss_sales_price",
    "store_sales"."ss_sold_date_sk" AS "ss_sold_date_sk",
    "store_sales"."ss_sold_time_sk" AS "ss_sold_time_sk"
  FROM "store_sales" "store_sales"
    LEFT JOIN "customer_address" "bought address" ON ("store_sales"."ss_addr_sk" = "bought address"."ca_address_sk")
    INNER JOIN "customer_demographics" "customer_demographics" ON ("store_sales"."ss_cdemo_sk" = "customer_demographics"."cd_demo_sk")
    INNER JOIN "household_demographics" "household_demographics" ON ("store_sales"."ss_hdemo_sk" = "household_demographics"."hd_demo_sk")
    LEFT JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")
    LEFT JOIN "customer" "customer" ON ("store_sales"."ss_customer_sk" = "customer"."c_customer_sk")
    LEFT JOIN "customer_address" "current address" ON ("customer"."c_current_addr_sk" = "current address"."ca_address_sk")
) "t0" ON ("item"."i_item_sk" = "t0"."ss_item_sk")
  LEFT JOIN "promotion" "promotion" ON ("t0"."ss_promo_sk" = "promotion"."p_promo_sk")
  LEFT JOIN "time_dim" "time_dim" ON ("t0"."ss_sold_time_sk" = "time_dim"."t_time_sk")
  LEFT JOIN "date_dim" "date_dim" ON ("t0"."ss_sold_date_sk" = "date_dim"."d_date_sk")
WHERE ((NOT (FLOOR("t0"."ss_sales_price" / 50) IS NULL)) AND ((NOT ("t0"."cd_education_statu" IN ('4 yr Degree', 'Primary', 'Secondary', 'Unknown'))) OR ("t0"."cd_education_statu" IS NULL)) AND (NOT (("t0"."cd_marital_status" IN ('D', 'U')) OR ("t0"."cd_marital_status" IS NULL))) AND ("t0"."hd_dep_count" IN (1, 3)))
GROUP BY 1,
  2,
  3,
  4;
  
  
-- Query 13 State and Profit Set
SELECT FLOOR("t0"."ss_net_profit" / 50) AS "ss_net_profit__bin",
  "t0"."ca_state" AS "ca_state",
  (NOT ("t2"."xtemp1_output" IS NULL)) AS "io_bought_state_an",
  SUM(1) AS "sum_number_of_reco"
FROM "item" "item"
  INNER JOIN (
  SELECT "store_sales"."ss_item_sk" AS "ss_item_sk",
    "bought address"."ca_state" AS "ca_state",
    "store_sales"."ss_net_profit" AS "ss_net_profit",
    "store_sales"."ss_promo_sk" AS "ss_promo_sk",
    "store_sales"."ss_sold_date_sk" AS "ss_sold_date_sk",
    "store_sales"."ss_sold_time_sk" AS "ss_sold_time_sk"
  FROM "store_sales" "store_sales"
    INNER JOIN "customer_address" "bought address" ON ("store_sales"."ss_addr_sk" = "bought address"."ca_address_sk")
    LEFT JOIN "customer_demographics" "customer_demographics" ON ("store_sales"."ss_cdemo_sk" = "customer_demographics"."cd_demo_sk")
    LEFT JOIN "household_demographics" "household_demographics" ON ("store_sales"."ss_hdemo_sk" = "household_demographics"."hd_demo_sk")
    LEFT JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")
    LEFT JOIN "customer" "customer" ON ("store_sales"."ss_customer_sk" = "customer"."c_customer_sk")
    LEFT JOIN "customer_address" "current address" ON ("customer"."c_current_addr_sk" = "current address"."ca_address_sk")
) "t0" ON ("item"."i_item_sk" = "t0"."ss_item_sk")
  LEFT JOIN "promotion" "promotion" ON ("t0"."ss_promo_sk" = "promotion"."p_promo_sk")
  LEFT JOIN "time_dim" "time_dim" ON ("t0"."ss_sold_time_sk" = "time_dim"."t_time_sk")
  LEFT JOIN "date_dim" "date_dim" ON ("t0"."ss_sold_date_sk" = "date_dim"."d_date_sk")
  LEFT JOIN (
  SELECT FLOOR("t1"."ss_net_profit" / 50) AS "ss_net_profit__bin",
    "t1"."ca_state" AS "ca_state",
    1 AS "xtemp1_output"
  FROM "item" "item"
    LEFT JOIN (
    SELECT "store_sales"."ss_item_sk" AS "ss_item_sk",
      "bought address"."ca_state" AS "ca_state",
      "store_sales"."ss_net_profit" AS "ss_net_profit"
    FROM "store_sales" "store_sales"
      LEFT JOIN "customer_address" "bought address" ON ("store_sales"."ss_addr_sk" = "bought address"."ca_address_sk")
  ) "t1" ON ("item"."i_item_sk" = "t1"."ss_item_sk")
  WHERE (((FLOOR("t1"."ss_net_profit" / 50) = 1) AND ("t1"."ca_state" IN ('MS', 'TX', 'VA'))) OR ((FLOOR("t1"."ss_net_profit" / 50) = 2) AND ("t1"."ca_state" IN ('MS', 'OH', 'TX', 'VA'))) OR ((FLOOR("t1"."ss_net_profit" / 50) = 3) AND ("t1"."ca_state" IN ('KY', 'MS', 'NM', 'OH', 'OR', 'TX', 'VA'))) OR ((FLOOR("t1"."ss_net_profit" / 50) = 4) AND ("t1"."ca_state" IN ('KY', 'MS', 'NM', 'OR', 'TX', 'VA'))) OR ((FLOOR("t1"."ss_net_profit" / 50) = 5) AND ("t1"."ca_state" IN ('KY', 'NM', 'OR'))))
  GROUP BY 1,
    2
) "t2" ON (((COALESCE(FLOOR("t0"."ss_net_profit" / 50), 0) = COALESCE("t2"."ss_net_profit__bin", 0)) AND (COALESCE(FLOOR("t0"."ss_net_profit" / 50), 1) = COALESCE("t2"."ss_net_profit__bin", 1))) AND ((COALESCE("t0"."ca_state", '0') = COALESCE("t2"."ca_state", '0')) AND (COALESCE("t0"."ca_state", '1') = COALESCE("t2"."ca_state", '1'))))
WHERE ((FLOOR("t0"."ss_net_profit" / 50) IN (1, 2, 3, 4, 5)) AND ("t0"."ca_state" IN ('KY', 'MS', 'NM', 'OH', 'OR', 'TX', 'VA')))
GROUP BY 1,
  2,
  3;


