-- Query 28
SELECT (CASE WHEN (("t0"."ss_quantity" >= 0) AND ("t0"."ss_quantity" <= 5) AND ((("t0"."ss_list_price" >= 8) AND ("t0"."ss_list_price" <= 18)) OR (("t0"."ss_coupon_amt" >= 459) AND ("t0"."ss_coupon_amt" <= 1459)) OR (("t0"."ss_wholesale_cost" >= 57) AND ("t0"."ss_wholesale_cost" <= 77)))) THEN 'B1' WHEN (("t0"."ss_quantity" >= 6) AND ("t0"."ss_quantity" <= 10) AND ((("t0"."ss_list_price" >= 90) AND ("t0"."ss_list_price" <= 100)) OR (("t0"."ss_coupon_amt" >= 2323) AND ("t0"."ss_coupon_amt" <= 3323)) OR (("t0"."ss_wholesale_cost" >= 31) AND ("t0"."ss_wholesale_cost" <= 51)))) THEN 'B2' WHEN (("t0"."ss_quantity" >= 11) AND ("t0"."ss_quantity" <= 15) AND ((("t0"."ss_list_price" >= 142) AND ("t0"."ss_list_price" <= 152)) OR (("t0"."ss_coupon_amt" >= 12214) AND ("t0"."ss_coupon_amt" <= 13214)) OR (("t0"."ss_wholesale_cost" >= 79) AND ("t0"."ss_wholesale_cost" <= 99)))) THEN 'B3' WHEN (("t0"."ss_quantity" >= 16) AND ("t0"."ss_quantity" <= 20) AND ((("t0"."ss_list_price" >= 135) AND ("t0"."ss_list_price" <= 145)) OR (("t0"."ss_coupon_amt" >= 6071) AND ("t0"."ss_coupon_amt" <= 7071)) OR (("t0"."ss_wholesale_cost" >= 38) AND ("t0"."ss_wholesale_cost" <= 58)))) THEN 'B4' WHEN (("t0"."ss_quantity" >= 21) AND ("t0"."ss_quantity" <= 25) AND ((("t0"."ss_list_price" >= 122) AND ("t0"."ss_list_price" <= 132)) OR (("t0"."ss_coupon_amt" >= 836) AND ("t0"."ss_coupon_amt" <= 1836)) OR (("t0"."ss_wholesale_cost" >= 17) AND ("t0"."ss_wholesale_cost" <= 37)))) THEN 'B5' WHEN (("t0"."ss_quantity" >= 26) AND ("t0"."ss_quantity" <= 30) AND ((("t0"."ss_list_price" >= 154) AND ("t0"."ss_list_price" <= 164)) OR (("t0"."ss_coupon_amt" >= 7326) AND ("t0"."ss_coupon_amt" <= 8326)) OR (("t0"."ss_wholesale_cost" >= 7) AND ("t0"."ss_wholesale_cost" <= 27)))) THEN 'B6' ELSE CAST(NULL AS VARCHAR) END) AS "calculation_930100",
  AVG("t0"."ss_list_price") AS "avg_ss_list_price_",
  COUNT("t0"."ss_list_price") AS "cnt_ss_list_price_",
  COUNT(DISTINCT "t0"."ss_list_price") AS "ctd_ss_list_price_"
FROM "item" "item"
  LEFT JOIN (
  SELECT "store_sales"."ss_item_sk" AS "ss_item_sk",
    "store_sales"."ss_coupon_amt" AS "ss_coupon_amt",
    "store_sales"."ss_list_price" AS "ss_list_price",
    "store_sales"."ss_promo_sk" AS "ss_promo_sk",
    "store_sales"."ss_quantity" AS "ss_quantity",
    "store_sales"."ss_sold_date_sk" AS "ss_sold_date_sk",
    "store_sales"."ss_sold_time_sk" AS "ss_sold_time_sk",
    "store_sales"."ss_wholesale_cost" AS "ss_wholesale_cost"
  FROM "store_sales" "store_sales"
    LEFT JOIN "customer_address" "bought address" ON ("store_sales"."ss_addr_sk" = "bought address"."ca_address_sk")
    LEFT JOIN "customer_demographics" "customer_demographics" ON ("store_sales"."ss_cdemo_sk" = "customer_demographics"."cd_demo_sk")
    LEFT JOIN "household_demographics" "household_demographics" ON ("store_sales"."ss_hdemo_sk" = "household_demographics"."hd_demo_sk")
    LEFT JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")
    LEFT JOIN "customer" "customer" ON ("store_sales"."ss_customer_sk" = "customer"."c_customer_sk")
    LEFT JOIN "customer_address" "current address" ON ("customer"."c_current_addr_sk" = "current address"."ca_address_sk")
) "t0" ON ("item"."i_item_sk" = "t0"."ss_item_sk")
  LEFT JOIN "promotion" "promotion" ON ("t0"."ss_promo_sk" = "promotion"."p_promo_sk")
  LEFT JOIN "time_dim" "time_dim" ON ("t0"."ss_sold_time_sk" = "time_dim"."t_time_sk")
  LEFT JOIN "date_dim" "date_dim" ON ("t0"."ss_sold_date_sk" = "date_dim"."d_date_sk")
WHERE (NOT ((CASE WHEN (("t0"."ss_quantity" >= 0) AND ("t0"."ss_quantity" <= 5) AND ((("t0"."ss_list_price" >= 8) AND ("t0"."ss_list_price" <= 18)) OR (("t0"."ss_coupon_amt" >= 459) AND ("t0"."ss_coupon_amt" <= 1459)) OR (("t0"."ss_wholesale_cost" >= 57) AND ("t0"."ss_wholesale_cost" <= 77)))) THEN 'B1' WHEN (("t0"."ss_quantity" >= 6) AND ("t0"."ss_quantity" <= 10) AND ((("t0"."ss_list_price" >= 90) AND ("t0"."ss_list_price" <= 100)) OR (("t0"."ss_coupon_amt" >= 2323) AND ("t0"."ss_coupon_amt" <= 3323)) OR (("t0"."ss_wholesale_cost" >= 31) AND ("t0"."ss_wholesale_cost" <= 51)))) THEN 'B2' WHEN (("t0"."ss_quantity" >= 11) AND ("t0"."ss_quantity" <= 15) AND ((("t0"."ss_list_price" >= 142) AND ("t0"."ss_list_price" <= 152)) OR (("t0"."ss_coupon_amt" >= 12214) AND ("t0"."ss_coupon_amt" <= 13214)) OR (("t0"."ss_wholesale_cost" >= 79) AND ("t0"."ss_wholesale_cost" <= 99)))) THEN 'B3' WHEN (("t0"."ss_quantity" >= 16) AND ("t0"."ss_quantity" <= 20) AND ((("t0"."ss_list_price" >= 135) AND ("t0"."ss_list_price" <= 145)) OR (("t0"."ss_coupon_amt" >= 6071) AND ("t0"."ss_coupon_amt" <= 7071)) OR (("t0"."ss_wholesale_cost" >= 38) AND ("t0"."ss_wholesale_cost" <= 58)))) THEN 'B4' WHEN (("t0"."ss_quantity" >= 21) AND ("t0"."ss_quantity" <= 25) AND ((("t0"."ss_list_price" >= 122) AND ("t0"."ss_list_price" <= 132)) OR (("t0"."ss_coupon_amt" >= 836) AND ("t0"."ss_coupon_amt" <= 1836)) OR (("t0"."ss_wholesale_cost" >= 17) AND ("t0"."ss_wholesale_cost" <= 37)))) THEN 'B5' WHEN (("t0"."ss_quantity" >= 26) AND ("t0"."ss_quantity" <= 30) AND ((("t0"."ss_list_price" >= 154) AND ("t0"."ss_list_price" <= 164)) OR (("t0"."ss_coupon_amt" >= 7326) AND ("t0"."ss_coupon_amt" <= 8326)) OR (("t0"."ss_wholesale_cost" >= 7) AND ("t0"."ss_wholesale_cost" <= 27)))) THEN 'B6' ELSE CAST(NULL AS VARCHAR) END) IS NULL))
GROUP BY 1;


