-- Query 21 Level of Detail Expression
SELECT "item"."i_item_id" AS "i_item_id",   SUM((CASE WHEN ("date_dim"."d_date" < CAST('2000-03-11' AS DATE)) THEN "inventory"."inv_quantity_on_hand" ELSE 0 END)) AS "sum_calculation_42",   SUM((CASE WHEN ("date_dim"."d_date" >= CAST('2000-03-11' AS DATE)) THEN "inventory"."inv_quantity_on_hand" ELSE 0 END)) AS "sum_inv_before__co",   "warehouse"."w_warehouse_name" AS "w_warehouse_name" FROM "inventory" "inventory"   INNER JOIN "warehouse" "warehouse" ON ("inventory"."inv_warehouse_sk" = "warehouse"."w_warehouse_sk")   INNER JOIN "item" "item" ON ("inventory"."inv_item_sk" = "item"."i_item_sk")   INNER JOIN "date_dim" "date_dim" ON ("inventory"."inv_date_sk" = "date_dim"."d_date_sk") WHERE (("date_dim"."d_date" >= CAST('2000-02-10 00:00:00' AS TIMESTAMP)) AND ("date_dim"."d_date" <= CAST('2000-04-10 00:00:00' AS TIMESTAMP)) AND ("item"."i_current_price" >= 0.99) AND ("item"."i_current_price" <= 1.49)) GROUP BY 1,   4 HAVING (((CASE WHEN SUM((CASE WHEN ("date_dim"."d_date" < CAST('2000-03-11' AS DATE)) THEN "inventory"."inv_quantity_on_hand" ELSE 0 END)) = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(SUM((CASE WHEN ("date_dim"."d_date" >= CAST('2000-03-11' AS DATE)) THEN "inventory"."inv_quantity_on_hand" ELSE 0 END)) AS DOUBLE) / SUM((CASE WHEN ("date_dim"."d_date" < CAST('2000-03-11' AS DATE)) THEN "inventory"."inv_quantity_on_hand" ELSE 0 END)) END) >= 0.66659999999999331) AND ((CASE WHEN SUM((CASE WHEN ("date_dim"."d_date" < CAST('2000-03-11' AS DATE)) THEN "inventory"."inv_quantity_on_hand" ELSE 0 END)) = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(SUM((CASE WHEN ("date_dim"."d_date" >= CAST('2000-03-11' AS DATE)) THEN "inventory"."inv_quantity_on_hand" ELSE 0 END)) AS DOUBLE) / SUM((CASE WHEN ("date_dim"."d_date" < CAST('2000-03-11' AS DATE)) THEN "inventory"."inv_quantity_on_hand" ELSE 0 END)) END) <= 1.5000000000000151));


-- Query 21 Level of Detail Expression
SELECT (("date_dim"."d_date" >= CAST('2000-02-10 00:00:00' AS TIMESTAMP)) AND ("date_dim"."d_date" <= CAST('2000-04-10 00:00:00' AS TIMESTAMP))) AS "calculation_186100" FROM "date_dim" "date_dim" WHERE (("date_dim"."d_date" >= CAST('2000-02-10 00:00:00' AS TIMESTAMP)) AND ("date_dim"."d_date" <= CAST('2000-04-10 00:00:00' AS TIMESTAMP))) GROUP BY 1;


-- Query 21 Table Calculation
SELECT ("date_dim"."d_date" >= CAST('2000-03-11' AS DATE)) AS "inv_after__copy_",
  SUM("inventory"."inv_quantity_on_hand") AS "temp_tc___95167062",
  "item"."i_item_id" AS "i_item_id",
  "warehouse"."w_warehouse_name" AS "w_warehouse_name"
FROM "inventory" "inventory"
  INNER JOIN "warehouse" "warehouse" ON ("inventory"."inv_warehouse_sk" = "warehouse"."w_warehouse_sk")
  INNER JOIN "item" "item" ON ("inventory"."inv_item_sk" = "item"."i_item_sk")
  INNER JOIN "date_dim" "date_dim" ON ("inventory"."inv_date_sk" = "date_dim"."d_date_sk")
WHERE (("date_dim"."d_date" >= CAST('2000-02-10 00:00:00' AS TIMESTAMP)) AND ("date_dim"."d_date" <= CAST('2000-04-10 00:00:00' AS TIMESTAMP)) AND ("item"."i_current_price" >= 0.99) AND ("item"."i_current_price" <= 1.49))
GROUP BY 1,
  3,
  4;
  
-- Query 21 Level of Detail Expression Date Calculaton
SELECT "item"."i_item_id" AS "i_item_id",   SUM((CASE WHEN (CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('2000-03-11' AS DATE)) THEN "inventory"."inv_quantity_on_hand" ELSE 0 END)) AS "sum_inv_after__cop",   SUM((CASE WHEN (CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-03-11' AS DATE)) THEN "inventory"."inv_quantity_on_hand" ELSE 0 END)) AS "sum_inv_before__co",   "warehouse"."w_warehouse_name" AS "w_warehouse_name" FROM "inventory" "inventory"   INNER JOIN "warehouse" "warehouse" ON ("inventory"."inv_warehouse_sk" = "warehouse"."w_warehouse_sk")   INNER JOIN "item" "item" ON ("inventory"."inv_item_sk" = "item"."i_item_sk")   INNER JOIN "date_dim" "date_dim" ON ("inventory"."inv_date_sk" = "date_dim"."d_date_sk") WHERE ((CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('2000-02-10 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) <= CAST('2000-04-10 00:00:00' AS TIMESTAMP)) AND ("item"."i_current_price" >= 0.99) AND ("item"."i_current_price" <= 1.49)) GROUP BY 1,   4 HAVING (((CASE WHEN SUM((CASE WHEN (CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-03-11' AS DATE)) THEN "inventory"."inv_quantity_on_hand" ELSE 0 END)) = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(SUM((CASE WHEN (CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('2000-03-11' AS DATE)) THEN "inventory"."inv_quantity_on_hand" ELSE 0 END)) AS DOUBLE) / SUM((CASE WHEN (CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-03-11' AS DATE)) THEN "inventory"."inv_quantity_on_hand" ELSE 0 END)) END) >= 0.66666599999999332) AND ((CASE WHEN SUM((CASE WHEN (CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-03-11' AS DATE)) THEN "inventory"."inv_quantity_on_hand" ELSE 0 END)) = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(SUM((CASE WHEN (CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('2000-03-11' AS DATE)) THEN "inventory"."inv_quantity_on_hand" ELSE 0 END)) AS DOUBLE) / SUM((CASE WHEN (CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-03-11' AS DATE)) THEN "inventory"."inv_quantity_on_hand" ELSE 0 END)) END) <= 1.5000000000000151));

-- Query 21 Level of Detail Expression Date Calculaton
SELECT ((CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('2000-02-10 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) <= CAST('2000-04-10 00:00:00' AS TIMESTAMP))) AS "date_in_range__cop" FROM "inventory" "inventory" WHERE ((CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('2000-02-10 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) <= CAST('2000-04-10 00:00:00' AS TIMESTAMP))) GROUP BY 1;


-- Query 21 Table Calculation Date Calculation
SELECT (CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('2000-03-11' AS DATE)) AS "date_after__copy_",
  SUM("inventory"."inv_quantity_on_hand") AS "temp_tc___95167062",
  "item"."i_item_id" AS "i_item_id",
  "warehouse"."w_warehouse_name" AS "w_warehouse_name"
FROM "inventory" "inventory"
  INNER JOIN "warehouse" "warehouse" ON ("inventory"."inv_warehouse_sk" = "warehouse"."w_warehouse_sk")
  INNER JOIN "item" "item" ON ("inventory"."inv_item_sk" = "item"."i_item_sk")
  INNER JOIN "date_dim" "date_dim" ON ("inventory"."inv_date_sk" = "date_dim"."d_date_sk")
WHERE ((CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('2000-02-10 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) <= CAST('2000-04-10 00:00:00' AS TIMESTAMP)) AND ("item"."i_current_price" >= 0.99) AND ("item"."i_current_price" <= 1.49))
GROUP BY 1,
  3,
  4;



