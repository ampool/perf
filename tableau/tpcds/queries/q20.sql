-- Query 20 
SELECT "item"."i_category" AS "i_category",
  "item"."i_class" AS "i_class",
  "item"."i_current_price" AS "i_current_price",
  "item"."i_item_desc" AS "i_item_desc",
  "item"."i_item_id" AS "i_item_id",
  SUM("catalog_sales"."cs_ext_sales_price") AS "sum_cs_ext_sales_p"
FROM "catalog_sales" "catalog_sales"
  INNER JOIN "item" "item" ON ("catalog_sales"."cs_item_sk" = "item"."i_item_sk")
  LEFT JOIN "customer_demographics" "sales customer demographics" ON ("catalog_sales"."cs_bill_cdemo_sk" = "sales customer demographics"."cd_demo_sk")
  INNER JOIN "date_dim" "sold date" ON ("catalog_sales"."cs_sold_date_sk" = "sold date"."d_date_sk")
  LEFT JOIN "date_dim" "ship date" ON ("catalog_sales"."cs_ship_date_sk" = "ship date"."d_date_sk")
  LEFT JOIN "promotion" "promotion" ON ("catalog_sales"."cs_promo_sk" = "promotion"."p_promo_sk")
  LEFT JOIN "catalog_returns" "catalog_returns" ON (("catalog_sales"."cs_item_sk" = "catalog_returns"."cr_item_sk") AND ("catalog_sales"."cs_order_number" = "catalog_returns"."cr_order_number"))
  LEFT JOIN "call_center" "call_center" ON ("catalog_sales"."cs_call_center_sk" = "call_center"."cc_call_center_sk")
  LEFT JOIN "ship_mode" "ship_mode" ON ("catalog_sales"."cs_ship_mode_sk" = "ship_mode"."sm_ship_mode_sk")
  LEFT JOIN "warehouse" "warehouse" ON ("catalog_sales"."cs_warehouse_sk" = "warehouse"."w_warehouse_sk")
  LEFT JOIN "customer" "customer" ON ("catalog_sales"."cs_bill_customer_sk" = "customer"."c_customer_sk")
  LEFT JOIN "customer_address" "customer_address" ON ("customer"."c_current_addr_sk" = "customer_address"."ca_address_sk")
  LEFT JOIN "customer_demographics" "current customer demographics" ON ("customer"."c_current_cdemo_sk" = "current customer demographics"."cd_demo_sk")
WHERE (("item"."i_category" IN ('Books                                             ', 'Home                                              ', 'Sports                                            ')) AND ("sold date"."d_date" >= CAST('1999-02-22 00:00:00' AS TIMESTAMP)) AND ("sold date"."d_date" < CAST('1999-03-25 00:00:00' AS TIMESTAMP)))
GROUP BY 1,
  2,
  3,
  4,
  5;
  

-- Query 20 Date Calculation  
  SELECT "item"."i_category" AS "i_category",
  "item"."i_class" AS "i_class",
  "item"."i_current_price" AS "i_current_price",
  "item"."i_item_desc" AS "i_item_desc",
  "item"."i_item_id" AS "i_item_id",
  SUM("catalog_sales"."cs_ext_sales_price") AS "sum_cs_ext_sales_p"
FROM "catalog_sales" "catalog_sales"
  INNER JOIN "item" "item" ON ("catalog_sales"."cs_item_sk" = "item"."i_item_sk")
  LEFT JOIN "customer_demographics" "sales customer demographics" ON ("catalog_sales"."cs_bill_cdemo_sk" = "sales customer demographics"."cd_demo_sk")
  LEFT JOIN "date_dim" "sold date" ON ("catalog_sales"."cs_sold_date_sk" = "sold date"."d_date_sk")
  LEFT JOIN "date_dim" "ship date" ON ("catalog_sales"."cs_ship_date_sk" = "ship date"."d_date_sk")
  LEFT JOIN "promotion" "promotion" ON ("catalog_sales"."cs_promo_sk" = "promotion"."p_promo_sk")
  LEFT JOIN "catalog_returns" "catalog_returns" ON (("catalog_sales"."cs_item_sk" = "catalog_returns"."cr_item_sk") AND ("catalog_sales"."cs_order_number" = "catalog_returns"."cr_order_number"))
  LEFT JOIN "call_center" "call_center" ON ("catalog_sales"."cs_call_center_sk" = "call_center"."cc_call_center_sk")
  LEFT JOIN "ship_mode" "ship_mode" ON ("catalog_sales"."cs_ship_mode_sk" = "ship_mode"."sm_ship_mode_sk")
  LEFT JOIN "warehouse" "warehouse" ON ("catalog_sales"."cs_warehouse_sk" = "warehouse"."w_warehouse_sk")
  LEFT JOIN "customer" "customer" ON ("catalog_sales"."cs_bill_customer_sk" = "customer"."c_customer_sk")
  LEFT JOIN "customer_address" "customer_address" ON ("customer"."c_current_addr_sk" = "customer_address"."ca_address_sk")
  LEFT JOIN "customer_demographics" "current customer demographics" ON ("customer"."c_current_cdemo_sk" = "current customer demographics"."cd_demo_sk")
WHERE (("item"."i_category" IN ('Books                                             ', 'Home                                              ', 'Sports                                            ')) AND (CAST(DATE_ADD('DAY', CAST(("catalog_sales"."cs_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1999-02-22 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("catalog_sales"."cs_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('1999-03-25 00:00:00' AS TIMESTAMP)))
GROUP BY 1,
  2,
  3,
  4,
  5;

