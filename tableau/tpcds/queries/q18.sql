-- Query 18
SELECT "sales customer demographics"."cd_gender" AS "cd_gender" FROM "catalog_sales" "catalog_sales"   LEFT JOIN "customer_demographics" "sales customer demographics" ON ("catalog_sales"."cs_bill_cdemo_sk" = "sales customer demographics"."cd_demo_sk") GROUP BY 1 ORDER BY "cd_gender" ASC;

-- Query 18
SELECT "sales customer demographics"."cd_education_status" AS "cd_education_statu" FROM "catalog_sales" "catalog_sales"   LEFT JOIN "customer_demographics" "sales customer demographics" ON ("catalog_sales"."cs_bill_cdemo_sk" = "sales customer demographics"."cd_demo_sk") GROUP BY 1 ORDER BY "cd_education_statu" ASC;

-- Query 18
SELECT AVG(CAST("customer"."c_birth_year" AS DOUBLE)) AS "avg_c_birth_year_o",   AVG(CAST("sales customer demographics"."cd_dep_count" AS DOUBLE)) AS "avg_cd_dep_count_o",   AVG("catalog_sales"."cs_coupon_amt") AS "avg_cs_coupon_amt_",   AVG("catalog_sales"."cs_list_price") AS "avg_cs_list_price_",   AVG("catalog_sales"."cs_net_profit") AS "avg_cs_net_profit_",   AVG(CAST("catalog_sales"."cs_quantity" AS DOUBLE)) AS "avg_cs_quantity_ok",   AVG("catalog_sales"."cs_sales_price") AS "avg_cs_sales_price",   "customer"."c_birth_month" AS "c_birth_month",   "customer_address"."ca_country" AS "ca_country",   "customer_address"."ca_county" AS "ca_county",   "customer_address"."ca_state" AS "ca_state",   COUNT("customer"."c_birth_year") AS "cnt_c_birth_year_o",   COUNT("sales customer demographics"."cd_dep_count") AS "cnt_cd_dep_count_o",   COUNT("catalog_sales"."cs_coupon_amt") AS "cnt_cs_coupon_amt_",   COUNT("catalog_sales"."cs_list_price") AS "cnt_cs_list_price_",   COUNT("catalog_sales"."cs_net_profit") AS "cnt_cs_net_profit_",   COUNT("catalog_sales"."cs_quantity") AS "cnt_cs_quantity_ok",   COUNT("catalog_sales"."cs_sales_price") AS "cnt_cs_sales_price",   "item"."i_item_id" AS "i_item_id",   SUM("customer"."c_birth_year") AS "sum_c_birth_year_o",   SUM("sales customer demographics"."cd_dep_count") AS "sum_cd_dep_count_o",   SUM("catalog_sales"."cs_coupon_amt") AS "sum_cs_coupon_amt_",   SUM("catalog_sales"."cs_list_price") AS "sum_cs_list_price_",   SUM("catalog_sales"."cs_net_profit") AS "sum_cs_net_profit_",   SUM("catalog_sales"."cs_quantity") AS "sum_cs_quantity_ok",   SUM("catalog_sales"."cs_sales_price") AS "sum_cs_sales_price" FROM "catalog_sales" "catalog_sales"   INNER JOIN "item" "item" ON ("catalog_sales"."cs_item_sk" = "item"."i_item_sk")   INNER JOIN "customer_demographics" "sales customer demographics" ON ("catalog_sales"."cs_bill_cdemo_sk" = "sales customer demographics"."cd_demo_sk")   INNER JOIN "date_dim" "sold date" ON ("catalog_sales"."cs_sold_date_sk" = "sold date"."d_date_sk")   LEFT JOIN "date_dim" "ship date" ON ("catalog_sales"."cs_ship_date_sk" = "ship date"."d_date_sk")   LEFT JOIN "promotion" "promotion" ON ("catalog_sales"."cs_promo_sk" = "promotion"."p_promo_sk")   LEFT JOIN "catalog_returns" "catalog_returns" ON (("catalog_sales"."cs_item_sk" = "catalog_returns"."cr_item_sk") AND ("catalog_sales"."cs_order_number" = "catalog_returns"."cr_order_number"))   LEFT JOIN "call_center" "call_center" ON ("catalog_sales"."cs_call_center_sk" = "call_center"."cc_call_center_sk")   LEFT JOIN "ship_mode" "ship_mode" ON ("catalog_sales"."cs_ship_mode_sk" = "ship_mode"."sm_ship_mode_sk")   LEFT JOIN "warehouse" "warehouse" ON ("catalog_sales"."cs_warehouse_sk" = "warehouse"."w_warehouse_sk")   INNER JOIN "customer" "customer" ON ("catalog_sales"."cs_bill_customer_sk" = "customer"."c_customer_sk")   INNER JOIN "customer_address" "customer_address" ON ("customer"."c_current_addr_sk" = "customer_address"."ca_address_sk")   LEFT JOIN "customer_demographics" "current customer demographics" ON ("customer"."c_current_cdemo_sk" = "current customer demographics"."cd_demo_sk") WHERE (("customer"."c_birth_month" IN (1, 2, 6, 8, 9, 12)) AND ("customer_address"."ca_state" IN ('IN', 'MS', 'ND', 'NM', 'OK', 'VA')) AND (NOT ("customer"."c_current_cdemo_sk" IS NULL)) AND ("sales customer demographics"."cd_education_status" = 'Unknown             ') AND ("sales customer demographics"."cd_gender" = 'F') AND ("sold date"."d_year" = 1998)) GROUP BY 8,   9,   10,   11,   19;

-- Query 18 Map
SELECT MAX("customer"."c_birth_month") AS "temp_attr_c_birth_",
  MIN("customer"."c_birth_month") AS "temp_attr_c_birth1",
  AVG(CAST("customer"."c_birth_year" AS DOUBLE)) AS "avg_c_birth_year_o",
  AVG(CAST("sales customer demographics"."cd_dep_count" AS DOUBLE)) AS "avg_cd_dep_count_o",
  AVG("catalog_sales"."cs_coupon_amt") AS "avg_cs_coupon_amt_",
  AVG("catalog_sales"."cs_list_price") AS "avg_cs_list_price_",
  AVG("catalog_sales"."cs_net_profit") AS "avg_cs_net_profit_",
  AVG(CAST("catalog_sales"."cs_quantity" AS DOUBLE)) AS "avg_cs_quantity_ok",
  AVG("catalog_sales"."cs_sales_price") AS "avg_cs_sales_price",
  "customer_address"."ca_country" AS "ca_country",
  "customer_address"."ca_county" AS "ca_county",
  "customer_address"."ca_state" AS "ca_state",
  "item"."i_item_id" AS "i_item_id"
FROM "catalog_sales" "catalog_sales"
  INNER JOIN "item" "item" ON ("catalog_sales"."cs_item_sk" = "item"."i_item_sk")
  INNER JOIN "customer_demographics" "sales customer demographics" ON ("catalog_sales"."cs_bill_cdemo_sk" = "sales customer demographics"."cd_demo_sk")
  INNER JOIN "date_dim" "sold date" ON ("catalog_sales"."cs_sold_date_sk" = "sold date"."d_date_sk")
  LEFT JOIN "date_dim" "ship date" ON ("catalog_sales"."cs_ship_date_sk" = "ship date"."d_date_sk")
  LEFT JOIN "promotion" "promotion" ON ("catalog_sales"."cs_promo_sk" = "promotion"."p_promo_sk")
  LEFT JOIN "catalog_returns" "catalog_returns" ON (("catalog_sales"."cs_item_sk" = "catalog_returns"."cr_item_sk") AND ("catalog_sales"."cs_order_number" = "catalog_returns"."cr_order_number"))
  LEFT JOIN "call_center" "call_center" ON ("catalog_sales"."cs_call_center_sk" = "call_center"."cc_call_center_sk")
  LEFT JOIN "ship_mode" "ship_mode" ON ("catalog_sales"."cs_ship_mode_sk" = "ship_mode"."sm_ship_mode_sk")
  LEFT JOIN "warehouse" "warehouse" ON ("catalog_sales"."cs_warehouse_sk" = "warehouse"."w_warehouse_sk")
  INNER JOIN "customer" "customer" ON ("catalog_sales"."cs_bill_customer_sk" = "customer"."c_customer_sk")
  INNER JOIN "customer_address" "customer_address" ON ("customer"."c_current_addr_sk" = "customer_address"."ca_address_sk")
  LEFT JOIN "customer_demographics" "current customer demographics" ON ("customer"."c_current_cdemo_sk" = "current customer demographics"."cd_demo_sk")
WHERE (("customer_address"."ca_state" IN ('IN', 'MS', 'ND', 'NM', 'OK', 'VA')) AND ("customer"."c_birth_month" IN (1, 2, 6, 8, 9, 12)) AND (NOT ("customer"."c_current_cdemo_sk" IS NULL)) AND ("sales customer demographics"."cd_education_status" = 'Unknown             ') AND ("sales customer demographics"."cd_gender" = 'F') AND ("sold date"."d_year" = 1998))
GROUP BY 10,
  11,
  12,
  13;

-- Query 18 Date Calculation
SELECT YEAR(CAST(DATE_ADD('DAY', CAST(("catalog_sales"."cs_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) AS "yr_calculation_048" FROM "catalog_sales" "catalog_sales" GROUP BY 1;

-- Query 18 Date Calculation
SELECT AVG(CAST("customer"."c_birth_year" AS DOUBLE)) AS "avg_c_birth_year_o",   AVG(CAST("sales customer demographics"."cd_dep_count" AS DOUBLE)) AS "avg_cd_dep_count_o",   AVG("catalog_sales"."cs_coupon_amt") AS "avg_cs_coupon_amt_",   AVG("catalog_sales"."cs_list_price") AS "avg_cs_list_price_",   AVG("catalog_sales"."cs_net_profit") AS "avg_cs_net_profit_",   AVG(CAST("catalog_sales"."cs_quantity" AS DOUBLE)) AS "avg_cs_quantity_ok",   AVG("catalog_sales"."cs_sales_price") AS "avg_cs_sales_price",   "customer"."c_birth_month" AS "c_birth_month",   "customer_address"."ca_country" AS "ca_country",   "customer_address"."ca_county" AS "ca_county",   "customer_address"."ca_state" AS "ca_state",   COUNT("customer"."c_birth_year") AS "cnt_c_birth_year_o",   COUNT("sales customer demographics"."cd_dep_count") AS "cnt_cd_dep_count_o",   COUNT("catalog_sales"."cs_coupon_amt") AS "cnt_cs_coupon_amt_",   COUNT("catalog_sales"."cs_list_price") AS "cnt_cs_list_price_",   COUNT("catalog_sales"."cs_net_profit") AS "cnt_cs_net_profit_",   COUNT("catalog_sales"."cs_quantity") AS "cnt_cs_quantity_ok",   COUNT("catalog_sales"."cs_sales_price") AS "cnt_cs_sales_price",   "item"."i_item_id" AS "i_item_id",   SUM("customer"."c_birth_year") AS "sum_c_birth_year_o",   SUM("sales customer demographics"."cd_dep_count") AS "sum_cd_dep_count_o",   SUM("catalog_sales"."cs_coupon_amt") AS "sum_cs_coupon_amt_",   SUM("catalog_sales"."cs_list_price") AS "sum_cs_list_price_",   SUM("catalog_sales"."cs_net_profit") AS "sum_cs_net_profit_",   SUM("catalog_sales"."cs_quantity") AS "sum_cs_quantity_ok",   SUM("catalog_sales"."cs_sales_price") AS "sum_cs_sales_price" FROM "catalog_sales" "catalog_sales"   INNER JOIN "item" "item" ON ("catalog_sales"."cs_item_sk" = "item"."i_item_sk")   INNER JOIN "customer_demographics" "sales customer demographics" ON ("catalog_sales"."cs_bill_cdemo_sk" = "sales customer demographics"."cd_demo_sk")   LEFT JOIN "date_dim" "sold date" ON ("catalog_sales"."cs_sold_date_sk" = "sold date"."d_date_sk")   LEFT JOIN "date_dim" "ship date" ON ("catalog_sales"."cs_ship_date_sk" = "ship date"."d_date_sk")   LEFT JOIN "promotion" "promotion" ON ("catalog_sales"."cs_promo_sk" = "promotion"."p_promo_sk")   LEFT JOIN "catalog_returns" "catalog_returns" ON (("catalog_sales"."cs_item_sk" = "catalog_returns"."cr_item_sk") AND ("catalog_sales"."cs_order_number" = "catalog_returns"."cr_order_number"))   LEFT JOIN "call_center" "call_center" ON ("catalog_sales"."cs_call_center_sk" = "call_center"."cc_call_center_sk")   LEFT JOIN "ship_mode" "ship_mode" ON ("catalog_sales"."cs_ship_mode_sk" = "ship_mode"."sm_ship_mode_sk")   LEFT JOIN "warehouse" "warehouse" ON ("catalog_sales"."cs_warehouse_sk" = "warehouse"."w_warehouse_sk")   INNER JOIN "customer" "customer" ON ("catalog_sales"."cs_bill_customer_sk" = "customer"."c_customer_sk")   INNER JOIN "customer_address" "customer_address" ON ("customer"."c_current_addr_sk" = "customer_address"."ca_address_sk")   LEFT JOIN "customer_demographics" "current customer demographics" ON ("customer"."c_current_cdemo_sk" = "current customer demographics"."cd_demo_sk") WHERE (("customer"."c_birth_month" IN (1, 2, 6, 8, 9, 12)) AND ("customer_address"."ca_state" IN ('IN', 'MS', 'ND', 'NM', 'OK', 'VA')) AND (NOT ("customer"."c_current_cdemo_sk" IS NULL)) AND ("sales customer demographics"."cd_education_status" = 'Unknown             ') AND ("sales customer demographics"."cd_gender" = 'F') AND (YEAR(CAST(DATE_ADD('DAY', CAST(("catalog_sales"."cs_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) = 1998)) GROUP BY 8,   9,   10,   11,   19;

-- Query 18 Map Date Calculation
SELECT MAX("customer"."c_birth_month") AS "temp_attr_c_birth_",
  MIN("customer"."c_birth_month") AS "temp_attr_c_birth1",
  AVG(CAST("customer"."c_birth_year" AS DOUBLE)) AS "avg_c_birth_year_o",
  AVG(CAST("sales customer demographics"."cd_dep_count" AS DOUBLE)) AS "avg_cd_dep_count_o",
  AVG("catalog_sales"."cs_coupon_amt") AS "avg_cs_coupon_amt_",
  AVG("catalog_sales"."cs_list_price") AS "avg_cs_list_price_",
  AVG("catalog_sales"."cs_net_profit") AS "avg_cs_net_profit_",
  AVG(CAST("catalog_sales"."cs_quantity" AS DOUBLE)) AS "avg_cs_quantity_ok",
  AVG("catalog_sales"."cs_sales_price") AS "avg_cs_sales_price",
  "customer_address"."ca_country" AS "ca_country",
  "customer_address"."ca_county" AS "ca_county",
  "customer_address"."ca_state" AS "ca_state",
  "item"."i_item_id" AS "i_item_id"
FROM "catalog_sales" "catalog_sales"
  INNER JOIN "item" "item" ON ("catalog_sales"."cs_item_sk" = "item"."i_item_sk")
  INNER JOIN "customer_demographics" "sales customer demographics" ON ("catalog_sales"."cs_bill_cdemo_sk" = "sales customer demographics"."cd_demo_sk")
  LEFT JOIN "date_dim" "sold date" ON ("catalog_sales"."cs_sold_date_sk" = "sold date"."d_date_sk")
  LEFT JOIN "date_dim" "ship date" ON ("catalog_sales"."cs_ship_date_sk" = "ship date"."d_date_sk")
  LEFT JOIN "promotion" "promotion" ON ("catalog_sales"."cs_promo_sk" = "promotion"."p_promo_sk")
  LEFT JOIN "catalog_returns" "catalog_returns" ON (("catalog_sales"."cs_item_sk" = "catalog_returns"."cr_item_sk") AND ("catalog_sales"."cs_order_number" = "catalog_returns"."cr_order_number"))
  LEFT JOIN "call_center" "call_center" ON ("catalog_sales"."cs_call_center_sk" = "call_center"."cc_call_center_sk")
  LEFT JOIN "ship_mode" "ship_mode" ON ("catalog_sales"."cs_ship_mode_sk" = "ship_mode"."sm_ship_mode_sk")
  LEFT JOIN "warehouse" "warehouse" ON ("catalog_sales"."cs_warehouse_sk" = "warehouse"."w_warehouse_sk")
  INNER JOIN "customer" "customer" ON ("catalog_sales"."cs_bill_customer_sk" = "customer"."c_customer_sk")
  INNER JOIN "customer_address" "customer_address" ON ("customer"."c_current_addr_sk" = "customer_address"."ca_address_sk")
  LEFT JOIN "customer_demographics" "current customer demographics" ON ("customer"."c_current_cdemo_sk" = "current customer demographics"."cd_demo_sk")
WHERE (("customer_address"."ca_state" IN ('IN', 'MS', 'ND', 'NM', 'OK', 'VA')) AND ("customer"."c_birth_month" IN (1, 2, 6, 8, 9, 12)) AND (NOT ("customer"."c_current_cdemo_sk" IS NULL)) AND ("sales customer demographics"."cd_education_status" = 'Unknown             ') AND ("sales customer demographics"."cd_gender" = 'F') AND (YEAR(CAST(DATE_ADD('DAY', CAST(("catalog_sales"."cs_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) = 1998))
GROUP BY 10,
  11,
  12,
  13;

