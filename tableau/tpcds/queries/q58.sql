-- Query 58
SELECT "week date"."d_date" AS "d_date__date_dim_" FROM "date_dim" "week date" GROUP BY 1;

-- Query 58
SELECT "t1"."x_measure__2" AS "temp_calculation_6",   "t3"."x_measure__5" AS "temp_calculation_1",   "t5"."x_measure__8" AS "temp_calculation_2",   "t1"."i_item_id" AS "i_item_id" FROM (   SELECT "t0"."i_item_id" AS "i_item_id",     SUM("t0"."x_measure__1") AS "x_measure__2"   FROM (     SELECT "catalog sales item"."i_item_id" AS "i_item_id",       MIN("store_sales"."ss_ext_sales_price") AS "x_measure__1"     FROM "item" "catalog sales item"       INNER JOIN "catalog_sales" "catalog_sales" ON ("catalog sales item"."i_item_sk" = "catalog_sales"."cs_item_sk")       INNER JOIN "date_dim" "catalog date" ON ("catalog_sales"."cs_sold_date_sk" = "catalog date"."d_date_sk")       INNER JOIN "item" "store sales item" ON ("catalog sales item"."i_item_id" = "store sales item"."i_item_id")       INNER JOIN "store_sales" "store_sales" ON ("store sales item"."i_item_sk" = "store_sales"."ss_item_sk")       INNER JOIN "date_dim" "store date" ON ("store_sales"."ss_sold_date_sk" = "store date"."d_date_sk")       INNER JOIN "item" "web sales item" ON ("catalog sales item"."i_item_id" = "web sales item"."i_item_id")       INNER JOIN "web_sales" "web_sales" ON ("web sales item"."i_item_sk" = "web_sales"."ws_item_sk")       INNER JOIN "date_dim" "web date" ON ("web_sales"."ws_sold_date_sk" = "web date"."d_date_sk")       INNER JOIN "date_dim" "week date" ON (("catalog date"."d_week_seq" = "week date"."d_week_seq") AND ("store date"."d_week_seq" = "week date"."d_week_seq") AND ("web date"."d_week_seq" = "week date"."d_week_seq"))     WHERE ("week date"."d_date" = CAST('2000-01-03' AS DATE))     GROUP BY 1,       "store_sales"."ss_ticket_number"   ) "t0"   GROUP BY 1 ) "t1"   INNER JOIN (   SELECT "t2"."i_item_id" AS "i_item_id",     SUM("t2"."x_measure__4") AS "x_measure__5"   FROM (     SELECT "catalog sales item"."i_item_id" AS "i_item_id",       MIN("catalog_sales"."cs_ext_sales_price") AS "x_measure__4"     FROM "item" "catalog sales item"       INNER JOIN "catalog_sales" "catalog_sales" ON ("catalog sales item"."i_item_sk" = "catalog_sales"."cs_item_sk")       INNER JOIN "date_dim" "catalog date" ON ("catalog_sales"."cs_sold_date_sk" = "catalog date"."d_date_sk")       INNER JOIN "item" "store sales item" ON ("catalog sales item"."i_item_id" = "store sales item"."i_item_id")       INNER JOIN "store_sales" "store_sales" ON ("store sales item"."i_item_sk" = "store_sales"."ss_item_sk")       INNER JOIN "date_dim" "store date" ON ("store_sales"."ss_sold_date_sk" = "store date"."d_date_sk")       INNER JOIN "item" "web sales item" ON ("catalog sales item"."i_item_id" = "web sales item"."i_item_id")       INNER JOIN "web_sales" "web_sales" ON ("web sales item"."i_item_sk" = "web_sales"."ws_item_sk")       INNER JOIN "date_dim" "web date" ON ("web_sales"."ws_sold_date_sk" = "web date"."d_date_sk")       INNER JOIN "date_dim" "week date" ON (("catalog date"."d_week_seq" = "week date"."d_week_seq") AND ("store date"."d_week_seq" = "week date"."d_week_seq") AND ("web date"."d_week_seq" = "week date"."d_week_seq"))     WHERE ("week date"."d_date" = CAST('2000-01-03' AS DATE))     GROUP BY "catalog_sales"."cs_order_number",       1   ) "t2"   GROUP BY 1 ) "t3" ON ("t1"."i_item_id" = "t3"."i_item_id")   INNER JOIN (   SELECT "t4"."i_item_id" AS "i_item_id",     SUM("t4"."x_measure__7") AS "x_measure__8"   FROM (     SELECT "catalog sales item"."i_item_id" AS "i_item_id",       MIN("web_sales"."ws_ext_sales_price") AS "x_measure__7"     FROM "item" "catalog sales item"       INNER JOIN "catalog_sales" "catalog_sales" ON ("catalog sales item"."i_item_sk" = "catalog_sales"."cs_item_sk")       INNER JOIN "date_dim" "catalog date" ON ("catalog_sales"."cs_sold_date_sk" = "catalog date"."d_date_sk")       INNER JOIN "item" "store sales item" ON ("catalog sales item"."i_item_id" = "store sales item"."i_item_id")       INNER JOIN "store_sales" "store_sales" ON ("store sales item"."i_item_sk" = "store_sales"."ss_item_sk")       INNER JOIN "date_dim" "store date" ON ("store_sales"."ss_sold_date_sk" = "store date"."d_date_sk")       INNER JOIN "item" "web sales item" ON ("catalog sales item"."i_item_id" = "web sales item"."i_item_id")       INNER JOIN "web_sales" "web_sales" ON ("web sales item"."i_item_sk" = "web_sales"."ws_item_sk")       INNER JOIN "date_dim" "web date" ON ("web_sales"."ws_sold_date_sk" = "web date"."d_date_sk")       INNER JOIN "date_dim" "week date" ON (("catalog date"."d_week_seq" = "week date"."d_week_seq") AND ("store date"."d_week_seq" = "week date"."d_week_seq") AND ("web date"."d_week_seq" = "week date"."d_week_seq"))     WHERE ("week date"."d_date" = CAST('2000-01-03' AS DATE))     GROUP BY 1,       "web_sales"."ws_order_number"   ) "t4"   GROUP BY 1 ) "t5" ON ("t1"."i_item_id" = "t5"."i_item_id")   INNER JOIN (   SELECT "t6"."i_item_id" AS "i_item_id",     SUM("t6"."x_measure__4") AS "x_measure__5"   FROM (     SELECT "catalog sales item"."i_item_id" AS "i_item_id",       MIN("catalog_sales"."cs_ext_sales_price") AS "x_measure__4"     FROM "item" "catalog sales item"       INNER JOIN "catalog_sales" "catalog_sales" ON ("catalog sales item"."i_item_sk" = "catalog_sales"."cs_item_sk")       INNER JOIN "date_dim" "catalog date" ON ("catalog_sales"."cs_sold_date_sk" = "catalog date"."d_date_sk")       INNER JOIN "item" "store sales item" ON ("catalog sales item"."i_item_id" = "store sales item"."i_item_id")       INNER JOIN "store_sales" "store_sales" ON ("store sales item"."i_item_sk" = "store_sales"."ss_item_sk")       INNER JOIN "date_dim" "store date" ON ("store_sales"."ss_sold_date_sk" = "store date"."d_date_sk")       INNER JOIN "item" "web sales item" ON ("catalog sales item"."i_item_id" = "web sales item"."i_item_id")       INNER JOIN "web_sales" "web_sales" ON ("web sales item"."i_item_sk" = "web_sales"."ws_item_sk")       INNER JOIN "date_dim" "web date" ON ("web_sales"."ws_sold_date_sk" = "web date"."d_date_sk")       INNER JOIN "date_dim" "week date" ON (("catalog date"."d_week_seq" = "week date"."d_week_seq") AND ("store date"."d_week_seq" = "week date"."d_week_seq") AND ("web date"."d_week_seq" = "week date"."d_week_seq"))     WHERE ("week date"."d_date" = CAST('2000-01-03' AS DATE))     GROUP BY "catalog_sales"."cs_order_number",       1   ) "t6"   GROUP BY 1 ) "t7" ON ("t1"."i_item_id" = "t7"."i_item_id")   INNER JOIN (   SELECT "t8"."i_item_id" AS "i_item_id",     SUM("t8"."x_measure__7") AS "x_measure__8"   FROM (     SELECT "catalog sales item"."i_item_id" AS "i_item_id",       MIN("web_sales"."ws_ext_sales_price") AS "x_measure__7"     FROM "item" "catalog sales item"       INNER JOIN "catalog_sales" "catalog_sales" ON ("catalog sales item"."i_item_sk" = "catalog_sales"."cs_item_sk")       INNER JOIN "date_dim" "catalog date" ON ("catalog_sales"."cs_sold_date_sk" = "catalog date"."d_date_sk")       INNER JOIN "item" "store sales item" ON ("catalog sales item"."i_item_id" = "store sales item"."i_item_id")       INNER JOIN "store_sales" "store_sales" ON ("store sales item"."i_item_sk" = "store_sales"."ss_item_sk")       INNER JOIN "date_dim" "store date" ON ("store_sales"."ss_sold_date_sk" = "store date"."d_date_sk")       INNER JOIN "item" "web sales item" ON ("catalog sales item"."i_item_id" = "web sales item"."i_item_id")       INNER JOIN "web_sales" "web_sales" ON ("web sales item"."i_item_sk" = "web_sales"."ws_item_sk")       INNER JOIN "date_dim" "web date" ON ("web_sales"."ws_sold_date_sk" = "web date"."d_date_sk")       INNER JOIN "date_dim" "week date" ON (("catalog date"."d_week_seq" = "week date"."d_week_seq") AND ("store date"."d_week_seq" = "week date"."d_week_seq") AND ("web date"."d_week_seq" = "week date"."d_week_seq"))     WHERE ("week date"."d_date" = CAST('2000-01-03' AS DATE))     GROUP BY 1,       "web_sales"."ws_order_number"   ) "t8"   GROUP BY 1 ) "t9" ON ("t1"."i_item_id" = "t9"."i_item_id")   INNER JOIN (   SELECT "t10"."i_item_id" AS "i_item_id",     SUM("t10"."x_measure__1") AS "x_measure__2"   FROM (     SELECT "catalog sales item"."i_item_id" AS "i_item_id",       MIN("store_sales"."ss_ext_sales_price") AS "x_measure__1"     FROM "item" "catalog sales item"       INNER JOIN "catalog_sales" "catalog_sales" ON ("catalog sales item"."i_item_sk" = "catalog_sales"."cs_item_sk")       INNER JOIN "date_dim" "catalog date" ON ("catalog_sales"."cs_sold_date_sk" = "catalog date"."d_date_sk")       INNER JOIN "item" "store sales item" ON ("catalog sales item"."i_item_id" = "store sales item"."i_item_id")       INNER JOIN "store_sales" "store_sales" ON ("store sales item"."i_item_sk" = "store_sales"."ss_item_sk")       INNER JOIN "date_dim" "store date" ON ("store_sales"."ss_sold_date_sk" = "store date"."d_date_sk")       INNER JOIN "item" "web sales item" ON ("catalog sales item"."i_item_id" = "web sales item"."i_item_id")       INNER JOIN "web_sales" "web_sales" ON ("web sales item"."i_item_sk" = "web_sales"."ws_item_sk")       INNER JOIN "date_dim" "web date" ON ("web_sales"."ws_sold_date_sk" = "web date"."d_date_sk")       INNER JOIN "date_dim" "week date" ON (("catalog date"."d_week_seq" = "week date"."d_week_seq") AND ("store date"."d_week_seq" = "week date"."d_week_seq") AND ("web date"."d_week_seq" = "week date"."d_week_seq"))     WHERE ("week date"."d_date" = CAST('2000-01-03' AS DATE))     GROUP BY 1,       "store_sales"."ss_ticket_number"   ) "t10"   GROUP BY 1 ) "t11" ON ("t1"."i_item_id" = "t11"."i_item_id") WHERE (((("t7"."x_measure__5" * 10) >= (9 * "t9"."x_measure__8")) AND (("t9"."x_measure__8" * 10) >= (9 * "t7"."x_measure__5")) AND (("t7"."x_measure__5" * 10) <= (11 * "t9"."x_measure__8")) AND (("t9"."x_measure__8" * 10) <= (11 * "t7"."x_measure__5"))) AND ((("t7"."x_measure__5" * 10) >= (9 * "t11"."x_measure__2")) AND (("t9"."x_measure__8" * 10) >= (9 * "t11"."x_measure__2")) AND (("t11"."x_measure__2" * 10) >= (9 * "t9"."x_measure__8")) AND (("t11"."x_measure__2" * 10) >= (9 * "t7"."x_measure__5")) AND (("t7"."x_measure__5" * 10) <= (11 * "t11"."x_measure__2")) AND (("t9"."x_measure__8" * 10) <= (11 * "t11"."x_measure__2")) AND (("t11"."x_measure__2" * 10) <= (11 * "t9"."x_measure__8")) AND (("t11"."x_measure__2" * 10) <= (11 * "t7"."x_measure__5"))));



-- Query 58 Date Calculation
SELECT "t1"."x_measure__2" AS "temp_calculation_6",
  "t3"."x_measure__5" AS "temp_calculation_1",
  "t5"."x_measure__8" AS "temp_calculation_2",
  "t1"."i_item_id" AS "i_item_id"
FROM (
  SELECT "t0"."i_item_id" AS "i_item_id",
    SUM("t0"."x_measure__1") AS "x_measure__2"
  FROM (
    SELECT "catalog sales item"."i_item_id" AS "i_item_id",
      MIN("store_sales"."ss_ext_sales_price") AS "x_measure__1"
    FROM "item" "catalog sales item"
      INNER JOIN "catalog_sales" "catalog_sales" ON ("catalog sales item"."i_item_sk" = "catalog_sales"."cs_item_sk")
      INNER JOIN "item" "store sales item" ON ("catalog sales item"."i_item_id" = "store sales item"."i_item_id")
      INNER JOIN "store_sales" "store_sales" ON ("store sales item"."i_item_sk" = "store_sales"."ss_item_sk")
      INNER JOIN "item" "web sales item" ON ("catalog sales item"."i_item_id" = "web sales item"."i_item_id")
      INNER JOIN "web_sales" "web_sales" ON ("web sales item"."i_item_sk" = "web_sales"."ws_item_sk")
    WHERE ((CAST(DATE_ADD('DAY', CAST(("web_sales"."ws_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1999-12-28 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("web_sales"."ws_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-01-04 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("catalog_sales"."cs_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1999-12-28 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("catalog_sales"."cs_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-01-04 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1999-12-28 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-01-04 00:00:00' AS TIMESTAMP)))
    GROUP BY 1,
      "store_sales"."ss_ticket_number"
  ) "t0"
  GROUP BY 1
) "t1"
  INNER JOIN (
  SELECT "t2"."i_item_id" AS "i_item_id",
    SUM("t2"."x_measure__4") AS "x_measure__5"
  FROM (
    SELECT "catalog sales item"."i_item_id" AS "i_item_id",
      MIN("catalog_sales"."cs_ext_sales_price") AS "x_measure__4"
    FROM "item" "catalog sales item"
      INNER JOIN "catalog_sales" "catalog_sales" ON ("catalog sales item"."i_item_sk" = "catalog_sales"."cs_item_sk")
      INNER JOIN "item" "store sales item" ON ("catalog sales item"."i_item_id" = "store sales item"."i_item_id")
      INNER JOIN "store_sales" "store_sales" ON ("store sales item"."i_item_sk" = "store_sales"."ss_item_sk")
      INNER JOIN "item" "web sales item" ON ("catalog sales item"."i_item_id" = "web sales item"."i_item_id")
      INNER JOIN "web_sales" "web_sales" ON ("web sales item"."i_item_sk" = "web_sales"."ws_item_sk")
    WHERE ((CAST(DATE_ADD('DAY', CAST(("web_sales"."ws_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1999-12-28 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("web_sales"."ws_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-01-04 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("catalog_sales"."cs_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1999-12-28 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("catalog_sales"."cs_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-01-04 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1999-12-28 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-01-04 00:00:00' AS TIMESTAMP)))
    GROUP BY "catalog_sales"."cs_order_number",
      1
  ) "t2"
  GROUP BY 1
) "t3" ON ("t1"."i_item_id" = "t3"."i_item_id")
  INNER JOIN (
  SELECT "t4"."i_item_id" AS "i_item_id",
    SUM("t4"."x_measure__7") AS "x_measure__8"
  FROM (
    SELECT "catalog sales item"."i_item_id" AS "i_item_id",
      MIN("web_sales"."ws_ext_sales_price") AS "x_measure__7"
    FROM "item" "catalog sales item"
      INNER JOIN "catalog_sales" "catalog_sales" ON ("catalog sales item"."i_item_sk" = "catalog_sales"."cs_item_sk")
      INNER JOIN "item" "store sales item" ON ("catalog sales item"."i_item_id" = "store sales item"."i_item_id")
      INNER JOIN "store_sales" "store_sales" ON ("store sales item"."i_item_sk" = "store_sales"."ss_item_sk")
      INNER JOIN "item" "web sales item" ON ("catalog sales item"."i_item_id" = "web sales item"."i_item_id")
      INNER JOIN "web_sales" "web_sales" ON ("web sales item"."i_item_sk" = "web_sales"."ws_item_sk")
    WHERE ((CAST(DATE_ADD('DAY', CAST(("web_sales"."ws_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1999-12-28 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("web_sales"."ws_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-01-04 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("catalog_sales"."cs_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1999-12-28 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("catalog_sales"."cs_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-01-04 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1999-12-28 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-01-04 00:00:00' AS TIMESTAMP)))
    GROUP BY 1,
      "web_sales"."ws_order_number"
  ) "t4"
  GROUP BY 1
) "t5" ON ("t1"."i_item_id" = "t5"."i_item_id")
  INNER JOIN (
  SELECT "t6"."i_item_id" AS "i_item_id",
    SUM("t6"."x_measure__4") AS "x_measure__5"
  FROM (
    SELECT "catalog sales item"."i_item_id" AS "i_item_id",
      MIN("catalog_sales"."cs_ext_sales_price") AS "x_measure__4"
    FROM "item" "catalog sales item"
      INNER JOIN "catalog_sales" "catalog_sales" ON ("catalog sales item"."i_item_sk" = "catalog_sales"."cs_item_sk")
      INNER JOIN "item" "store sales item" ON ("catalog sales item"."i_item_id" = "store sales item"."i_item_id")
      INNER JOIN "store_sales" "store_sales" ON ("store sales item"."i_item_sk" = "store_sales"."ss_item_sk")
      INNER JOIN "item" "web sales item" ON ("catalog sales item"."i_item_id" = "web sales item"."i_item_id")
      INNER JOIN "web_sales" "web_sales" ON ("web sales item"."i_item_sk" = "web_sales"."ws_item_sk")
    WHERE ((CAST(DATE_ADD('DAY', CAST(("web_sales"."ws_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1999-12-28 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("web_sales"."ws_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-01-04 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("catalog_sales"."cs_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1999-12-28 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("catalog_sales"."cs_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-01-04 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1999-12-28 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-01-04 00:00:00' AS TIMESTAMP)))
    GROUP BY "catalog_sales"."cs_order_number",
      1
  ) "t6"
  GROUP BY 1
) "t7" ON ("t1"."i_item_id" = "t7"."i_item_id")
  INNER JOIN (
  SELECT "t8"."i_item_id" AS "i_item_id",
    SUM("t8"."x_measure__7") AS "x_measure__8"
  FROM (
    SELECT "catalog sales item"."i_item_id" AS "i_item_id",
      MIN("web_sales"."ws_ext_sales_price") AS "x_measure__7"
    FROM "item" "catalog sales item"
      INNER JOIN "catalog_sales" "catalog_sales" ON ("catalog sales item"."i_item_sk" = "catalog_sales"."cs_item_sk")
      INNER JOIN "item" "store sales item" ON ("catalog sales item"."i_item_id" = "store sales item"."i_item_id")
      INNER JOIN "store_sales" "store_sales" ON ("store sales item"."i_item_sk" = "store_sales"."ss_item_sk")
      INNER JOIN "item" "web sales item" ON ("catalog sales item"."i_item_id" = "web sales item"."i_item_id")
      INNER JOIN "web_sales" "web_sales" ON ("web sales item"."i_item_sk" = "web_sales"."ws_item_sk")
    WHERE ((CAST(DATE_ADD('DAY', CAST(("web_sales"."ws_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1999-12-28 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("web_sales"."ws_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-01-04 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("catalog_sales"."cs_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1999-12-28 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("catalog_sales"."cs_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-01-04 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1999-12-28 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-01-04 00:00:00' AS TIMESTAMP)))
    GROUP BY 1,
      "web_sales"."ws_order_number"
  ) "t8"
  GROUP BY 1
) "t9" ON ("t1"."i_item_id" = "t9"."i_item_id")
  INNER JOIN (
  SELECT "t10"."i_item_id" AS "i_item_id",
    SUM("t10"."x_measure__1") AS "x_measure__2"
  FROM (
    SELECT "catalog sales item"."i_item_id" AS "i_item_id",
      MIN("store_sales"."ss_ext_sales_price") AS "x_measure__1"
    FROM "item" "catalog sales item"
      INNER JOIN "catalog_sales" "catalog_sales" ON ("catalog sales item"."i_item_sk" = "catalog_sales"."cs_item_sk")
      INNER JOIN "item" "store sales item" ON ("catalog sales item"."i_item_id" = "store sales item"."i_item_id")
      INNER JOIN "store_sales" "store_sales" ON ("store sales item"."i_item_sk" = "store_sales"."ss_item_sk")
      INNER JOIN "item" "web sales item" ON ("catalog sales item"."i_item_id" = "web sales item"."i_item_id")
      INNER JOIN "web_sales" "web_sales" ON ("web sales item"."i_item_sk" = "web_sales"."ws_item_sk")
    WHERE ((CAST(DATE_ADD('DAY', CAST(("web_sales"."ws_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1999-12-28 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("web_sales"."ws_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-01-04 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("catalog_sales"."cs_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1999-12-28 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("catalog_sales"."cs_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-01-04 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1999-12-28 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-01-04 00:00:00' AS TIMESTAMP)))
    GROUP BY 1,
      "store_sales"."ss_ticket_number"
  ) "t10"
  GROUP BY 1
) "t11" ON ("t1"."i_item_id" = "t11"."i_item_id")
WHERE (((("t7"."x_measure__5" * 10) >= (9 * "t9"."x_measure__8")) AND (("t9"."x_measure__8" * 10) >= (9 * "t7"."x_measure__5")) AND (("t7"."x_measure__5" * 10) <= (11 * "t9"."x_measure__8")) AND (("t9"."x_measure__8" * 10) <= (11 * "t7"."x_measure__5"))) AND ((("t7"."x_measure__5" * 10) >= (9 * "t11"."x_measure__2")) AND (("t9"."x_measure__8" * 10) >= (9 * "t11"."x_measure__2")) AND (("t11"."x_measure__2" * 10) >= (9 * "t9"."x_measure__8")) AND (("t11"."x_measure__2" * 10) >= (9 * "t7"."x_measure__5")) AND (("t7"."x_measure__5" * 10) <= (11 * "t11"."x_measure__2")) AND (("t9"."x_measure__8" * 10) <= (11 * "t11"."x_measure__2")) AND (("t11"."x_measure__2" * 10) <= (11 * "t9"."x_measure__8")) AND (("t11"."x_measure__2" * 10) <= (11 * "t7"."x_measure__5"))));



