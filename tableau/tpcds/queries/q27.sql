-- Query 27
SELECT "store"."s_state" AS "s_state" FROM "store_sales" "store_sales"   LEFT JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")   RIGHT JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk") GROUP BY 1 ORDER BY "s_state" ASC;

-- Query 27
SELECT AVG("t0"."ss_coupon_amt") AS "avg_ss_coupon_amt_",   AVG("t0"."ss_list_price") AS "avg_ss_list_price_",   AVG(CAST("t0"."ss_quantity" AS DOUBLE)) AS "avg_ss_quantity_ok",   AVG("t0"."ss_sales_price") AS "avg_ss_sales_price",   "item"."i_item_id" AS "i_item_id",   "t0"."s_state" AS "s_state" FROM "item" "item"   INNER JOIN (   SELECT "store_sales"."ss_item_sk" AS "ss_item_sk",     "customer_demographics"."cd_education_status" AS "cd_education_statu",     "customer_demographics"."cd_gender" AS "cd_gender",     "customer_demographics"."cd_marital_status" AS "cd_marital_status",     "store"."s_state" AS "s_state",     "store_sales"."ss_coupon_amt" AS "ss_coupon_amt",     "store_sales"."ss_list_price" AS "ss_list_price",     "store_sales"."ss_promo_sk" AS "ss_promo_sk",     "store_sales"."ss_quantity" AS "ss_quantity",     "store_sales"."ss_sales_price" AS "ss_sales_price",     "store_sales"."ss_sold_date_sk" AS "ss_sold_date_sk",     "store_sales"."ss_sold_time_sk" AS "ss_sold_time_sk"   FROM "store_sales" "store_sales"     LEFT JOIN "customer_address" "bought address" ON ("store_sales"."ss_addr_sk" = "bought address"."ca_address_sk")     INNER JOIN "customer_demographics" "customer_demographics" ON ("store_sales"."ss_cdemo_sk" = "customer_demographics"."cd_demo_sk")     LEFT JOIN "household_demographics" "household_demographics" ON ("store_sales"."ss_hdemo_sk" = "household_demographics"."hd_demo_sk")     INNER JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")     LEFT JOIN "customer" "customer" ON ("store_sales"."ss_customer_sk" = "customer"."c_customer_sk")     LEFT JOIN "customer_address" "current address" ON ("customer"."c_current_addr_sk" = "current address"."ca_address_sk") ) "t0" ON ("item"."i_item_sk" = "t0"."ss_item_sk")   LEFT JOIN "promotion" "promotion" ON ("t0"."ss_promo_sk" = "promotion"."p_promo_sk")   LEFT JOIN "time_dim" "time_dim" ON ("t0"."ss_sold_time_sk" = "time_dim"."t_time_sk")   INNER JOIN "date_dim" "date_dim" ON ("t0"."ss_sold_date_sk" = "date_dim"."d_date_sk") WHERE (("t0"."s_state" = 'TN') AND ("t0"."cd_education_statu" = 'College             ') AND ("t0"."cd_gender" = 'M') AND ("t0"."cd_marital_status" = 'S') AND ("date_dim"."d_year" = 2002)) GROUP BY 5,   6;

-- Query 27 Date Calculation
SELECT AVG("t0"."ss_coupon_amt") AS "avg_ss_coupon_amt_",
  AVG("t0"."ss_list_price") AS "avg_ss_list_price_",
  AVG(CAST("t0"."ss_quantity" AS DOUBLE)) AS "avg_ss_quantity_ok",
  AVG("t0"."ss_sales_price") AS "avg_ss_sales_price",
  "item"."i_item_id" AS "i_item_id",
  "t0"."s_state" AS "s_state"
FROM "item" "item"
  INNER JOIN (
  SELECT "store_sales"."ss_item_sk" AS "ss_item_sk",
    "customer_demographics"."cd_education_status" AS "cd_education_statu",
    "customer_demographics"."cd_gender" AS "cd_gender",
    "customer_demographics"."cd_marital_status" AS "cd_marital_status",
    "store"."s_state" AS "s_state",
    "store_sales"."ss_coupon_amt" AS "ss_coupon_amt",
    "store_sales"."ss_list_price" AS "ss_list_price",
    "store_sales"."ss_promo_sk" AS "ss_promo_sk",
    "store_sales"."ss_quantity" AS "ss_quantity",
    "store_sales"."ss_sales_price" AS "ss_sales_price",
    "store_sales"."ss_sold_date_sk" AS "ss_sold_date_sk",
    "store_sales"."ss_sold_time_sk" AS "ss_sold_time_sk"
  FROM "store_sales" "store_sales"
    LEFT JOIN "customer_address" "bought address" ON ("store_sales"."ss_addr_sk" = "bought address"."ca_address_sk")
    INNER JOIN "customer_demographics" "customer_demographics" ON ("store_sales"."ss_cdemo_sk" = "customer_demographics"."cd_demo_sk")
    LEFT JOIN "household_demographics" "household_demographics" ON ("store_sales"."ss_hdemo_sk" = "household_demographics"."hd_demo_sk")
    INNER JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")
    LEFT JOIN "customer" "customer" ON ("store_sales"."ss_customer_sk" = "customer"."c_customer_sk")
    LEFT JOIN "customer_address" "current address" ON ("customer"."c_current_addr_sk" = "current address"."ca_address_sk")
) "t0" ON ("item"."i_item_sk" = "t0"."ss_item_sk")
  LEFT JOIN "promotion" "promotion" ON ("t0"."ss_promo_sk" = "promotion"."p_promo_sk")
  LEFT JOIN "time_dim" "time_dim" ON ("t0"."ss_sold_time_sk" = "time_dim"."t_time_sk")
  LEFT JOIN "date_dim" "date_dim" ON ("t0"."ss_sold_date_sk" = "date_dim"."d_date_sk")
WHERE (("t0"."s_state" = 'TN') AND ("t0"."cd_education_statu" = 'College             ') AND ("t0"."cd_gender" = 'M') AND ("t0"."cd_marital_status" = 'S') AND (YEAR(CAST(DATE_ADD('DAY', CAST(("t0"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) = 2002))
GROUP BY 5,
  6;
