-- Query 42
SELECT "date_dim"."d_moy" AS "d_moy" FROM "date_dim" "date_dim" GROUP BY 1;

-- Query 42
SELECT 2000 AS "d_year",   "item"."i_category" AS "i_category",   "item"."i_category_id" AS "i_category_id",   SUM("store_sales"."ss_ext_sales_price") AS "sum_ss_ext_sales_p" FROM "store_sales" "store_sales"   INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")   INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")   LEFT JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk") WHERE (("date_dim"."d_year" = 2000) AND ("date_dim"."d_moy" = 11) AND ("item"."i_manager_id" = 1)) GROUP BY "date_dim"."d_year",   2,   3;

-- Query 42 Date Calculation
SELECT MONTH(CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) AS "mn_calculation_048" FROM "store_sales" "store_sales" GROUP BY 1;

-- Query 42 Date Calculation
SELECT "item"."i_category" AS "i_category",   "item"."i_category_id" AS "i_category_id",   SUM("store_sales"."ss_ext_sales_price") AS "sum_ss_ext_sales_p",   2000 AS "yr_calculation_048" FROM "store_sales" "store_sales"   INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")   INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")   LEFT JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk") WHERE ((YEAR(CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) = 2000) AND ("item"."i_manager_id" = 1) AND (MONTH(CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) = 11)) GROUP BY 1,   2,   YEAR(CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE));
