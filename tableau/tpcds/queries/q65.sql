-- Query 65 Level of Detail Expression
SELECT "t0"."i_brand" AS "i_brand",
  "t0"."i_current_price" AS "i_current_price",
  "t0"."i_item_desc" AS "i_item_desc",
  "t0"."i_item_sk" AS "i_item_sk",
  "t0"."i_wholesale_cost" AS "i_wholesale_cost",
  "t0"."s_store_name" AS "s_store_name",
  "t0"."s_store_sk" AS "s_store_sk",
  "t0"."sum_ss_sales_price" AS "sum_ss_sales_price"
FROM (
  SELECT "item"."i_brand" AS "i_brand",
    "item"."i_current_price" AS "i_current_price",
    "item"."i_item_desc" AS "i_item_desc",
    "item"."i_item_sk" AS "i_item_sk",
    "item"."i_wholesale_cost" AS "i_wholesale_cost",
    "store"."s_store_name" AS "s_store_name",
    "store"."s_store_sk" AS "s_store_sk",
    SUM("store_sales"."ss_sales_price") AS "sum_ss_sales_price"
  FROM "store_sales" "store_sales"
    INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")
    INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")
    LEFT JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")
  WHERE (("date_dim"."d_month_seq" >= 1176) AND ("date_dim"."d_month_seq" <= 1187) AND (NOT ("store_sales"."ss_store_sk" IS NULL)))
  GROUP BY 1,
    2,
    3,
    4,
    5,
    6,
    7
) "t0"
  INNER JOIN (
  SELECT "t1"."i_brand" AS "i_brand",
    "t1"."i_current_price" AS "i_current_price",
    "t1"."i_item_desc" AS "i_item_desc",
    "t1"."i_item_sk" AS "i_item_sk",
    "t1"."i_wholesale_cost" AS "i_wholesale_cost",
    "t1"."s_store_name" AS "s_store_name",
    "t1"."s_store_sk" AS "s_store_sk",
    MIN("t4"."x_measure__3") AS "x_measure__4"
  FROM (
    SELECT "item"."i_brand" AS "i_brand",
      "item"."i_current_price" AS "i_current_price",
      "item"."i_item_desc" AS "i_item_desc",
      "item"."i_item_sk" AS "i_item_sk",
      "item"."i_wholesale_cost" AS "i_wholesale_cost",
      "store"."s_store_name" AS "s_store_name",
      "store_sales"."ss_store_sk" AS "ss_store_sk",
      MIN("store"."s_store_sk") AS "s_store_sk"
    FROM "store_sales" "store_sales"
      INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")
      INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")
      LEFT JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")
    WHERE ((NOT ("store_sales"."ss_store_sk" IS NULL)) AND ("date_dim"."d_month_seq" >= 1176) AND ("date_dim"."d_month_seq" <= 1187))
    GROUP BY 1,
      2,
      3,
      4,
      5,
      6,
      7
  ) "t1"
    INNER JOIN (
    SELECT "t2"."ss_store_sk" AS "ss_store_sk",
      AVG("t3"."x_measure__2") AS "x_measure__3"
    FROM (
      SELECT "store_sales"."ss_item_sk" AS "ss_item_sk",
        "store_sales"."ss_store_sk" AS "ss_store_sk"
      FROM "store_sales" "store_sales"
        INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")
        INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")
      WHERE (("date_dim"."d_month_seq" >= 1176) AND ("date_dim"."d_month_seq" <= 1187))
      GROUP BY 1,
        2
    ) "t2"
      INNER JOIN (
      SELECT "store_sales"."ss_item_sk" AS "ss_item_sk",
        "store_sales"."ss_store_sk" AS "ss_store_sk",
        SUM("store_sales"."ss_sales_price") AS "x_measure__2"
      FROM "store_sales" "store_sales"
        INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")
        INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")
        LEFT JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")
      WHERE (("date_dim"."d_month_seq" >= 1176) AND ("date_dim"."d_month_seq" <= 1187))
      GROUP BY 1,
        2
    ) "t3" ON (("t2"."ss_item_sk" = "t3"."ss_item_sk") AND ((COALESCE("t2"."ss_store_sk", 0) = COALESCE("t3"."ss_store_sk", 0)) AND (COALESCE("t2"."ss_store_sk", 1) = COALESCE("t3"."ss_store_sk", 1))))
    GROUP BY 1
  ) "t4" ON ("t1"."ss_store_sk" = "t4"."ss_store_sk")
  GROUP BY 1,
    2,
    3,
    4,
    5,
    6,
    7
) "t5" ON (((COALESCE("t0"."i_brand", '0') = COALESCE("t5"."i_brand", '0')) AND (COALESCE("t0"."i_brand", '1') = COALESCE("t5"."i_brand", '1'))) AND ((COALESCE("t0"."i_current_price", 0) = COALESCE("t5"."i_current_price", 0)) AND (COALESCE("t0"."i_current_price", 1) = COALESCE("t5"."i_current_price", 1))) AND ((COALESCE("t0"."i_item_desc", '0') = COALESCE("t5"."i_item_desc", '0')) AND (COALESCE("t0"."i_item_desc", '1') = COALESCE("t5"."i_item_desc", '1'))) AND ("t0"."i_item_sk" = "t5"."i_item_sk") AND ((COALESCE("t0"."i_wholesale_cost", 0) = COALESCE("t5"."i_wholesale_cost", 0)) AND (COALESCE("t0"."i_wholesale_cost", 1) = COALESCE("t5"."i_wholesale_cost", 1))) AND ((COALESCE("t0"."s_store_name", '0') = COALESCE("t5"."s_store_name", '0')) AND (COALESCE("t0"."s_store_name", '1') = COALESCE("t5"."s_store_name", '1'))) AND ((COALESCE("t0"."s_store_sk", 0) = COALESCE("t5"."s_store_sk", 0)) AND (COALESCE("t0"."s_store_sk", 1) = COALESCE("t5"."s_store_sk", 1))))
WHERE ("t5"."x_measure__4" > (10 * "t0"."sum_ss_sales_price"));


-- Query 65 Table Calculation
SELECT SUM("store_sales"."ss_sales_price") AS "temp_tc___33806120",
  MIN("item"."i_brand") AS "temp_attr_i_brand_",
  MAX("item"."i_brand") AS "temp_attr_i_brand1",
  MAX("item"."i_current_price") AS "temp_attr_i_curren",
  MIN("item"."i_current_price") AS "temp_attr_i_curre1",
  MIN("item"."i_item_desc") AS "temp_attr_i_item_d",
  MAX("item"."i_item_desc") AS "temp_attr_i_item_1",
  MAX("item"."i_wholesale_cost") AS "temp_attr_i_wholes",
  MIN("item"."i_wholesale_cost") AS "temp_attr_i_whole1",
  MIN("store"."s_store_name") AS "temp_attr_s_store_",
  MAX("store"."s_store_name") AS "temp_attr_s_store1",
  "item"."i_item_sk" AS "i_item_sk",
  "store"."s_store_sk" AS "s_store_sk"
FROM "store_sales" "store_sales"
  INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")
  INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")
  LEFT JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")
WHERE (("date_dim"."d_month_seq" >= 1176) AND ("date_dim"."d_month_seq" <= 1187) AND (NOT ("store_sales"."ss_store_sk" IS NULL)))
GROUP BY 12,
  13;


-- Query 65 Level of Detail Expression and Date Calculation
  SELECT "t0"."i_brand" AS "i_brand",
  "t0"."i_current_price" AS "i_current_price",
  "t0"."i_item_desc" AS "i_item_desc",
  "t0"."i_item_sk" AS "i_item_sk",
  "t0"."i_wholesale_cost" AS "i_wholesale_cost",
  "t0"."s_store_name" AS "s_store_name",
  "t0"."s_store_sk" AS "s_store_sk",
  "t0"."sum_ss_sales_price" AS "sum_ss_sales_price"
FROM (
  SELECT "item"."i_brand" AS "i_brand",
    "item"."i_current_price" AS "i_current_price",
    "item"."i_item_desc" AS "i_item_desc",
    "item"."i_item_sk" AS "i_item_sk",
    "item"."i_wholesale_cost" AS "i_wholesale_cost",
    "store"."s_store_name" AS "s_store_name",
    "store"."s_store_sk" AS "s_store_sk",
    SUM("store_sales"."ss_sales_price") AS "sum_ss_sales_price"
  FROM "store_sales" "store_sales"
    INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")
    INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")
    LEFT JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")
  WHERE ((CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1998-01-01 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('1999-01-01 00:00:00' AS TIMESTAMP)) AND (NOT ("store_sales"."ss_store_sk" IS NULL)))
  GROUP BY 1,
    2,
    3,
    4,
    5,
    6,
    7
) "t0"
  INNER JOIN (
  SELECT "t1"."i_brand" AS "i_brand",
    "t1"."i_current_price" AS "i_current_price",
    "t1"."i_item_desc" AS "i_item_desc",
    "t1"."i_item_sk" AS "i_item_sk",
    "t1"."i_wholesale_cost" AS "i_wholesale_cost",
    "t1"."s_store_name" AS "s_store_name",
    "t1"."s_store_sk" AS "s_store_sk",
    MIN("t4"."x_measure__3") AS "x_measure__4"
  FROM (
    SELECT "item"."i_brand" AS "i_brand",
      "item"."i_current_price" AS "i_current_price",
      "item"."i_item_desc" AS "i_item_desc",
      "item"."i_item_sk" AS "i_item_sk",
      "item"."i_wholesale_cost" AS "i_wholesale_cost",
      "store"."s_store_name" AS "s_store_name",
      "store_sales"."ss_store_sk" AS "ss_store_sk",
      MIN("store"."s_store_sk") AS "s_store_sk"
    FROM "store_sales" "store_sales"
      INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")
      INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")
      LEFT JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")
    WHERE ((NOT ("store_sales"."ss_store_sk" IS NULL)) AND (CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1998-01-01 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('1999-01-01 00:00:00' AS TIMESTAMP)))
    GROUP BY 1,
      2,
      3,
      4,
      5,
      6,
      7
  ) "t1"
    INNER JOIN (
    SELECT "t2"."ss_store_sk" AS "ss_store_sk",
      AVG("t3"."x_measure__2") AS "x_measure__3"
    FROM (
      SELECT "store_sales"."ss_item_sk" AS "ss_item_sk",
        "store_sales"."ss_store_sk" AS "ss_store_sk"
      FROM "store_sales" "store_sales"
        INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")
        INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")
      WHERE ((CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1998-01-01 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('1999-01-01 00:00:00' AS TIMESTAMP)))
      GROUP BY 1,
        2
    ) "t2"
      INNER JOIN (
      SELECT "store_sales"."ss_item_sk" AS "ss_item_sk",
        "store_sales"."ss_store_sk" AS "ss_store_sk",
        SUM("store_sales"."ss_sales_price") AS "x_measure__2"
      FROM "store_sales" "store_sales"
        INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")
        INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")
        LEFT JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")
      WHERE ((CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1998-01-01 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('1999-01-01 00:00:00' AS TIMESTAMP)))
      GROUP BY 1,
        2
    ) "t3" ON (("t2"."ss_item_sk" = "t3"."ss_item_sk") AND ((COALESCE("t2"."ss_store_sk", 0) = COALESCE("t3"."ss_store_sk", 0)) AND (COALESCE("t2"."ss_store_sk", 1) = COALESCE("t3"."ss_store_sk", 1))))
    GROUP BY 1
  ) "t4" ON ("t1"."ss_store_sk" = "t4"."ss_store_sk")
  GROUP BY 1,
    2,
    3,
    4,
    5,
    6,
    7
) "t5" ON (((COALESCE("t0"."i_brand", '0') = COALESCE("t5"."i_brand", '0')) AND (COALESCE("t0"."i_brand", '1') = COALESCE("t5"."i_brand", '1'))) AND ((COALESCE("t0"."i_current_price", 0) = COALESCE("t5"."i_current_price", 0)) AND (COALESCE("t0"."i_current_price", 1) = COALESCE("t5"."i_current_price", 1))) AND ((COALESCE("t0"."i_item_desc", '0') = COALESCE("t5"."i_item_desc", '0')) AND (COALESCE("t0"."i_item_desc", '1') = COALESCE("t5"."i_item_desc", '1'))) AND ("t0"."i_item_sk" = "t5"."i_item_sk") AND ((COALESCE("t0"."i_wholesale_cost", 0) = COALESCE("t5"."i_wholesale_cost", 0)) AND (COALESCE("t0"."i_wholesale_cost", 1) = COALESCE("t5"."i_wholesale_cost", 1))) AND ((COALESCE("t0"."s_store_name", '0') = COALESCE("t5"."s_store_name", '0')) AND (COALESCE("t0"."s_store_name", '1') = COALESCE("t5"."s_store_name", '1'))) AND ((COALESCE("t0"."s_store_sk", 0) = COALESCE("t5"."s_store_sk", 0)) AND (COALESCE("t0"."s_store_sk", 1) = COALESCE("t5"."s_store_sk", 1))))
WHERE ("t5"."x_measure__4" > (10 * "t0"."sum_ss_sales_price"));


-- Query 65 Table and Date Calculation
SELECT SUM("store_sales"."ss_sales_price") AS "temp_tc___33806120",
  MIN("item"."i_brand") AS "temp_attr_i_brand_",
  MAX("item"."i_brand") AS "temp_attr_i_brand1",
  MAX("item"."i_current_price") AS "temp_attr_i_curren",
  MIN("item"."i_current_price") AS "temp_attr_i_curre1",
  MIN("item"."i_item_desc") AS "temp_attr_i_item_d",
  MAX("item"."i_item_desc") AS "temp_attr_i_item_1",
  MAX("item"."i_wholesale_cost") AS "temp_attr_i_wholes",
  MIN("item"."i_wholesale_cost") AS "temp_attr_i_whole1",
  MIN("store"."s_store_name") AS "temp_attr_s_store_",
  MAX("store"."s_store_name") AS "temp_attr_s_store1",
  "item"."i_item_sk" AS "i_item_sk",
  "store"."s_store_sk" AS "s_store_sk"
FROM "store_sales" "store_sales"
  INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")
  INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")
  LEFT JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")
WHERE ((CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1998-01-01 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('1999-01-01 00:00:00' AS TIMESTAMP)) AND (NOT ("store_sales"."ss_store_sk" IS NULL)))
GROUP BY 12,
  13;
  
  


