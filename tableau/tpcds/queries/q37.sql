-- Query 37
SELECT MIN("inventory date"."d_date") AS "temp_none_d_date_q",   MAX("inventory date"."d_date") AS "temp_none_d_date_1",   COUNT(1) AS "x__alias__0" FROM "catalog_sales" "catalog_sales"   LEFT JOIN "inventory" "inventory" ON ("catalog_sales"."cs_item_sk" = "inventory"."inv_item_sk")   LEFT JOIN "date_dim" "inventory date" ON ("inventory"."inv_date_sk" = "inventory date"."d_date_sk") HAVING (COUNT(1) > 0);

-- Query 37
SELECT MIN("item"."i_current_price") AS "temp_none_i_curren",   MAX("item"."i_current_price") AS "temp_none_i_curre1",   COUNT(1) AS "x__alias__0" FROM "item" "item" HAVING (COUNT(1) > 0);

-- Query 37
SELECT "item"."i_current_price" AS "i_current_price",   "item"."i_item_desc" AS "i_item_desc",   "item"."i_item_id" AS "i_item_id" FROM "catalog_sales" "catalog_sales"   INNER JOIN "item" "item" ON ("catalog_sales"."cs_item_sk" = "item"."i_item_sk")   INNER JOIN "inventory" "inventory" ON ("catalog_sales"."cs_item_sk" = "inventory"."inv_item_sk")   INNER JOIN "date_dim" "inventory date" ON ("inventory"."inv_date_sk" = "inventory date"."d_date_sk") WHERE (("item"."i_current_price" >= 68.00) AND ("item"."i_current_price" <= 98.00) AND ("inventory date"."d_date" >= CAST('2000-02-01' AS DATE)) AND ("inventory date"."d_date" <= CAST('2000-04-01' AS DATE)) AND ("item"."i_manufact_id" IN (677, 694, 808, 940)) AND ("inventory"."inv_quantity_on_hand" >= 100) AND ("inventory"."inv_quantity_on_hand" <= 500)) GROUP BY 1,   2,   3;


-- Query 37 Date Calculation
SELECT "item"."i_current_price" AS "i_current_price",
  "item"."i_item_desc" AS "i_item_desc",
  "item"."i_item_id" AS "i_item_id"
FROM "catalog_sales" "catalog_sales"
  INNER JOIN "item" "item" ON ("catalog_sales"."cs_item_sk" = "item"."i_item_sk")
  INNER JOIN "inventory" "inventory" ON ("catalog_sales"."cs_item_sk" = "inventory"."inv_item_sk")
WHERE (("item"."i_current_price" >= 68.00) AND ("item"."i_current_price" <= 98.00) AND (CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('2000-02-01 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-04-02 00:00:00' AS TIMESTAMP)) AND ("item"."i_manufact_id" IN (677, 694, 808, 940)) AND ("inventory"."inv_quantity_on_hand" >= 100) AND ("inventory"."inv_quantity_on_hand" <= 500))
GROUP BY 1,
  2,
  3;

