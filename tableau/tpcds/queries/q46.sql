-- Query 46
SELECT "t0"."c_first_name" AS "c_first_name",   "t0"."c_last_name" AS "c_last_name",   "t0"."ca_city__customer_" AS "ca_city__customer_",   "t0"."ca_city" AS "ca_city",   "t0"."ss_ticket_number" AS "ss_ticket_number",   SUM("t0"."ss_coupon_amt") AS "sum_ss_coupon_amt_",   SUM("t0"."ss_net_profit") AS "sum_ss_net_profit_" FROM "item" "item"   INNER JOIN (   SELECT "store_sales"."ss_item_sk" AS "ss_item_sk",     "customer"."c_first_name" AS "c_first_name",     "customer"."c_last_name" AS "c_last_name",     "current address"."ca_city" AS "ca_city__customer_",     "bought address"."ca_city" AS "ca_city",     "household_demographics"."hd_dep_count" AS "hd_dep_count",     "household_demographics"."hd_vehicle_count" AS "hd_vehicle_count",     "store"."s_city" AS "s_city",     "store_sales"."ss_coupon_amt" AS "ss_coupon_amt",     "store_sales"."ss_net_profit" AS "ss_net_profit",     "store_sales"."ss_promo_sk" AS "ss_promo_sk",     "store_sales"."ss_sold_date_sk" AS "ss_sold_date_sk",     "store_sales"."ss_sold_time_sk" AS "ss_sold_time_sk",     "store_sales"."ss_ticket_number" AS "ss_ticket_number"   FROM "store_sales" "store_sales"     INNER JOIN "customer_address" "bought address" ON ("store_sales"."ss_addr_sk" = "bought address"."ca_address_sk")     LEFT JOIN "customer_demographics" "customer_demographics" ON ("store_sales"."ss_cdemo_sk" = "customer_demographics"."cd_demo_sk")     LEFT JOIN "household_demographics" "household_demographics" ON ("store_sales"."ss_hdemo_sk" = "household_demographics"."hd_demo_sk")     INNER JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")     INNER JOIN "customer" "customer" ON ("store_sales"."ss_customer_sk" = "customer"."c_customer_sk")     INNER JOIN "customer_address" "current address" ON ("customer"."c_current_addr_sk" = "current address"."ca_address_sk") ) "t0" ON ("item"."i_item_sk" = "t0"."ss_item_sk")   LEFT JOIN "promotion" "promotion" ON ("t0"."ss_promo_sk" = "promotion"."p_promo_sk")   LEFT JOIN "time_dim" "time_dim" ON ("t0"."ss_sold_time_sk" = "time_dim"."t_time_sk")   INNER JOIN "date_dim" "date_dim" ON ("t0"."ss_sold_date_sk" = "date_dim"."d_date_sk") WHERE (("t0"."ca_city" <> "t0"."ca_city__customer_") AND (("t0"."hd_dep_count" = 4) OR ("t0"."hd_vehicle_count" = 3)) AND ("date_dim"."d_dow" IN (0, 6)) AND ("date_dim"."d_year" >= 1999) AND ("date_dim"."d_year" <= 2001) AND ("t0"."s_city" IN ('Fairview', 'Midway'))) GROUP BY 1,   2,   3,   4,   5;

-- Query 46
SELECT (("household_demographics"."hd_dep_count" = 4) OR ("household_demographics"."hd_vehicle_count" = 3)) AS "calculation_786013" FROM "store_sales" "store_sales"   LEFT JOIN "household_demographics" "household_demographics" ON ("store_sales"."ss_hdemo_sk" = "household_demographics"."hd_demo_sk")   RIGHT JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk") WHERE (("household_demographics"."hd_dep_count" = 4) OR ("household_demographics"."hd_vehicle_count" = 3)) GROUP BY 1;

-- Query 46
SELECT ("bought address"."ca_city" <> "current address"."ca_city") AS "calculation_842013" FROM "store_sales" "store_sales"   INNER JOIN "customer_address" "bought address" ON ("store_sales"."ss_addr_sk" = "bought address"."ca_address_sk")   INNER JOIN "customer" "customer" ON ("store_sales"."ss_customer_sk" = "customer"."c_customer_sk")   INNER JOIN "customer_address" "current address" ON ("customer"."c_current_addr_sk" = "current address"."ca_address_sk") WHERE ("bought address"."ca_city" <> "current address"."ca_city") GROUP BY 1;


--  Query 46 Date Calculation
SELECT "t0"."c_first_name" AS "c_first_name",
  "t0"."c_last_name" AS "c_last_name",
  "t0"."ca_city__customer_" AS "ca_city__customer_",
  "t0"."ca_city" AS "ca_city",
  "t0"."ss_ticket_number" AS "ss_ticket_number",
  SUM("t0"."ss_coupon_amt") AS "sum_ss_coupon_amt_",
  SUM("t0"."ss_net_profit") AS "sum_ss_net_profit_"
FROM "item" "item"
  INNER JOIN (
  SELECT "store_sales"."ss_item_sk" AS "ss_item_sk",
    "customer"."c_first_name" AS "c_first_name",
    "customer"."c_last_name" AS "c_last_name",
    "current address"."ca_city" AS "ca_city__customer_",
    "bought address"."ca_city" AS "ca_city",
    "household_demographics"."hd_dep_count" AS "hd_dep_count",
    "household_demographics"."hd_vehicle_count" AS "hd_vehicle_count",
    "store"."s_city" AS "s_city",
    "store_sales"."ss_coupon_amt" AS "ss_coupon_amt",
    "store_sales"."ss_net_profit" AS "ss_net_profit",
    "store_sales"."ss_promo_sk" AS "ss_promo_sk",
    "store_sales"."ss_sold_date_sk" AS "ss_sold_date_sk",
    "store_sales"."ss_sold_time_sk" AS "ss_sold_time_sk",
    "store_sales"."ss_ticket_number" AS "ss_ticket_number"
  FROM "store_sales" "store_sales"
    INNER JOIN "customer_address" "bought address" ON ("store_sales"."ss_addr_sk" = "bought address"."ca_address_sk")
    LEFT JOIN "customer_demographics" "customer_demographics" ON ("store_sales"."ss_cdemo_sk" = "customer_demographics"."cd_demo_sk")
    LEFT JOIN "household_demographics" "household_demographics" ON ("store_sales"."ss_hdemo_sk" = "household_demographics"."hd_demo_sk")
    INNER JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")
    INNER JOIN "customer" "customer" ON ("store_sales"."ss_customer_sk" = "customer"."c_customer_sk")
    INNER JOIN "customer_address" "current address" ON ("customer"."c_current_addr_sk" = "current address"."ca_address_sk")
) "t0" ON ("item"."i_item_sk" = "t0"."ss_item_sk")
  LEFT JOIN "promotion" "promotion" ON ("t0"."ss_promo_sk" = "promotion"."p_promo_sk")
  LEFT JOIN "time_dim" "time_dim" ON ("t0"."ss_sold_time_sk" = "time_dim"."t_time_sk")
  LEFT JOIN "date_dim" "date_dim" ON ("t0"."ss_sold_date_sk" = "date_dim"."d_date_sk")
WHERE (("t0"."ca_city" <> "t0"."ca_city__customer_") AND (("t0"."hd_dep_count" = 4) OR ("t0"."hd_vehicle_count" = 3)) AND ("t0"."s_city" IN ('Fairview', 'Midway')) AND ((1 + MOD((MOD((MOD((MOD(DATE_DIFF('DAY', CAST('1995-01-01' AS DATE), CAST(CAST(DATE_ADD('DAY', CAST(("t0"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) AS DATE)), 7) + ABS(7)),7) + 7), 7) + ABS(7)),7)) IN (1, 7)) AND (YEAR(CAST(DATE_ADD('DAY', CAST(("t0"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) IN (1999, 2000, 2001)))
GROUP BY 1,
  2,
  3,
  4,
  5;
