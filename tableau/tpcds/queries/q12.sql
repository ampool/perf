-- Query 12 
SELECT "item"."i_category" AS "i_category" FROM "item" "item" GROUP BY 1 ORDER BY "i_category" ASC;

-- Query 12 
SELECT "item"."i_category" AS "i_category",   "item"."i_class" AS "i_class",   "item"."i_current_price" AS "i_current_price",   "item"."i_item_desc" AS "i_item_desc",   "item"."i_item_id" AS "i_item_id",   SUM("web_sales"."ws_ext_sales_price") AS "sum_ws_ext_sales_p" FROM "web_sales" "web_sales"   INNER JOIN "date_dim" "sold date" ON ("web_sales"."ws_sold_date_sk" = "sold date"."d_date_sk")   INNER JOIN "item" "item" ON ("web_sales"."ws_item_sk" = "item"."i_item_sk")   LEFT JOIN "customer" "customer" ON ("web_sales"."ws_bill_customer_sk" = "customer"."c_customer_sk")   LEFT JOIN "customer_address" "current address" ON ("customer"."c_current_addr_sk" = "current address"."ca_address_sk")   LEFT JOIN "customer_address" "bill address" ON ("web_sales"."ws_bill_addr_sk" = "bill address"."ca_address_sk")   LEFT JOIN "time_dim" "time_dim" ON ("web_sales"."ws_sold_time_sk" = "time_dim"."t_time_sk")   LEFT JOIN "web_page" "web_page" ON ("web_sales"."ws_web_page_sk" = "web_page"."wp_web_page_sk")   LEFT JOIN "household_demographics" "household_demographics" ON ("web_sales"."ws_ship_hdemo_sk" = "household_demographics"."hd_demo_sk")   LEFT JOIN "date_dim" "ship date" ON ("web_sales"."ws_ship_date_sk" = "ship date"."d_date_sk")   LEFT JOIN "ship_mode" "ship_mode" ON ("web_sales"."ws_ship_mode_sk" = "ship_mode"."sm_ship_mode_sk")   LEFT JOIN "warehouse" "warehouse" ON ("web_sales"."ws_warehouse_sk" = "warehouse"."w_warehouse_sk")   LEFT JOIN "customer_address" "ship address" ON ("web_sales"."ws_ship_addr_sk" = "ship address"."ca_address_sk")   LEFT JOIN "web_returns" "web_returns" ON (("web_sales"."ws_order_number" = "web_returns"."wr_order_number") AND ("web_sales"."ws_item_sk" = "web_returns"."wr_item_sk"))   LEFT JOIN "web_site" "web_site" ON ("web_sales"."ws_web_site_sk" = "web_site"."web_site_sk") WHERE (("item"."i_category" IN ('Books                                             ', 'Home                                              ', 'Sports                                            ')) AND ("sold date"."d_date" >= CAST('1999-02-22 00:00:00' AS TIMESTAMP)) AND ("sold date"."d_date" < CAST('1999-03-25 00:00:00' AS TIMESTAMP))) GROUP BY 1,   2,   3,   4,   5;

-- Query 12 Date Calculation
SELECT "item"."i_category" AS "i_category",
  "item"."i_class" AS "i_class",
  "item"."i_current_price" AS "i_current_price",
  "item"."i_item_desc" AS "i_item_desc",
  "item"."i_item_id" AS "i_item_id",
  SUM("web_sales"."ws_ext_sales_price") AS "sum_ws_ext_sales_p"
FROM "web_sales" "web_sales"
  LEFT JOIN "date_dim" "sold date" ON ("web_sales"."ws_sold_date_sk" = "sold date"."d_date_sk")
  INNER JOIN "item" "item" ON ("web_sales"."ws_item_sk" = "item"."i_item_sk")
  LEFT JOIN "customer" "customer" ON ("web_sales"."ws_bill_customer_sk" = "customer"."c_customer_sk")
  LEFT JOIN "customer_address" "current address" ON ("customer"."c_current_addr_sk" = "current address"."ca_address_sk")
  LEFT JOIN "customer_address" "bill address" ON ("web_sales"."ws_bill_addr_sk" = "bill address"."ca_address_sk")
  LEFT JOIN "time_dim" "time_dim" ON ("web_sales"."ws_sold_time_sk" = "time_dim"."t_time_sk")
  LEFT JOIN "web_page" "web_page" ON ("web_sales"."ws_web_page_sk" = "web_page"."wp_web_page_sk")
  LEFT JOIN "household_demographics" "household_demographics" ON ("web_sales"."ws_ship_hdemo_sk" = "household_demographics"."hd_demo_sk")
  LEFT JOIN "date_dim" "ship date" ON ("web_sales"."ws_ship_date_sk" = "ship date"."d_date_sk")
  LEFT JOIN "ship_mode" "ship_mode" ON ("web_sales"."ws_ship_mode_sk" = "ship_mode"."sm_ship_mode_sk")
  LEFT JOIN "warehouse" "warehouse" ON ("web_sales"."ws_warehouse_sk" = "warehouse"."w_warehouse_sk")
  LEFT JOIN "customer_address" "ship address" ON ("web_sales"."ws_ship_addr_sk" = "ship address"."ca_address_sk")
  LEFT JOIN "web_returns" "web_returns" ON (("web_sales"."ws_order_number" = "web_returns"."wr_order_number") AND ("web_sales"."ws_item_sk" = "web_returns"."wr_item_sk"))
  LEFT JOIN "web_site" "web_site" ON ("web_sales"."ws_web_site_sk" = "web_site"."web_site_sk")
WHERE (("item"."i_category" IN ('Books                                             ', 'Home                                              ', 'Sports                                            ')) AND (CAST(DATE_ADD('DAY', CAST(("web_sales"."ws_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('1999-02-22 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("web_sales"."ws_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('1999-03-25 00:00:00' AS TIMESTAMP)))
GROUP BY 1,
  2,
  3,
  4,
  5;
  
  
  

