-- Query 36
SELECT "date_dim"."d_year" AS "d_year" FROM "date_dim" "date_dim" GROUP BY 1;

-- Query 36
SELECT SUM("store_sales"."ss_net_profit") AS "temp_calculation_1",   SUM("store_sales"."ss_ext_sales_price") AS "temp_calculation_2",   "item"."i_category" AS "i_category",   "item"."i_class" AS "i_class" FROM "store_sales" "store_sales"   INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")   INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")   INNER JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk") WHERE (("date_dim"."d_year" = 2001) AND ("store"."s_state" = 'TN')) GROUP BY 3,   4;

-- Query 36
SELECT "item"."i_category" AS "i_category",   "item"."i_class" AS "i_class",   (CASE WHEN SUM("store_sales"."ss_ext_sales_price") = 0 THEN CAST(NULL AS DOUBLE) ELSE SUM("store_sales"."ss_net_profit") / SUM("store_sales"."ss_ext_sales_price") END) AS "usr_calculation_17" FROM "store_sales" "store_sales"   INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")   INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")   INNER JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk") WHERE (("date_dim"."d_year" = 2001) AND ("store"."s_state" = 'TN')) GROUP BY 1,   2;



-- Query 36 Date Calculation
SELECT YEAR(CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) AS "yr_calculation_048" FROM "store_sales" "store_sales" GROUP BY 1;

-- Query 36 Date Calculation
SELECT SUM("store_sales"."ss_net_profit") AS "temp_calculation_1",   SUM("store_sales"."ss_ext_sales_price") AS "temp_calculation_2",   "item"."i_category" AS "i_category",   "item"."i_class" AS "i_class" FROM "store_sales" "store_sales"   INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")   INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")   INNER JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk") WHERE (("store"."s_state" = 'TN') AND (YEAR(CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) = 2001)) GROUP BY 3,   4;

-- Query 36 Date Calculation
SELECT "item"."i_category" AS "i_category",   "item"."i_class" AS "i_class",   (CASE WHEN SUM("store_sales"."ss_ext_sales_price") = 0 THEN CAST(NULL AS DOUBLE) ELSE SUM("store_sales"."ss_net_profit") / SUM("store_sales"."ss_ext_sales_price") END) AS "usr_calculation_17" FROM "store_sales" "store_sales"   INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")   INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")   INNER JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk") WHERE (("store"."s_state" = 'TN') AND (YEAR(CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) = 2001)) GROUP BY 1,   2;
