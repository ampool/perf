-- Query 9
SELECT CAST(CASE WHEN (CASE WHEN 20 = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(("t0"."ss_quantity" + 19) AS DOUBLE) / 20 END) >=0 THEN FLOOR((CASE WHEN 20 = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(("t0"."ss_quantity" + 19) AS DOUBLE) / 20 END)) ELSE CEIL((CASE WHEN 20 = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(("t0"."ss_quantity" + 19) AS DOUBLE) / 20 END)) END AS BIGINT) AS "calculation_422090",
  SUM("t0"."ss_net_paid") AS "temp_calculation_7",
  SUM("t0"."ss_ext_discount_am") AS "temp_calculation_1",
  COUNT("t0"."ss_net_paid") AS "temp_calculation_2",
  MAX((CASE WHEN (CAST(CASE WHEN (CASE WHEN 20 = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(("t0"."ss_quantity" + 19) AS DOUBLE) / 20 END) >=0 THEN FLOOR((CASE WHEN 20 = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(("t0"."ss_quantity" + 19) AS DOUBLE) / 20 END)) ELSE CEIL((CASE WHEN 20 = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(("t0"."ss_quantity" + 19) AS DOUBLE) / 20 END)) END AS BIGINT) = 1) THEN 74129 WHEN (CAST(CASE WHEN (CASE WHEN 20 = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(("t0"."ss_quantity" + 19) AS DOUBLE) / 20 END) >=0 THEN FLOOR((CASE WHEN 20 = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(("t0"."ss_quantity" + 19) AS DOUBLE) / 20 END)) ELSE CEIL((CASE WHEN 20 = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(("t0"."ss_quantity" + 19) AS DOUBLE) / 20 END)) END AS BIGINT) = 2) THEN 122840 WHEN (CAST(CASE WHEN (CASE WHEN 20 = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(("t0"."ss_quantity" + 19) AS DOUBLE) / 20 END) >=0 THEN FLOOR((CASE WHEN 20 = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(("t0"."ss_quantity" + 19) AS DOUBLE) / 20 END)) ELSE CEIL((CASE WHEN 20 = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(("t0"."ss_quantity" + 19) AS DOUBLE) / 20 END)) END AS BIGINT) = 3) THEN 56580 WHEN (CAST(CASE WHEN (CASE WHEN 20 = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(("t0"."ss_quantity" + 19) AS DOUBLE) / 20 END) >=0 THEN FLOOR((CASE WHEN 20 = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(("t0"."ss_quantity" + 19) AS DOUBLE) / 20 END)) ELSE CEIL((CASE WHEN 20 = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(("t0"."ss_quantity" + 19) AS DOUBLE) / 20 END)) END AS BIGINT) = 4) THEN 10097 WHEN (CAST(CASE WHEN (CASE WHEN 20 = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(("t0"."ss_quantity" + 19) AS DOUBLE) / 20 END) >=0 THEN FLOOR((CASE WHEN 20 = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(("t0"."ss_quantity" + 19) AS DOUBLE) / 20 END)) ELSE CEIL((CASE WHEN 20 = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(("t0"."ss_quantity" + 19) AS DOUBLE) / 20 END)) END AS BIGINT) = 5) THEN 165306 ELSE CAST(NULL AS BIGINT) END)) AS "temp_calculation_3",
  SUM(1) AS "temp_calculation_4",
  COUNT("t0"."ss_ext_discount_am") AS "temp_calculation_5"
FROM "item" "item"
  INNER JOIN (
  SELECT "store_sales"."ss_item_sk" AS "ss_item_sk",
    "store_sales"."ss_ext_discount_amt" AS "ss_ext_discount_am",
    "store_sales"."ss_net_paid" AS "ss_net_paid",
    "store_sales"."ss_promo_sk" AS "ss_promo_sk",
    "store_sales"."ss_quantity" AS "ss_quantity",
    "store_sales"."ss_sold_date_sk" AS "ss_sold_date_sk",
    "store_sales"."ss_sold_time_sk" AS "ss_sold_time_sk"
  FROM "store_sales" "store_sales"
    LEFT JOIN "customer_address" "bought address" ON ("store_sales"."ss_addr_sk" = "bought address"."ca_address_sk")
    LEFT JOIN "customer_demographics" "customer_demographics" ON ("store_sales"."ss_cdemo_sk" = "customer_demographics"."cd_demo_sk")
    LEFT JOIN "household_demographics" "household_demographics" ON ("store_sales"."ss_hdemo_sk" = "household_demographics"."hd_demo_sk")
    LEFT JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")
    LEFT JOIN "customer" "customer" ON ("store_sales"."ss_customer_sk" = "customer"."c_customer_sk")
    LEFT JOIN "customer_address" "current address" ON ("customer"."c_current_addr_sk" = "current address"."ca_address_sk")
) "t0" ON ("item"."i_item_sk" = "t0"."ss_item_sk")
  LEFT JOIN "promotion" "promotion" ON ("t0"."ss_promo_sk" = "promotion"."p_promo_sk")
  LEFT JOIN "time_dim" "time_dim" ON ("t0"."ss_sold_time_sk" = "time_dim"."t_time_sk")
  LEFT JOIN "date_dim" "date_dim" ON ("t0"."ss_sold_date_sk" = "date_dim"."d_date_sk")
WHERE (NOT ((CAST(CASE WHEN (CASE WHEN 20 = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(("t0"."ss_quantity" + 19) AS DOUBLE) / 20 END) >=0 THEN FLOOR((CASE WHEN 20 = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(("t0"."ss_quantity" + 19) AS DOUBLE) / 20 END)) ELSE CEIL((CASE WHEN 20 = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(("t0"."ss_quantity" + 19) AS DOUBLE) / 20 END)) END AS BIGINT) IN (0)) OR (CAST(CASE WHEN (CASE WHEN 20 = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(("t0"."ss_quantity" + 19) AS DOUBLE) / 20 END) >=0 THEN FLOOR((CASE WHEN 20 = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(("t0"."ss_quantity" + 19) AS DOUBLE) / 20 END)) ELSE CEIL((CASE WHEN 20 = 0 THEN CAST(NULL AS DOUBLE) ELSE CAST(("t0"."ss_quantity" + 19) AS DOUBLE) / 20 END)) END AS BIGINT) IS NULL)))
GROUP BY 1;
