-- Query 43
SELECT "store"."s_gmt_offset" AS "s_gmt_offset" FROM "store_sales" "store_sales"   LEFT JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk") GROUP BY 1;

-- Query 43
SELECT "date_dim"."d_day_name" AS "d_day_name",   "store"."s_store_id" AS "s_store_id",   "store"."s_store_name" AS "s_store_name",   SUM("store_sales"."ss_sales_price") AS "sum_ss_sales_price" FROM "store_sales" "store_sales"   INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")   INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")   INNER JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk") WHERE (("date_dim"."d_year" = 2000) AND ("store"."s_gmt_offset" = -5.00)) GROUP BY 1,   2,   3;



-- Query 43 Date Calculation
SELECT "store"."s_store_id" AS "s_store_id",   "store"."s_store_name" AS "s_store_name",   SUM("store_sales"."ss_sales_price") AS "sum_ss_sales_price",   (1 + MOD((MOD((MOD((MOD(DATE_DIFF('DAY', CAST('1995-01-01' AS DATE), CAST(CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) AS DATE)), 7) + ABS(7)),7) + 7), 7) + ABS(7)),7)) AS "wd_calculation_048" FROM "store_sales" "store_sales"   INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")   INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")   INNER JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk") WHERE (("store"."s_gmt_offset" = -5.00) AND (YEAR(CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) = 2000)) GROUP BY 1,   2,   4;
