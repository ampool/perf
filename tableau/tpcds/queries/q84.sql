-- Query 84
SELECT MIN("income_band"."ib_lower_bound") AS "temp_none_ib_lower",   MAX("income_band"."ib_lower_bound") AS "temp_none_ib_lowe1",   COUNT(1) AS "x__alias__0" FROM "income_band" "income_band" HAVING (COUNT(1) > 0);

-- Query 84
SELECT MIN("income_band"."ib_upper_bound") AS "temp_none_ib_upper",   MAX("income_band"."ib_upper_bound") AS "temp_none_ib_uppe1",   COUNT(1) AS "x__alias__0" FROM "income_band" "income_band" HAVING (COUNT(1) > 0);


-- Query 84
SELECT "customer_address"."ca_city" AS "ca_city" FROM "customer_address" "customer_address" GROUP BY 1 ORDER BY "ca_city" ASC;

-- Query 84
SELECT "customer"."c_customer_id" AS "c_customer_id",   "customer"."c_first_name" AS "c_first_name",   "customer"."c_last_name" AS "c_last_name",   "store_returns"."sr_ticket_number" AS "sr_ticket_number" FROM "store_returns" "store_returns"   INNER JOIN "customer_demographics" "customer_demographics" ON ("store_returns"."sr_cdemo_sk" = "customer_demographics"."cd_demo_sk")   INNER JOIN "customer" "customer" ON ("customer_demographics"."cd_demo_sk" = "customer"."c_current_cdemo_sk")   INNER JOIN "customer_address" "customer_address" ON ("customer"."c_current_addr_sk" = "customer_address"."ca_address_sk")   INNER JOIN "household_demographics" "household_demographics" ON ("customer"."c_current_hdemo_sk" = "household_demographics"."hd_demo_sk")   INNER JOIN "income_band" "income_band" ON ("household_demographics"."hd_income_band_sk" = "income_band"."ib_income_band_sk") WHERE (("customer_address"."ca_city" = 'Edgewood') AND ("income_band"."ib_lower_bound" >= 38128) AND ("income_band"."ib_upper_bound" <= 88128));
