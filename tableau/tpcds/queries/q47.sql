-- Query 47 Level of Detail Expression
SELECT "date_dim"."d_moy" AS "d_moy" FROM "date_dim" "date_dim" GROUP BY 1 LIMIT 24;


-- Query 47 Level of Detail Expression
SELECT "t3"."x_measure__3" AS "temp_calculation_3",   "t0"."temp_calculation_8" AS "temp_calculation_8",   "t0"."d_moy" AS "d_moy",   "t0"."d_year" AS "d_year",   "t0"."i_brand" AS "i_brand",   "t0"."i_category" AS "i_category",   "t0"."s_company_name" AS "s_company_name",   "t0"."s_store_name" AS "s_store_name" FROM (   SELECT "date_dim"."d_moy" AS "d_moy",     "date_dim"."d_year" AS "d_year",     "item"."i_brand" AS "i_brand",     "item"."i_category" AS "i_category",     "store"."s_company_name" AS "s_company_name",     "store"."s_store_name" AS "s_store_name",     SUM("store_sales"."ss_sales_price") AS "temp_calculation_8"   FROM "store_sales" "store_sales"     INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")     INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")     INNER JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")   WHERE (((("date_dim"."d_year" = 1998) AND ("date_dim"."d_moy" = 12)) OR ("date_dim"."d_year" = 1999) OR (("date_dim"."d_year" = 2000) AND ("date_dim"."d_moy" = 1))) AND (NOT ("item"."i_brand" IS NULL)) AND (NOT ("item"."i_category" IS NULL)) AND (NOT ("store"."s_company_name" IS NULL)) AND (NOT ("store"."s_store_name" IS NULL)))   GROUP BY 1,     2,     3,     4,     5,     6 ) "t0"   INNER JOIN (   SELECT "t1"."d_year" AS "d_year",     "t1"."i_brand" AS "i_brand",     "t1"."i_category" AS "i_category",     "t1"."s_company_name" AS "s_company_name",     "t1"."s_store_name" AS "s_store_name",     AVG("t2"."x_measure__1") AS "x_measure__3"   FROM (     SELECT "date_dim"."d_moy" AS "d_moy",       "date_dim"."d_year" AS "d_year",       "item"."i_brand" AS "i_brand",       "item"."i_category" AS "i_category",       "store"."s_company_name" AS "s_company_name",       "store"."s_store_name" AS "s_store_name"     FROM "store_sales" "store_sales"       INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")       INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")       INNER JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")     WHERE ((NOT ("item"."i_brand" IS NULL)) AND (NOT ("item"."i_category" IS NULL)) AND (NOT ("store"."s_company_name" IS NULL)) AND (NOT ("store"."s_store_name" IS NULL)) AND ((("date_dim"."d_year" = 1998) AND ("date_dim"."d_moy" = 12)) OR ("date_dim"."d_year" = 1999) OR (("date_dim"."d_year" = 2000) AND ("date_dim"."d_moy" = 1))))     GROUP BY 1,       2,       3,       4,       5,       6   ) "t1"     INNER JOIN (     SELECT "date_dim"."d_moy" AS "d_moy",       "date_dim"."d_year" AS "d_year",       "item"."i_brand" AS "i_brand",       "item"."i_category" AS "i_category",       "store"."s_company_name" AS "s_company_name",       "store"."s_store_name" AS "s_store_name",       SUM("store_sales"."ss_sales_price") AS "x_measure__1"     FROM "store_sales" "store_sales"       INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")       INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")       INNER JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")     WHERE (((("date_dim"."d_year" = 1998) AND ("date_dim"."d_moy" = 12)) OR ("date_dim"."d_year" = 1999) OR (("date_dim"."d_year" = 2000) AND ("date_dim"."d_moy" = 1))) AND (NOT ("item"."i_brand" IS NULL)) AND (NOT ("item"."i_category" IS NULL)) AND (NOT ("store"."s_company_name" IS NULL)) AND (NOT ("store"."s_store_name" IS NULL)))     GROUP BY 1,       2,       3,       4,       5,       6   ) "t2" ON (((COALESCE("t1"."d_moy", 0) = COALESCE("t2"."d_moy", 0)) AND (COALESCE("t1"."d_moy", 1) = COALESCE("t2"."d_moy", 1))) AND ((COALESCE("t1"."d_year", 0) = COALESCE("t2"."d_year", 0)) AND (COALESCE("t1"."d_year", 1) = COALESCE("t2"."d_year", 1))) AND ("t1"."i_brand" = "t2"."i_brand") AND ("t1"."i_category" = "t2"."i_category") AND ("t1"."s_company_name" = "t2"."s_company_name") AND ("t1"."s_store_name" = "t2"."s_store_name"))   GROUP BY 1,     2,     3,     4,     5 ) "t3" ON (((COALESCE("t0"."d_year", 0) = COALESCE("t3"."d_year", 0)) AND (COALESCE("t0"."d_year", 1) = COALESCE("t3"."d_year", 1))) AND ("t0"."i_brand" = "t3"."i_brand") AND ("t0"."i_category" = "t3"."i_category") AND ("t0"."s_company_name" = "t3"."s_company_name") AND ("t0"."s_store_name" = "t3"."s_store_name"));

-- Query 47 Level of Detail Expression
SELECT "t0"."d_moy" AS "d_moy",   "t0"."d_year" AS "d_year",   "t0"."i_brand" AS "i_brand",   "t0"."i_category" AS "i_category",   "t0"."s_company_name" AS "s_company_name",   "t0"."s_store_name" AS "s_store_name",   ("t0"."x_measure__0" - "t3"."x_measure__3") AS "usr_sales_compared" FROM (   SELECT "date_dim"."d_moy" AS "d_moy",     "date_dim"."d_year" AS "d_year",     "item"."i_brand" AS "i_brand",     "item"."i_category" AS "i_category",     "store"."s_company_name" AS "s_company_name",     "store"."s_store_name" AS "s_store_name",     SUM("store_sales"."ss_sales_price") AS "x_measure__0"   FROM "store_sales" "store_sales"     INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")     INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")     INNER JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")   WHERE (((("date_dim"."d_year" = 1998) AND ("date_dim"."d_moy" = 12)) OR ("date_dim"."d_year" = 1999) OR (("date_dim"."d_year" = 2000) AND ("date_dim"."d_moy" = 1))) AND (NOT ("item"."i_brand" IS NULL)) AND (NOT ("item"."i_category" IS NULL)) AND (NOT ("store"."s_company_name" IS NULL)) AND (NOT ("store"."s_store_name" IS NULL)))   GROUP BY 1,     2,     3,     4,     5,     6 ) "t0"   INNER JOIN (   SELECT "t1"."d_year" AS "d_year",     "t1"."i_brand" AS "i_brand",     "t1"."i_category" AS "i_category",     "t1"."s_company_name" AS "s_company_name",     "t1"."s_store_name" AS "s_store_name",     AVG("t2"."x_measure__0") AS "x_measure__3"   FROM (     SELECT "date_dim"."d_moy" AS "d_moy",       "date_dim"."d_year" AS "d_year",       "item"."i_brand" AS "i_brand",       "item"."i_category" AS "i_category",       "store"."s_company_name" AS "s_company_name",       "store"."s_store_name" AS "s_store_name"     FROM "store_sales" "store_sales"       INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")       INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")       INNER JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")     WHERE ((NOT ("item"."i_brand" IS NULL)) AND (NOT ("item"."i_category" IS NULL)) AND (NOT ("store"."s_company_name" IS NULL)) AND (NOT ("store"."s_store_name" IS NULL)) AND ((("date_dim"."d_year" = 1998) AND ("date_dim"."d_moy" = 12)) OR ("date_dim"."d_year" = 1999) OR (("date_dim"."d_year" = 2000) AND ("date_dim"."d_moy" = 1))))     GROUP BY 1,       2,       3,       4,       5,       6   ) "t1"     INNER JOIN (     SELECT "date_dim"."d_moy" AS "d_moy",       "date_dim"."d_year" AS "d_year",       "item"."i_brand" AS "i_brand",       "item"."i_category" AS "i_category",       "store"."s_company_name" AS "s_company_name",       "store"."s_store_name" AS "s_store_name",       SUM("store_sales"."ss_sales_price") AS "x_measure__0"     FROM "store_sales" "store_sales"       INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")       INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")       INNER JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")     WHERE (((("date_dim"."d_year" = 1998) AND ("date_dim"."d_moy" = 12)) OR ("date_dim"."d_year" = 1999) OR (("date_dim"."d_year" = 2000) AND ("date_dim"."d_moy" = 1))) AND (NOT ("item"."i_brand" IS NULL)) AND (NOT ("item"."i_category" IS NULL)) AND (NOT ("store"."s_company_name" IS NULL)) AND (NOT ("store"."s_store_name" IS NULL)))     GROUP BY 1,       2,       3,       4,       5,       6   ) "t2" ON (((COALESCE("t1"."d_moy", 0) = COALESCE("t2"."d_moy", 0)) AND (COALESCE("t1"."d_moy", 1) = COALESCE("t2"."d_moy", 1))) AND ((COALESCE("t1"."d_year", 0) = COALESCE("t2"."d_year", 0)) AND (COALESCE("t1"."d_year", 1) = COALESCE("t2"."d_year", 1))) AND ("t1"."i_brand" = "t2"."i_brand") AND ("t1"."i_category" = "t2"."i_category") AND ("t1"."s_company_name" = "t2"."s_company_name") AND ("t1"."s_store_name" = "t2"."s_store_name"))   GROUP BY 1,     2,     3,     4,     5 ) "t3" ON (((COALESCE("t0"."d_year", 0) = COALESCE("t3"."d_year", 0)) AND (COALESCE("t0"."d_year", 1) = COALESCE("t3"."d_year", 1))) AND ("t0"."i_brand" = "t3"."i_brand") AND ("t0"."i_category" = "t3"."i_category") AND ("t0"."s_company_name" = "t3"."s_company_name") AND ("t0"."s_store_name" = "t3"."s_store_name"));

-- -- Query 47 Level of Detail Expression
SELECT "date_dim"."d_moy" AS "d_moy",   "date_dim"."d_year" AS "d_year" FROM "date_dim" "date_dim" WHERE ((("date_dim"."d_year" = 1998) AND ("date_dim"."d_moy" = 12)) OR ("date_dim"."d_year" = 1999) OR (("date_dim"."d_year" = 2000) AND ("date_dim"."d_moy" = 1))) GROUP BY 1,   2 ORDER BY "d_year" ASC,   "d_moy" ASC;


-- Query 47 Table Calculation
SELECT "t3"."x_measure__3" AS "temp_sales_compar1",
  "t0"."temp_sales_compare" AS "temp_sales_compare",
  "t0"."d_moy" AS "d_moy",
  "t0"."d_year" AS "d_year",
  "t0"."i_brand" AS "i_brand",
  "t0"."i_category" AS "i_category",
  "t0"."s_company_name" AS "s_company_name",
  "t0"."s_store_name" AS "s_store_name"
FROM (
  SELECT "date_dim"."d_moy" AS "d_moy",
    "date_dim"."d_year" AS "d_year",
    "item"."i_brand" AS "i_brand",
    "item"."i_category" AS "i_category",
    "store"."s_company_name" AS "s_company_name",
    "store"."s_store_name" AS "s_store_name",
    SUM("store_sales"."ss_sales_price") AS "temp_sales_compare"
  FROM "store_sales" "store_sales"
    INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")
    INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")
    INNER JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")
  WHERE (((("date_dim"."d_year" = 1998) AND ("date_dim"."d_moy" = 12)) OR ("date_dim"."d_year" = 1999) OR (("date_dim"."d_year" = 2000) AND ("date_dim"."d_moy" = 1))) AND (NOT ("item"."i_brand" IS NULL)) AND (NOT ("item"."i_category" IS NULL)) AND (NOT ("store"."s_company_name" IS NULL)) AND (NOT ("store"."s_store_name" IS NULL)))
  GROUP BY 1,
    2,
    3,
    4,
    5,
    6
) "t0"
  INNER JOIN (
  SELECT "t1"."d_year" AS "d_year",
    "t1"."i_brand" AS "i_brand",
    "t1"."i_category" AS "i_category",
    "t1"."s_company_name" AS "s_company_name",
    "t1"."s_store_name" AS "s_store_name",
    AVG("t2"."x_measure__1") AS "x_measure__3"
  FROM (
    SELECT "date_dim"."d_moy" AS "d_moy",
      "date_dim"."d_year" AS "d_year",
      "item"."i_brand" AS "i_brand",
      "item"."i_category" AS "i_category",
      "store"."s_company_name" AS "s_company_name",
      "store"."s_store_name" AS "s_store_name"
    FROM "store_sales" "store_sales"
      INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")
      INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")
      INNER JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")
    WHERE ((NOT ("item"."i_brand" IS NULL)) AND (NOT ("item"."i_category" IS NULL)) AND (NOT ("store"."s_company_name" IS NULL)) AND (NOT ("store"."s_store_name" IS NULL)) AND ((("date_dim"."d_year" = 1998) AND ("date_dim"."d_moy" = 12)) OR ("date_dim"."d_year" = 1999) OR (("date_dim"."d_year" = 2000) AND ("date_dim"."d_moy" = 1))))
    GROUP BY 1,
      2,
      3,
      4,
      5,
      6
  ) "t1"
    INNER JOIN (
    SELECT "date_dim"."d_moy" AS "d_moy",
      "date_dim"."d_year" AS "d_year",
      "item"."i_brand" AS "i_brand",
      "item"."i_category" AS "i_category",
      "store"."s_company_name" AS "s_company_name",
      "store"."s_store_name" AS "s_store_name",
      SUM("store_sales"."ss_sales_price") AS "x_measure__1"
    FROM "store_sales" "store_sales"
      INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")
      INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")
      INNER JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk")
    WHERE (((("date_dim"."d_year" = 1998) AND ("date_dim"."d_moy" = 12)) OR ("date_dim"."d_year" = 1999) OR (("date_dim"."d_year" = 2000) AND ("date_dim"."d_moy" = 1))) AND (NOT ("item"."i_brand" IS NULL)) AND (NOT ("item"."i_category" IS NULL)) AND (NOT ("store"."s_company_name" IS NULL)) AND (NOT ("store"."s_store_name" IS NULL)))
    GROUP BY 1,
      2,
      3,
      4,
      5,
      6
  ) "t2" ON (((COALESCE("t1"."d_moy", 0) = COALESCE("t2"."d_moy", 0)) AND (COALESCE("t1"."d_moy", 1) = COALESCE("t2"."d_moy", 1))) AND ((COALESCE("t1"."d_year", 0) = COALESCE("t2"."d_year", 0)) AND (COALESCE("t1"."d_year", 1) = COALESCE("t2"."d_year", 1))) AND ("t1"."i_brand" = "t2"."i_brand") AND ("t1"."i_category" = "t2"."i_category") AND ("t1"."s_company_name" = "t2"."s_company_name") AND ("t1"."s_store_name" = "t2"."s_store_name"))
  GROUP BY 1,
    2,
    3,
    4,
    5
) "t3" ON (((COALESCE("t0"."d_year", 0) = COALESCE("t3"."d_year", 0)) AND (COALESCE("t0"."d_year", 1) = COALESCE("t3"."d_year", 1))) AND ("t0"."i_brand" = "t3"."i_brand") AND ("t0"."i_category" = "t3"."i_category") AND ("t0"."s_company_name" = "t3"."s_company_name") AND ("t0"."s_store_name" = "t3"."s_store_name"));
