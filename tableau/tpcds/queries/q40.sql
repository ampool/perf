-- Query 40
SELECT MIN("sold date"."d_date") AS "temp_none_d_date_q",   MAX("sold date"."d_date") AS "temp_none_d_date_1",   COUNT(1) AS "x__alias__0" FROM "catalog_sales" "catalog_sales"   LEFT JOIN "date_dim" "sold date" ON ("catalog_sales"."cs_sold_date_sk" = "sold date"."d_date_sk") HAVING (COUNT(1) > 0);

-- Query 40
SELECT "item"."i_item_id" AS "i_item_id",   SUM((CASE WHEN ("sold date"."d_date" < CAST('2000-03-11' AS DATE)) THEN ("catalog_sales"."cs_sales_price" - COALESCE(CAST("catalog_returns"."cr_refunded_cash" AS DOUBLE), 0.0)) ELSE 0. END)) AS "sum_calculation_87",   SUM((CASE WHEN ("sold date"."d_date" >= CAST('2000-03-11' AS DATE)) THEN ("catalog_sales"."cs_sales_price" - COALESCE(CAST("catalog_returns"."cr_refunded_cash" AS DOUBLE), 0.0)) ELSE 0. END)) AS "sum_sales_before__",   "warehouse"."w_state" AS "w_state__warehouse" FROM "catalog_sales" "catalog_sales"   INNER JOIN "item" "item" ON ("catalog_sales"."cs_item_sk" = "item"."i_item_sk")   LEFT JOIN "customer_demographics" "sales customer demographics" ON ("catalog_sales"."cs_bill_cdemo_sk" = "sales customer demographics"."cd_demo_sk")   INNER JOIN "date_dim" "sold date" ON ("catalog_sales"."cs_sold_date_sk" = "sold date"."d_date_sk")   LEFT JOIN "date_dim" "ship date" ON ("catalog_sales"."cs_ship_date_sk" = "ship date"."d_date_sk")   LEFT JOIN "promotion" "promotion" ON ("catalog_sales"."cs_promo_sk" = "promotion"."p_promo_sk")   LEFT JOIN "catalog_returns" "catalog_returns" ON (("catalog_sales"."cs_item_sk" = "catalog_returns"."cr_item_sk") AND ("catalog_sales"."cs_order_number" = "catalog_returns"."cr_order_number"))   LEFT JOIN "call_center" "call_center" ON ("catalog_sales"."cs_call_center_sk" = "call_center"."cc_call_center_sk")   LEFT JOIN "ship_mode" "ship_mode" ON ("catalog_sales"."cs_ship_mode_sk" = "ship_mode"."sm_ship_mode_sk")   LEFT JOIN "warehouse" "warehouse" ON ("catalog_sales"."cs_warehouse_sk" = "warehouse"."w_warehouse_sk")   LEFT JOIN "customer" "customer" ON ("catalog_sales"."cs_bill_customer_sk" = "customer"."c_customer_sk")   LEFT JOIN "customer_address" "customer_address" ON ("customer"."c_current_addr_sk" = "customer_address"."ca_address_sk")   LEFT JOIN "customer_demographics" "current customer demographics" ON ("customer"."c_current_cdemo_sk" = "current customer demographics"."cd_demo_sk") WHERE ((NOT ("catalog_sales"."cs_warehouse_sk" IS NULL)) AND ("sold date"."d_date" >= CAST('2000-02-10' AS DATE)) AND ("sold date"."d_date" <= CAST('2000-04-10' AS DATE)) AND ("item"."i_current_price" >= 0.99) AND ("item"."i_current_price" <= 1.49)) GROUP BY 1,   4;


-- Query 40 Date Calculation
SELECT "item"."i_item_id" AS "i_item_id",
  SUM((CASE WHEN (CAST(DATE_ADD('DAY', CAST(("catalog_sales"."cs_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('2000-03-11' AS DATE)) THEN ("catalog_sales"."cs_sales_price" - COALESCE(CAST("catalog_returns"."cr_refunded_cash" AS DOUBLE), 0.0)) ELSE 0. END)) AS "sum_sales_after__c",
  SUM((CASE WHEN (CAST(DATE_ADD('DAY', CAST(("catalog_sales"."cs_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-03-11' AS DATE)) THEN ("catalog_sales"."cs_sales_price" - COALESCE(CAST("catalog_returns"."cr_refunded_cash" AS DOUBLE), 0.0)) ELSE 0. END)) AS "sum_sales_before__",
  "warehouse"."w_state" AS "w_state__warehouse"
FROM "catalog_sales" "catalog_sales"
  INNER JOIN "item" "item" ON ("catalog_sales"."cs_item_sk" = "item"."i_item_sk")
  LEFT JOIN "customer_demographics" "sales customer demographics" ON ("catalog_sales"."cs_bill_cdemo_sk" = "sales customer demographics"."cd_demo_sk")
  LEFT JOIN "date_dim" "sold date" ON ("catalog_sales"."cs_sold_date_sk" = "sold date"."d_date_sk")
  LEFT JOIN "date_dim" "ship date" ON ("catalog_sales"."cs_ship_date_sk" = "ship date"."d_date_sk")
  LEFT JOIN "promotion" "promotion" ON ("catalog_sales"."cs_promo_sk" = "promotion"."p_promo_sk")
  LEFT JOIN "catalog_returns" "catalog_returns" ON (("catalog_sales"."cs_item_sk" = "catalog_returns"."cr_item_sk") AND ("catalog_sales"."cs_order_number" = "catalog_returns"."cr_order_number"))
  LEFT JOIN "call_center" "call_center" ON ("catalog_sales"."cs_call_center_sk" = "call_center"."cc_call_center_sk")
  LEFT JOIN "ship_mode" "ship_mode" ON ("catalog_sales"."cs_ship_mode_sk" = "ship_mode"."sm_ship_mode_sk")
  LEFT JOIN "warehouse" "warehouse" ON ("catalog_sales"."cs_warehouse_sk" = "warehouse"."w_warehouse_sk")
  LEFT JOIN "customer" "customer" ON ("catalog_sales"."cs_bill_customer_sk" = "customer"."c_customer_sk")
  LEFT JOIN "customer_address" "customer_address" ON ("customer"."c_current_addr_sk" = "customer_address"."ca_address_sk")
  LEFT JOIN "customer_demographics" "current customer demographics" ON ("customer"."c_current_cdemo_sk" = "current customer demographics"."cd_demo_sk")
WHERE ((CAST(DATE_ADD('DAY', CAST(("catalog_sales"."cs_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) >= CAST('2000-02-10 00:00:00' AS TIMESTAMP)) AND (CAST(DATE_ADD('DAY', CAST(("catalog_sales"."cs_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE) < CAST('2000-04-11 00:00:00' AS TIMESTAMP)) AND (NOT ("catalog_sales"."cs_warehouse_sk" IS NULL)) AND ("item"."i_current_price" >= 0.99) AND ("item"."i_current_price" <= 1.49))
GROUP BY 1,
  4;
