-- Query 55
SELECT "item"."i_manager_id" AS "i_manager_id" FROM "item" "item" GROUP BY 1;

-- Query 55
SELECT "item"."i_brand" AS "i_brand",   "item"."i_brand_id" AS "i_brand_id",   SUM("store_sales"."ss_ext_sales_price") AS "sum_ss_ext_sales_p" FROM "store_sales" "store_sales"   INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")   INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")   INNER JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk") WHERE (("date_dim"."d_moy" = 11) AND ("date_dim"."d_year" = 1999) AND ("item"."i_manager_id" = 28)) GROUP BY 1,   2;


-- Query 55 Date Calculation
SELECT ((YEAR(CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) * 100) + MONTH(CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE))) AS "my_calculation_048" FROM "store_sales" "store_sales" GROUP BY 1;


-- Query 55 Date Calculation
SELECT "item"."i_brand" AS "i_brand",   "item"."i_brand_id" AS "i_brand_id",   SUM("store_sales"."ss_ext_sales_price") AS "sum_ss_ext_sales_p" FROM "store_sales" "store_sales"   INNER JOIN "date_dim" "date_dim" ON ("store_sales"."ss_sold_date_sk" = "date_dim"."d_date_sk")   INNER JOIN "item" "item" ON ("store_sales"."ss_item_sk" = "item"."i_item_sk")   INNER JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk") WHERE (("item"."i_manager_id" = 28) AND (((YEAR(CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) * 100) + MONTH(CAST(DATE_ADD('DAY', CAST(("store_sales"."ss_sold_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE))) = 199911)) GROUP BY 1,   2;
