-- Query 50
SELECT "date_dim"."d_moy" AS "d_moy" FROM "date_dim" "date_dim" GROUP BY 1;

-- Query 50
SELECT "date_dim"."d_year" AS "d_year" FROM "date_dim" "date_dim" GROUP BY 1;


SELECT FLOOR((CASE WHEN ((("store_returns"."sr_returned_date_sk" - "store_sales"."ss_sold_date_sk") - 1) > 120) THEN 120 ELSE (("store_returns"."sr_returned_date_sk" - "store_sales"."ss_sold_date_sk") - 1) END) / 30) AS "return_lag__bin_",   "store"."s_company_id" AS "s_company_id",   "store"."s_county" AS "s_county",   "store"."s_state" AS "s_state",   "store"."s_store_name" AS "s_store_name",   "store"."s_street_name" AS "s_street_name",   "store"."s_street_number" AS "s_street_number",   "store"."s_street_type" AS "s_street_type",   "store"."s_suite_number" AS "s_suite_number",   "store"."s_zip" AS "s_zip",   SUM(1) AS "sum_number_of_reco" FROM "store_sales" "store_sales"   INNER JOIN "store_returns" "store_returns" ON (("store_sales"."ss_customer_sk" = "store_returns"."sr_customer_sk") AND ("store_sales"."ss_item_sk" = "store_returns"."sr_item_sk") AND ("store_sales"."ss_ticket_number" = "store_returns"."sr_ticket_number"))   INNER JOIN "date_dim" "date_dim" ON ("store_returns"."sr_returned_date_sk" = "date_dim"."d_date_sk")   INNER JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk") WHERE ((NOT (FLOOR((CASE WHEN ((("store_returns"."sr_returned_date_sk" - "store_sales"."ss_sold_date_sk") - 1) > 120) THEN 120 ELSE (("store_returns"."sr_returned_date_sk" - "store_sales"."ss_sold_date_sk") - 1) END) / 30) IS NULL)) AND ("date_dim"."d_moy" = 8) AND ("date_dim"."d_year" = 2001)) GROUP BY 1,   2,   3,   4,   5,   6,   7,   8,   9,   10;


-- Query 50 Date Calculation
SELECT YEAR(CAST(DATE_ADD('DAY', CAST(("store_returns"."sr_returned_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) AS "yr_calculation_048" FROM "store_returns" "store_returns" GROUP BY 1;


-- Query 50 Date Calculation
SELECT MONTH(CAST(DATE_ADD('DAY', CAST(("store_returns"."sr_returned_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) AS "mn_calculation_048" FROM "store_returns" "store_returns" GROUP BY 1;


-- Query 50 Date Calculation
SELECT FLOOR((CASE WHEN ((("store_returns"."sr_returned_date_sk" - "store_sales"."ss_sold_date_sk") - 1) > 120) THEN 120 ELSE (("store_returns"."sr_returned_date_sk" - "store_sales"."ss_sold_date_sk") - 1) END) / 30) AS "return_lag__bin_",   "store"."s_company_id" AS "s_company_id",   "store"."s_county" AS "s_county",   "store"."s_state" AS "s_state",   "store"."s_store_name" AS "s_store_name",   "store"."s_street_name" AS "s_street_name",   "store"."s_street_number" AS "s_street_number",   "store"."s_street_type" AS "s_street_type",   "store"."s_suite_number" AS "s_suite_number",   "store"."s_zip" AS "s_zip",   SUM(1) AS "sum_number_of_reco" FROM "store_sales" "store_sales"   INNER JOIN "store_returns" "store_returns" ON (("store_sales"."ss_customer_sk" = "store_returns"."sr_customer_sk") AND ("store_sales"."ss_item_sk" = "store_returns"."sr_item_sk") AND ("store_sales"."ss_ticket_number" = "store_returns"."sr_ticket_number"))   INNER JOIN "date_dim" "date_dim" ON ("store_returns"."sr_returned_date_sk" = "date_dim"."d_date_sk")   INNER JOIN "store" "store" ON ("store_sales"."ss_store_sk" = "store"."s_store_sk") WHERE ((NOT (FLOOR((CASE WHEN ((("store_returns"."sr_returned_date_sk" - "store_sales"."ss_sold_date_sk") - 1) > 120) THEN 120 ELSE (("store_returns"."sr_returned_date_sk" - "store_sales"."ss_sold_date_sk") - 1) END) / 30) IS NULL)) AND (MONTH(CAST(DATE_ADD('DAY', CAST(("store_returns"."sr_returned_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) = 8) AND (YEAR(CAST(DATE_ADD('DAY', CAST(("store_returns"."sr_returned_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) = 2001)) GROUP BY 1,   2,   3,   4,   5,   6,   7,   8,   9,   10;
