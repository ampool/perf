-- Query 39.1 Table Calculation
SELECT "date_dim"."d_year" AS "d_year" FROM "date_dim" "date_dim" GROUP BY 1;

-- Query 39.1 Table Calculation
SELECT SUM((CAST("inventory"."inv_quantity_on_hand" AS DOUBLE) * CAST("inventory"."inv_quantity_on_hand" AS DOUBLE))) AS "temp_calculation_1",   COUNT("inventory"."inv_quantity_on_hand") AS "temp_calculation_2",   SUM("inventory"."inv_quantity_on_hand") AS "temp_calculation_3",   AVG(CAST("inventory"."inv_quantity_on_hand" AS DOUBLE)) AS "avg_inv_quantity_o",   "date_dim"."d_moy" AS "d_moy",   "item"."i_item_sk" AS "i_item_sk",   STDDEV("inventory"."inv_quantity_on_hand") AS "std_inv_quantity_o",   "warehouse"."w_warehouse_sk" AS "w_warehouse_sk" FROM "inventory" "inventory"   INNER JOIN "warehouse" "warehouse" ON ("inventory"."inv_warehouse_sk" = "warehouse"."w_warehouse_sk")   INNER JOIN "item" "item" ON ("inventory"."inv_item_sk" = "item"."i_item_sk")   INNER JOIN "date_dim" "date_dim" ON ("inventory"."inv_date_sk" = "date_dim"."d_date_sk") WHERE (("date_dim"."d_moy" IN (1, 2)) AND ("date_dim"."d_year" = 2001)) GROUP BY 5,   6,   8 HAVING ((CASE WHEN AVG(CAST("inventory"."inv_quantity_on_hand" AS DOUBLE)) = 0 THEN CAST(NULL AS DOUBLE) ELSE STDDEV("inventory"."inv_quantity_on_hand") / AVG(CAST("inventory"."inv_quantity_on_hand" AS DOUBLE)) END) >= 0.99999999999999001);



-- Query 39.1 Level of Detail Expression
SELECT SUM((CAST("inventory"."inv_quantity_on_hand" AS DOUBLE) * CAST("inventory"."inv_quantity_on_hand" AS DOUBLE))) AS "temp_calculation_1",
  COUNT("inventory"."inv_quantity_on_hand") AS "temp_calculation_2",
  SUM("inventory"."inv_quantity_on_hand") AS "temp_calculation_3",
  AVG(CAST("inventory"."inv_quantity_on_hand" AS DOUBLE)) AS "avg_inv_quantity_o",
  "date_dim"."d_moy" AS "d_moy",
  "item"."i_item_sk" AS "i_item_sk",
  STDDEV("inventory"."inv_quantity_on_hand") AS "std_inv_quantity_o",
  "warehouse"."w_warehouse_sk" AS "w_warehouse_sk"
FROM "inventory" "inventory"
  INNER JOIN "warehouse" "warehouse" ON ("inventory"."inv_warehouse_sk" = "warehouse"."w_warehouse_sk")
  INNER JOIN "item" "item" ON ("inventory"."inv_item_sk" = "item"."i_item_sk")
  INNER JOIN "date_dim" "date_dim" ON ("inventory"."inv_date_sk" = "date_dim"."d_date_sk")
  INNER JOIN (
  SELECT "warehouse"."w_warehouse_sk" AS "w_warehouse_sk",
    "item"."i_item_sk" AS "i_item_sk"
  FROM "inventory" "inventory"
    INNER JOIN "warehouse" "warehouse" ON ("inventory"."inv_warehouse_sk" = "warehouse"."w_warehouse_sk")
    INNER JOIN "item" "item" ON ("inventory"."inv_item_sk" = "item"."i_item_sk")
    INNER JOIN "date_dim" "date_dim" ON ("inventory"."inv_date_sk" = "date_dim"."d_date_sk")
    INNER JOIN (
    SELECT "t0"."i_item_sk" AS "i_item_sk",
      "t0"."w_warehouse_sk" AS "w_warehouse_sk",
      COUNT(CASE WHEN ((((CASE WHEN "t0"."x_measure__2" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__1" / "t0"."x_measure__2" END) > 1) AND true) OR ((NOT ((CASE WHEN "t0"."x_measure__2" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__1" / "t0"."x_measure__2" END) > 1)) AND CAST(NULL AS BOOLEAN))) THEN 1 WHEN NOT ((((CASE WHEN "t0"."x_measure__2" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__1" / "t0"."x_measure__2" END) > 1) AND true) OR ((NOT ((CASE WHEN "t0"."x_measure__2" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__1" / "t0"."x_measure__2" END) > 1)) AND CAST(NULL AS BOOLEAN))) THEN 0 ELSE CAST(NULL AS BIGINT) END) AS "x_measure__4"
    FROM (
      SELECT "item"."i_item_sk" AS "i_item_sk",
        "warehouse"."w_warehouse_sk" AS "w_warehouse_sk",
        STDDEV("inventory"."inv_quantity_on_hand") AS "x_measure__1",
        AVG(CAST("inventory"."inv_quantity_on_hand" AS DOUBLE)) AS "x_measure__2"
      FROM "inventory" "inventory"
        INNER JOIN "warehouse" "warehouse" ON ("inventory"."inv_warehouse_sk" = "warehouse"."w_warehouse_sk")
        INNER JOIN "item" "item" ON ("inventory"."inv_item_sk" = "item"."i_item_sk")
        INNER JOIN "date_dim" "date_dim" ON ("inventory"."inv_date_sk" = "date_dim"."d_date_sk")
      WHERE (("date_dim"."d_moy" IN (1, 2)) AND ("date_dim"."d_year" = 2001))
      GROUP BY "date_dim"."d_moy",
        1,
        2
    ) "t0"
    GROUP BY 1,
      2
    HAVING (COUNT(CASE WHEN ((((CASE WHEN "t0"."x_measure__2" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__1" / "t0"."x_measure__2" END) > 1) AND true) OR ((NOT ((CASE WHEN "t0"."x_measure__2" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__1" / "t0"."x_measure__2" END) > 1)) AND CAST(NULL AS BOOLEAN))) THEN 1 WHEN NOT ((((CASE WHEN "t0"."x_measure__2" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__1" / "t0"."x_measure__2" END) > 1) AND true) OR ((NOT ((CASE WHEN "t0"."x_measure__2" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__1" / "t0"."x_measure__2" END) > 1)) AND CAST(NULL AS BOOLEAN))) THEN 0 ELSE CAST(NULL AS BIGINT) END) = 2)
  ) "t1" ON (("item"."i_item_sk" = "t1"."i_item_sk") AND ("warehouse"."w_warehouse_sk" = "t1"."w_warehouse_sk"))
  WHERE (("date_dim"."d_moy" IN (1, 2)) AND ("date_dim"."d_year" = 2001))
  GROUP BY 2,
    1
) "t2" ON (("warehouse"."w_warehouse_sk" = "t2"."w_warehouse_sk") AND ("item"."i_item_sk" = "t2"."i_item_sk"))
WHERE (("date_dim"."d_moy" IN (1, 2)) AND ("date_dim"."d_year" = 2001))
GROUP BY 5,
  6,
  8;

  
  
--Query 39.1 Table and Date Calculation  
SELECT YEAR(CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) AS "yr_calculation_111" FROM "inventory" "inventory" GROUP BY 1;

--Query 39.1 Table and Date Calculation  
SELECT SUM((CAST("inventory"."inv_quantity_on_hand" AS DOUBLE) * CAST("inventory"."inv_quantity_on_hand" AS DOUBLE))) AS "temp_calculation_1",   COUNT("inventory"."inv_quantity_on_hand") AS "temp_calculation_2",   SUM("inventory"."inv_quantity_on_hand") AS "temp_calculation_3",   AVG(CAST("inventory"."inv_quantity_on_hand" AS DOUBLE)) AS "avg_inv_quantity_o",   "item"."i_item_sk" AS "i_item_sk",   MONTH(CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) AS "mn_calculation_111",   STDDEV("inventory"."inv_quantity_on_hand") AS "std_inv_quantity_o",   "warehouse"."w_warehouse_sk" AS "w_warehouse_sk" FROM "inventory" "inventory"   INNER JOIN "warehouse" "warehouse" ON ("inventory"."inv_warehouse_sk" = "warehouse"."w_warehouse_sk")   INNER JOIN "item" "item" ON ("inventory"."inv_item_sk" = "item"."i_item_sk")   INNER JOIN "date_dim" "date_dim" ON ("inventory"."inv_date_sk" = "date_dim"."d_date_sk") WHERE ((MONTH(CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) IN (1, 2)) AND (YEAR(CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) = 2001)) GROUP BY 5,   6,   8 HAVING ((CASE WHEN AVG(CAST("inventory"."inv_quantity_on_hand" AS DOUBLE)) = 0 THEN CAST(NULL AS DOUBLE) ELSE STDDEV("inventory"."inv_quantity_on_hand") / AVG(CAST("inventory"."inv_quantity_on_hand" AS DOUBLE)) END) >= 0.99999999999999001);


-- Query 39.1 Level of Detail Expression Date Calculation
SELECT SUM((CAST("inventory"."inv_quantity_on_hand" AS DOUBLE) * CAST("inventory"."inv_quantity_on_hand" AS DOUBLE))) AS "temp_calculation_1",
  COUNT("inventory"."inv_quantity_on_hand") AS "temp_calculation_2",
  SUM("inventory"."inv_quantity_on_hand") AS "temp_calculation_3",
  AVG(CAST("inventory"."inv_quantity_on_hand" AS DOUBLE)) AS "avg_inv_quantity_o",
  "item"."i_item_sk" AS "i_item_sk",
  MONTH(CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) AS "mn_calculation_111",
  STDDEV("inventory"."inv_quantity_on_hand") AS "std_inv_quantity_o",
  "warehouse"."w_warehouse_sk" AS "w_warehouse_sk"
FROM "inventory" "inventory"
  INNER JOIN "warehouse" "warehouse" ON ("inventory"."inv_warehouse_sk" = "warehouse"."w_warehouse_sk")
  INNER JOIN "item" "item" ON ("inventory"."inv_item_sk" = "item"."i_item_sk")
  INNER JOIN "date_dim" "date_dim" ON ("inventory"."inv_date_sk" = "date_dim"."d_date_sk")
  INNER JOIN (
  SELECT "t0"."w_warehouse_sk" AS "w_warehouse_sk",
    "t0"."i_item_sk" AS "i_item_sk",
    COUNT(CASE WHEN ((((CASE WHEN "t0"."x_measure__3" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__2" / "t0"."x_measure__3" END) > 1) AND true) OR ((NOT ((CASE WHEN "t0"."x_measure__3" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__2" / "t0"."x_measure__3" END) > 1)) AND CAST(NULL AS BOOLEAN))) THEN 1 WHEN NOT ((((CASE WHEN "t0"."x_measure__3" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__2" / "t0"."x_measure__3" END) > 1) AND true) OR ((NOT ((CASE WHEN "t0"."x_measure__3" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__2" / "t0"."x_measure__3" END) > 1)) AND CAST(NULL AS BOOLEAN))) THEN 0 ELSE CAST(NULL AS BIGINT) END) AS "x_measure__5"
  FROM (
    SELECT "item"."i_item_sk" AS "i_item_sk",
      "warehouse"."w_warehouse_sk" AS "w_warehouse_sk",
      STDDEV("inventory"."inv_quantity_on_hand") AS "x_measure__2",
      AVG(CAST("inventory"."inv_quantity_on_hand" AS DOUBLE)) AS "x_measure__3"
    FROM "inventory" "inventory"
      INNER JOIN "warehouse" "warehouse" ON ("inventory"."inv_warehouse_sk" = "warehouse"."w_warehouse_sk")
      INNER JOIN "item" "item" ON ("inventory"."inv_item_sk" = "item"."i_item_sk")
      INNER JOIN "date_dim" "date_dim" ON ("inventory"."inv_date_sk" = "date_dim"."d_date_sk")
    WHERE ((MONTH(CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) IN (1, 2)) AND (YEAR(CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) = 2001))
    GROUP BY MONTH(CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)),
      1,
      2
  ) "t0"
  GROUP BY 2,
    1
  HAVING (COUNT(CASE WHEN ((((CASE WHEN "t0"."x_measure__3" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__2" / "t0"."x_measure__3" END) > 1) AND true) OR ((NOT ((CASE WHEN "t0"."x_measure__3" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__2" / "t0"."x_measure__3" END) > 1)) AND CAST(NULL AS BOOLEAN))) THEN 1 WHEN NOT ((((CASE WHEN "t0"."x_measure__3" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__2" / "t0"."x_measure__3" END) > 1) AND true) OR ((NOT ((CASE WHEN "t0"."x_measure__3" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__2" / "t0"."x_measure__3" END) > 1)) AND CAST(NULL AS BOOLEAN))) THEN 0 ELSE CAST(NULL AS BIGINT) END) = 2)
) "t1" ON (("warehouse"."w_warehouse_sk" = "t1"."w_warehouse_sk") AND ("item"."i_item_sk" = "t1"."i_item_sk"))
WHERE ((MONTH(CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) IN (1, 2)) AND (YEAR(CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) = 2001))
GROUP BY 5,
  6,
  8;

  
  
-- Query 39.2 Table Calculation
SELECT SUM((CAST("inventory"."inv_quantity_on_hand" AS DOUBLE) * CAST("inventory"."inv_quantity_on_hand" AS DOUBLE))) AS "temp_calculation_1",
  COUNT("inventory"."inv_quantity_on_hand") AS "temp_calculation_2",
  SUM("inventory"."inv_quantity_on_hand") AS "temp_calculation_3",
  AVG(CAST("inventory"."inv_quantity_on_hand" AS DOUBLE)) AS "avg_inv_quantity_o",
  "date_dim"."d_moy" AS "d_moy",
  "item"."i_item_sk" AS "i_item_sk",
  STDDEV("inventory"."inv_quantity_on_hand") AS "std_inv_quantity_o",
  "warehouse"."w_warehouse_sk" AS "w_warehouse_sk"
FROM "inventory" "inventory"
  INNER JOIN "warehouse" "warehouse" ON ("inventory"."inv_warehouse_sk" = "warehouse"."w_warehouse_sk")
  INNER JOIN "item" "item" ON ("inventory"."inv_item_sk" = "item"."i_item_sk")
  INNER JOIN "date_dim" "date_dim" ON ("inventory"."inv_date_sk" = "date_dim"."d_date_sk")
WHERE (("date_dim"."d_moy" IN (1, 2)) AND ("date_dim"."d_year" = 2001))
GROUP BY 5,
  6,
  8
HAVING ((CASE WHEN AVG(CAST("inventory"."inv_quantity_on_hand" AS DOUBLE)) = 0 THEN CAST(NULL AS DOUBLE) ELSE STDDEV("inventory"."inv_quantity_on_hand") / AVG(CAST("inventory"."inv_quantity_on_hand" AS DOUBLE)) END) > (CASE WHEN ("date_dim"."d_moy" = 1) THEN 1.5 ELSE 1. END));


-- Query 39.2 Level of Detail Expression
SELECT SUM((CAST("inventory"."inv_quantity_on_hand" AS DOUBLE) * CAST("inventory"."inv_quantity_on_hand" AS DOUBLE))) AS "temp_calculation_1",
  COUNT("inventory"."inv_quantity_on_hand") AS "temp_calculation_2",
  SUM("inventory"."inv_quantity_on_hand") AS "temp_calculation_3",
  AVG(CAST("inventory"."inv_quantity_on_hand" AS DOUBLE)) AS "avg_inv_quantity_o",
  "date_dim"."d_moy" AS "d_moy",
  "item"."i_item_sk" AS "i_item_sk",
  STDDEV("inventory"."inv_quantity_on_hand") AS "std_inv_quantity_o",
  "warehouse"."w_warehouse_sk" AS "w_warehouse_sk"
FROM "inventory" "inventory"
  INNER JOIN "warehouse" "warehouse" ON ("inventory"."inv_warehouse_sk" = "warehouse"."w_warehouse_sk")
  INNER JOIN "item" "item" ON ("inventory"."inv_item_sk" = "item"."i_item_sk")
  INNER JOIN "date_dim" "date_dim" ON ("inventory"."inv_date_sk" = "date_dim"."d_date_sk")
  INNER JOIN (
  SELECT "warehouse"."w_warehouse_sk" AS "w_warehouse_sk",
    "item"."i_item_sk" AS "i_item_sk"
  FROM "inventory" "inventory"
    INNER JOIN "warehouse" "warehouse" ON ("inventory"."inv_warehouse_sk" = "warehouse"."w_warehouse_sk")
    INNER JOIN "item" "item" ON ("inventory"."inv_item_sk" = "item"."i_item_sk")
    INNER JOIN "date_dim" "date_dim" ON ("inventory"."inv_date_sk" = "date_dim"."d_date_sk")
    INNER JOIN (
    SELECT "t0"."i_item_sk" AS "i_item_sk",
      "t0"."w_warehouse_sk" AS "w_warehouse_sk",
      COUNT(CASE WHEN ((((CASE WHEN "t0"."x_measure__2" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__1" / "t0"."x_measure__2" END) > (CASE WHEN ("t0"."d_moy" = 1) THEN 1.5 ELSE 1. END)) AND true) OR ((NOT ((CASE WHEN "t0"."x_measure__2" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__1" / "t0"."x_measure__2" END) > (CASE WHEN ("t0"."d_moy" = 1) THEN 1.5 ELSE 1. END))) AND CAST(NULL AS BOOLEAN))) THEN 1 WHEN NOT ((((CASE WHEN "t0"."x_measure__2" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__1" / "t0"."x_measure__2" END) > (CASE WHEN ("t0"."d_moy" = 1) THEN 1.5 ELSE 1. END)) AND true) OR ((NOT ((CASE WHEN "t0"."x_measure__2" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__1" / "t0"."x_measure__2" END) > (CASE WHEN ("t0"."d_moy" = 1) THEN 1.5 ELSE 1. END))) AND CAST(NULL AS BOOLEAN))) THEN 0 ELSE CAST(NULL AS BIGINT) END) AS "x_measure__5"
    FROM (
      SELECT "date_dim"."d_moy" AS "d_moy",
        "item"."i_item_sk" AS "i_item_sk",
        "warehouse"."w_warehouse_sk" AS "w_warehouse_sk",
        STDDEV("inventory"."inv_quantity_on_hand") AS "x_measure__1",
        AVG(CAST("inventory"."inv_quantity_on_hand" AS DOUBLE)) AS "x_measure__2"
      FROM "inventory" "inventory"
        INNER JOIN "warehouse" "warehouse" ON ("inventory"."inv_warehouse_sk" = "warehouse"."w_warehouse_sk")
        INNER JOIN "item" "item" ON ("inventory"."inv_item_sk" = "item"."i_item_sk")
        INNER JOIN "date_dim" "date_dim" ON ("inventory"."inv_date_sk" = "date_dim"."d_date_sk")
      WHERE (("date_dim"."d_moy" IN (1, 2)) AND ("date_dim"."d_year" = 2001))
      GROUP BY 1,
        2,
        3
    ) "t0"
    GROUP BY 1,
      2
    HAVING (COUNT(CASE WHEN ((((CASE WHEN "t0"."x_measure__2" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__1" / "t0"."x_measure__2" END) > (CASE WHEN ("t0"."d_moy" = 1) THEN 1.5 ELSE 1. END)) AND true) OR ((NOT ((CASE WHEN "t0"."x_measure__2" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__1" / "t0"."x_measure__2" END) > (CASE WHEN ("t0"."d_moy" = 1) THEN 1.5 ELSE 1. END))) AND CAST(NULL AS BOOLEAN))) THEN 1 WHEN NOT ((((CASE WHEN "t0"."x_measure__2" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__1" / "t0"."x_measure__2" END) > (CASE WHEN ("t0"."d_moy" = 1) THEN 1.5 ELSE 1. END)) AND true) OR ((NOT ((CASE WHEN "t0"."x_measure__2" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__1" / "t0"."x_measure__2" END) > (CASE WHEN ("t0"."d_moy" = 1) THEN 1.5 ELSE 1. END))) AND CAST(NULL AS BOOLEAN))) THEN 0 ELSE CAST(NULL AS BIGINT) END) = 2)
  ) "t1" ON (("item"."i_item_sk" = "t1"."i_item_sk") AND ("warehouse"."w_warehouse_sk" = "t1"."w_warehouse_sk"))
  WHERE (("date_dim"."d_moy" IN (1, 2)) AND ("date_dim"."d_year" = 2001))
  GROUP BY 2,
    1
) "t2" ON (("warehouse"."w_warehouse_sk" = "t2"."w_warehouse_sk") AND ("item"."i_item_sk" = "t2"."i_item_sk"))
WHERE (("date_dim"."d_moy" IN (1, 2)) AND ("date_dim"."d_year" = 2001))
GROUP BY 5,
  6,
  8;

  
  
-- Query 39.2 Table and Date Calculation
SELECT SUM((CAST("inventory"."inv_quantity_on_hand" AS DOUBLE) * CAST("inventory"."inv_quantity_on_hand" AS DOUBLE))) AS "temp_calculation_1",
  COUNT("inventory"."inv_quantity_on_hand") AS "temp_calculation_2",
  SUM("inventory"."inv_quantity_on_hand") AS "temp_calculation_3",
  AVG(CAST("inventory"."inv_quantity_on_hand" AS DOUBLE)) AS "avg_inv_quantity_o",
  "item"."i_item_sk" AS "i_item_sk",
  MONTH(CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) AS "mn_calculation_111",
  STDDEV("inventory"."inv_quantity_on_hand") AS "std_inv_quantity_o",
  "warehouse"."w_warehouse_sk" AS "w_warehouse_sk",
  MAX((CASE WHEN ("date_dim"."d_moy" = 1) THEN 1.5 ELSE 1. END)) AS "x_measure__1"
FROM "inventory" "inventory"
  INNER JOIN "warehouse" "warehouse" ON ("inventory"."inv_warehouse_sk" = "warehouse"."w_warehouse_sk")
  INNER JOIN "item" "item" ON ("inventory"."inv_item_sk" = "item"."i_item_sk")
  INNER JOIN "date_dim" "date_dim" ON ("inventory"."inv_date_sk" = "date_dim"."d_date_sk")
WHERE ((MONTH(CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) IN (1, 2)) AND (YEAR(CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) = 2001))
GROUP BY 5,
  6,
  8
HAVING ((CASE WHEN AVG(CAST("inventory"."inv_quantity_on_hand" AS DOUBLE)) = 0 THEN CAST(NULL AS DOUBLE) ELSE STDDEV("inventory"."inv_quantity_on_hand") / AVG(CAST("inventory"."inv_quantity_on_hand" AS DOUBLE)) END) > MAX((CASE WHEN ("date_dim"."d_moy" = 1) THEN 1.5 ELSE 1. END)));


-- Query 39.2 Level of Detail Expression Date Calculation
SELECT SUM((CAST("inventory"."inv_quantity_on_hand" AS DOUBLE) * CAST("inventory"."inv_quantity_on_hand" AS DOUBLE))) AS "temp_calculation_1",
  COUNT("inventory"."inv_quantity_on_hand") AS "temp_calculation_2",
  SUM("inventory"."inv_quantity_on_hand") AS "temp_calculation_3",
  AVG(CAST("inventory"."inv_quantity_on_hand" AS DOUBLE)) AS "avg_inv_quantity_o",
  "item"."i_item_sk" AS "i_item_sk",
  MONTH(CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) AS "mn_calculation_111",
  STDDEV("inventory"."inv_quantity_on_hand") AS "std_inv_quantity_o",
  "warehouse"."w_warehouse_sk" AS "w_warehouse_sk"
FROM "inventory" "inventory"
  INNER JOIN "warehouse" "warehouse" ON ("inventory"."inv_warehouse_sk" = "warehouse"."w_warehouse_sk")
  INNER JOIN "item" "item" ON ("inventory"."inv_item_sk" = "item"."i_item_sk")
  INNER JOIN "date_dim" "date_dim" ON ("inventory"."inv_date_sk" = "date_dim"."d_date_sk")
  INNER JOIN (
  SELECT "t0"."w_warehouse_sk" AS "w_warehouse_sk",
    "t0"."i_item_sk" AS "i_item_sk",
    COUNT(CASE WHEN ((((CASE WHEN "t0"."x_measure__3" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__2" / "t0"."x_measure__3" END) > "t0"."x_measure__5") AND true) OR ((NOT ((CASE WHEN "t0"."x_measure__3" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__2" / "t0"."x_measure__3" END) > "t0"."x_measure__5")) AND CAST(NULL AS BOOLEAN))) THEN 1 WHEN NOT ((((CASE WHEN "t0"."x_measure__3" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__2" / "t0"."x_measure__3" END) > "t0"."x_measure__5") AND true) OR ((NOT ((CASE WHEN "t0"."x_measure__3" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__2" / "t0"."x_measure__3" END) > "t0"."x_measure__5")) AND CAST(NULL AS BOOLEAN))) THEN 0 ELSE CAST(NULL AS BIGINT) END) AS "x_measure__7"
  FROM (
    SELECT "item"."i_item_sk" AS "i_item_sk",
      "warehouse"."w_warehouse_sk" AS "w_warehouse_sk",
      STDDEV("inventory"."inv_quantity_on_hand") AS "x_measure__2",
      AVG(CAST("inventory"."inv_quantity_on_hand" AS DOUBLE)) AS "x_measure__3",
      MIN((CASE WHEN ("date_dim"."d_moy" = 1) THEN 1.5 ELSE 1. END)) AS "x_measure__5"
    FROM "inventory" "inventory"
      INNER JOIN "warehouse" "warehouse" ON ("inventory"."inv_warehouse_sk" = "warehouse"."w_warehouse_sk")
      INNER JOIN "item" "item" ON ("inventory"."inv_item_sk" = "item"."i_item_sk")
      INNER JOIN "date_dim" "date_dim" ON ("inventory"."inv_date_sk" = "date_dim"."d_date_sk")
    WHERE ((MONTH(CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) IN (1, 2)) AND (YEAR(CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) = 2001))
    GROUP BY MONTH(CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)),
      1,
      2
  ) "t0"
  GROUP BY 2,
    1
  HAVING (COUNT(CASE WHEN ((((CASE WHEN "t0"."x_measure__3" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__2" / "t0"."x_measure__3" END) > "t0"."x_measure__5") AND true) OR ((NOT ((CASE WHEN "t0"."x_measure__3" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__2" / "t0"."x_measure__3" END) > "t0"."x_measure__5")) AND CAST(NULL AS BOOLEAN))) THEN 1 WHEN NOT ((((CASE WHEN "t0"."x_measure__3" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__2" / "t0"."x_measure__3" END) > "t0"."x_measure__5") AND true) OR ((NOT ((CASE WHEN "t0"."x_measure__3" = 0 THEN CAST(NULL AS DOUBLE) ELSE "t0"."x_measure__2" / "t0"."x_measure__3" END) > "t0"."x_measure__5")) AND CAST(NULL AS BOOLEAN))) THEN 0 ELSE CAST(NULL AS BIGINT) END) = 2)
) "t1" ON (("warehouse"."w_warehouse_sk" = "t1"."w_warehouse_sk") AND ("item"."i_item_sk" = "t1"."i_item_sk"))
WHERE ((MONTH(CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) IN (1, 2)) AND (YEAR(CAST(DATE_ADD('DAY', CAST(("inventory"."inv_date_sk" - 2415021) AS BIGINT), CAST(CAST('1900-01-01' AS DATE) AS TIMESTAMP)) AS DATE)) = 2001))
GROUP BY 5,
  6,
  8;
