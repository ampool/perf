#!/usr/bin/env bash
TIMEFORMAT="%R"
#!/bin/bash
for file in $2/*.sql; do
    QUERY=$(basename "$file")
    #echo "${QUERY}"
    echo "Running query ${file}"
   (time java -jar /opt/ampool/presto-cli-executable.jar --server localhost:9291 --catalog ampool --schema $1 --file $file ) 2<&1 > $3/${QUERY}.out
done

