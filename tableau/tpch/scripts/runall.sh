#!/usr/bin/env bash
./run.sh tpch002 1 | xargs echo > run1tpch2.timings
sed -i 's/ Q/\nQ/g' run1tpch2.timings
./run.sh tpch002 2 | xargs echo > run2tpch2.timings
sed -i 's/ Q/\nQ/g' run2tpch2.timings
./run.sh tpch002 3 | xargs echo > run3tpch2.timings
sed -i 's/ Q/\nQ/g' run3tpch2.timings
./run.sh tpch010 1 | xargs echo > run1tpch10.timings
sed -i 's/ Q/\nQ/g' run1tpch10.timings
./run.sh tpch010 2 | xargs echo > run2tpch10.timings
sed -i 's/ Q/\nQ/g' run2tpch10.timings
./run.sh tpch010 3 | xargs echo > run3tpch10.timings
sed -i 's/ Q/\nQ/g' run3tpch10.timings
./run.sh tpch100 1 | xargs echo > run1tpch100.timings
sed -i 's/ Q/\nQ/g' run1tpch100.timings
./run.sh tpch100 2 | xargs echo > run2tpch100.timings
sed -i 's/ Q/\nQ/g' run2tpch100.timings
./run.sh tpch100 3 | xargs echo > run3tpch100.timings
sed -i 's/ Q/\nQ/g' run3tpch100.timings

