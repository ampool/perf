#!/usr/bin/env bash
TIMEFORMAT="%R"
for i in {9,18,21}
do
 echo "Query$i:"
(time java -jar /opt/ampool/presto-cli-executable.jar --server localhost:9291 --catalog ampool --schema $1 --file dnq$i.sql ) 2<&1 > tpch${1}results${2}.out
done
