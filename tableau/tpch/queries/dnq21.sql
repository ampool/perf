-- query 21.1
SELECT "lineitem"."l_orderkey" AS "l_orderkey__copy_"
FROM "lineitem" "lineitem"
WHERE ("lineitem"."l_orderkey" IS NULL)
LIMIT 1;

-- query 21.2
SELECT "dnm"."s_name" AS "s_name"
FROM "tpch_min_d" "dnm"
  INNER JOIN (
  SELECT "dnm"."o_orderkey" AS "none_l_orderkey__c",
    COUNT(DISTINCT (CASE WHEN ("dnm"."l_commitdate" < "dnm"."l_receiptdate") THEN "dnm"."ps_suppkey" ELSE CAST(NULL AS BIGINT) END)) AS "x_measure__0"
  FROM "tpch_min_d" "dnm"
  GROUP BY 1
  HAVING (COUNT(DISTINCT (CASE WHEN ("dnm"."l_commitdate" < "dnm"."l_receiptdate") THEN "dnm"."ps_suppkey" ELSE CAST(NULL AS BIGINT) END)) = 1)
) "t0" ON ("dnm"."o_orderkey" = "t0"."none_l_orderkey__c")
  INNER JOIN (
  SELECT "dnm"."o_orderkey" AS "none_l_orderkey_ok",
    COUNT(DISTINCT "dnm"."ps_suppkey") AS "x_measure__1"
  FROM "tpch_min_d" "dnm"
  GROUP BY 1
  HAVING (COUNT(DISTINCT "dnm"."ps_suppkey") > 1)
) "t1" ON ("dnm"."o_orderkey" = "t1"."none_l_orderkey_ok")
WHERE (("dnm"."l_commitdate" < "dnm"."l_receiptdate") AND ("dnm"."n_name" = 'SAUDI ARABIA') AND ("dnm"."o_orderstatus" = 'F') AND ("dnm"."s_name" IS NULL))
LIMIT 1;



--query 21.2
SELECT "dnm"."s_name" AS "s_name",
  SUM(1) AS "sum_number_of_reco"
FROM "tpch_min_d" "dnm"
  INNER JOIN (
  SELECT "dnm"."o_orderkey" AS "none_l_orderkey__c",
    COUNT(DISTINCT (CASE WHEN ("dnm"."l_commitdate" < "dnm"."l_receiptdate") THEN "dnm"."ps_suppkey" ELSE CAST(NULL AS BIGINT) END)) AS "x_measure__0"
  FROM "tpch_min_d" "dnm"
  GROUP BY 1
  HAVING (COUNT(DISTINCT (CASE WHEN ("dnm"."l_commitdate" < "dnm"."l_receiptdate") THEN "dnm"."ps_suppkey" ELSE CAST(NULL AS BIGINT) END)) = 1)
) "t0" ON ("dnm"."o_orderkey" = "t0"."none_l_orderkey__c")
  INNER JOIN (
  SELECT "dnm"."o_orderkey" AS "none_l_orderkey_ok",
    COUNT(DISTINCT "dnm"."ps_suppkey") AS "x_measure__1"
  FROM "tpch_min_d" "dnm"
  GROUP BY 1
  HAVING (COUNT(DISTINCT "dnm"."ps_suppkey") > 1)
) "t1" ON ("dnm"."o_orderkey" = "t1"."none_l_orderkey_ok")
WHERE (("dnm"."l_commitdate" < "dnm"."l_receiptdate") AND ("dnm"."n_name" = 'SAUDI ARABIA') AND ("dnm"."o_orderstatus" = 'F'))
GROUP BY 1
ORDER BY "sum_number_of_reco" DESC,
  "s_name" ASC
LIMIT 100;


