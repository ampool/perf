-- Query 7.1
SELECT "n2"."n_name" AS "n_name"
FROM "nation" "n2"
GROUP BY 1
ORDER BY "n_name" ASC;

-- Query 7.2
SELECT "n1"."n_name" AS "n_name__nation1_"
FROM "nation" "n1"
GROUP BY 1
ORDER BY "n_name__nation1_" ASC;

-- Query 7.3
SELECT "n1"."n_name" AS "n_name__nation1_",
  "n2"."n_name" AS "n_name",
  SUM(("lineitem"."l_extendedprice" * (1 - "lineitem"."l_discount"))) AS "sum_disc_price_ok",
  YEAR("lineitem"."l_shipdate") AS "yr_l_shipdate_ok"
FROM "lineitem" "lineitem"
  INNER JOIN "orders" "orders" ON ("lineitem"."l_orderkey" = "orders"."o_orderkey")
  INNER JOIN "customer" "customer" ON ("orders"."o_custkey" = "customer"."c_custkey")
  INNER JOIN "nation" "n1" ON ("customer"."c_nationkey" = "n1"."n_nationkey")
  INNER JOIN "partsupp" "partsupp" ON (("lineitem"."l_partkey" = "partsupp"."ps_partkey") AND ("lineitem"."l_suppkey" = "partsupp"."ps_suppkey"))
  INNER JOIN "supplier" "supplier" ON ("partsupp"."ps_suppkey" = "supplier"."s_suppkey")
  INNER JOIN "nation" "n2" ON ("supplier"."s_nationkey" = "n2"."n_nationkey")
WHERE (("n1"."n_name" IN ('FRANCE', 'GERMANY')) AND ("n2"."n_name" IN ('FRANCE', 'GERMANY')) AND ("customer"."c_nationkey" <> "supplier"."s_nationkey") AND ("lineitem"."l_shipdate" >= CAST('1995-01-01' AS DATE)) AND ("lineitem"."l_shipdate" <= CAST('1996-12-31' AS DATE)))
GROUP BY 1,
  2,
  4;
