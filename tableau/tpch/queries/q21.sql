-- query 21.1
SELECT "lineitem"."l_orderkey" AS "l_orderkey__copy_"
FROM "lineitem" "lineitem"
WHERE ("lineitem"."l_orderkey" IS NULL)
LIMIT 1;

-- query 21.2
SELECT "supplier"."s_name" AS "s_name"
FROM "lineitem" "lineitem"
  INNER JOIN "orders" "orders" ON ("lineitem"."l_orderkey" = "orders"."o_orderkey")
  INNER JOIN "partsupp" "partsupp" ON (("lineitem"."l_partkey" = "partsupp"."ps_partkey") AND ("lineitem"."l_suppkey" = "partsupp"."ps_suppkey"))
  INNER JOIN "supplier" "supplier" ON ("partsupp"."ps_suppkey" = "supplier"."s_suppkey")
  INNER JOIN "nation" "n2" ON ("supplier"."s_nationkey" = "n2"."n_nationkey")
  INNER JOIN (
  SELECT "lineitem"."l_orderkey" AS "none_l_orderkey__c",
    COUNT(DISTINCT (CASE WHEN ("lineitem"."l_commitdate" < "lineitem"."l_receiptdate") THEN "lineitem"."l_suppkey" ELSE CAST(NULL AS BIGINT) END)) AS "x_measure__0"
  FROM "lineitem" "lineitem"
  GROUP BY 1
  HAVING (COUNT(DISTINCT (CASE WHEN ("lineitem"."l_commitdate" < "lineitem"."l_receiptdate") THEN "lineitem"."l_suppkey" ELSE CAST(NULL AS BIGINT) END)) = 1)
) "t0" ON ("lineitem"."l_orderkey" = "t0"."none_l_orderkey__c")
  INNER JOIN (
  SELECT "lineitem"."l_orderkey" AS "none_l_orderkey_ok",
    COUNT(DISTINCT "lineitem"."l_suppkey") AS "x_measure__1"
  FROM "lineitem" "lineitem"
  GROUP BY 1
  HAVING (COUNT(DISTINCT "lineitem"."l_suppkey") > 1)
) "t1" ON ("lineitem"."l_orderkey" = "t1"."none_l_orderkey_ok")
WHERE (("lineitem"."l_commitdate" < "lineitem"."l_receiptdate") AND ("n2"."n_name" = 'SAUDI ARABIA') AND ("orders"."o_orderstatus" = 'F') AND ("supplier"."s_name" IS NULL))
LIMIT 1;

--query 21.2
SELECT "supplier"."s_name" AS "s_name",
  SUM(1) AS "sum_number_of_reco"
FROM "lineitem" "lineitem"
  INNER JOIN "orders" "orders" ON ("lineitem"."l_orderkey" = "orders"."o_orderkey")
  INNER JOIN "partsupp" "partsupp" ON (("lineitem"."l_partkey" = "partsupp"."ps_partkey") AND ("lineitem"."l_suppkey" = "partsupp"."ps_suppkey"))
  INNER JOIN "supplier" "supplier" ON ("partsupp"."ps_suppkey" = "supplier"."s_suppkey")
  INNER JOIN "nation" "n2" ON ("supplier"."s_nationkey" = "n2"."n_nationkey")
  INNER JOIN (
  SELECT "lineitem"."l_orderkey" AS "none_l_orderkey__c",
    COUNT(DISTINCT (CASE WHEN ("lineitem"."l_commitdate" < "lineitem"."l_receiptdate") THEN "lineitem"."l_suppkey" ELSE CAST(NULL AS BIGINT) END)) AS "x_measure__0"
  FROM "lineitem" "lineitem"
  GROUP BY 1
  HAVING (COUNT(DISTINCT (CASE WHEN ("lineitem"."l_commitdate" < "lineitem"."l_receiptdate") THEN "lineitem"."l_suppkey" ELSE CAST(NULL AS BIGINT) END)) = 1)
) "t0" ON ("lineitem"."l_orderkey" = "t0"."none_l_orderkey__c")
  INNER JOIN (
  SELECT "lineitem"."l_orderkey" AS "none_l_orderkey_ok",
    COUNT(DISTINCT "lineitem"."l_suppkey") AS "x_measure__1"
  FROM "lineitem" "lineitem"
  GROUP BY 1
  HAVING (COUNT(DISTINCT "lineitem"."l_suppkey") > 1)
) "t1" ON ("lineitem"."l_orderkey" = "t1"."none_l_orderkey_ok")
WHERE (("lineitem"."l_commitdate" < "lineitem"."l_receiptdate") AND ("n2"."n_name" = 'SAUDI ARABIA') AND ("orders"."o_orderstatus" = 'F'))
GROUP BY 1
ORDER BY "sum_number_of_reco" DESC,
  "s_name" ASC
LIMIT 100;


