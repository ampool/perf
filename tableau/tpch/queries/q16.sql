-- Query 16.1
SELECT "part"."p_brand" AS "p_brand"
FROM "part" "part"
GROUP BY 1
ORDER BY "p_brand" ASC;

-- Query 16.2
SELECT "supplier"."s_suppkey" AS "s_suppkey"
FROM "supplier" "supplier"
WHERE ("supplier"."s_suppkey" IS NULL)
LIMIT 1;

-- Query 16.3
SELECT COUNT(DISTINCT "partsupp"."ps_suppkey") AS "ctd_ps_suppkey_ok",
  "part"."p_brand" AS "p_brand",
  "part"."p_size" AS "p_size",
  "part"."p_type" AS "p_type"
FROM "partsupp" "partsupp"
  INNER JOIN "part" "part" ON ("partsupp"."ps_partkey" = "part"."p_partkey")
  INNER JOIN "supplier" "supplier" ON ("partsupp"."ps_suppkey" = "supplier"."s_suppkey")
  INNER JOIN (
  SELECT "supplier"."s_suppkey" AS "s_suppkey",
    MAX((CASE WHEN (REGEXP_LIKE("supplier"."s_comment", CONCAT('.*', 'Customer', '.*')) AND (CASE
  WHEN STRPOS( "supplier"."s_comment", 'Customer' )
   < 1 THEN STRPOS( "supplier"."s_comment", 'Complaints' )
  WHEN STRPOS( SUBSTR("supplier"."s_comment", STRPOS( "supplier"."s_comment", 'Customer' )
  ), 'Complaints' ) = 0 THEN 0
  ELSE STRPOS( SUBSTR("supplier"."s_comment", STRPOS( "supplier"."s_comment", 'Customer' )
  ), 'Complaints' ) + STRPOS( "supplier"."s_comment", 'Customer' )
   - 1
  END
   > 0)) THEN 1 ELSE 0 END)) AS "x_measure__1"
  FROM "partsupp" "partsupp"
    INNER JOIN "supplier" "supplier" ON ("partsupp"."ps_suppkey" = "supplier"."s_suppkey")
  GROUP BY 1
  HAVING (MAX((CASE WHEN (REGEXP_LIKE("supplier"."s_comment", CONCAT('.*', 'Customer', '.*')) AND (CASE
  WHEN STRPOS( "supplier"."s_comment", 'Customer' )
   < 1 THEN STRPOS( "supplier"."s_comment", 'Complaints' )
  WHEN STRPOS( SUBSTR("supplier"."s_comment", STRPOS( "supplier"."s_comment", 'Customer' )
  ), 'Complaints' ) = 0 THEN 0
  ELSE STRPOS( SUBSTR("supplier"."s_comment", STRPOS( "supplier"."s_comment", 'Customer' )
  ), 'Complaints' ) + STRPOS( "supplier"."s_comment", 'Customer' )
   - 1
  END
   > 0)) THEN 1 ELSE 0 END)) = 0)
) "t0" ON ("supplier"."s_suppkey" = "t0"."s_suppkey")
WHERE (("part"."p_brand" <> 'Brand#45') AND ("part"."p_size" IN (3, 9, 14, 19, 23, 36, 45, 49)) AND (("part"."p_type" IS NULL) OR (NOT
(CASE WHEN (1 IS NULL) OR (LENGTH('medium polished') IS NULL) THEN CAST(NULL AS VARCHAR)
      WHEN LENGTH('medium polished') < 1 THEN ''
      WHEN 1 < 1 THEN SUBSTR(LOWER("part"."p_type"),CAST(1 AS BIGINT),CAST(LENGTH('medium polished') AS BIGINT))
      ELSE SUBSTR(LOWER("part"."p_type"),CAST(1 AS BIGINT),CAST(LENGTH('medium polished') AS BIGINT)) END) = 'medium polished')))
GROUP BY 2,
  3,
  4;

