--query 18.1
SELECT MIN("t0"."sum_l_quantity_qk") AS "temp_sum_l_quantit",
  MAX("t0"."sum_l_quantity_qk") AS "temp_sum_l_quanti1",
  COUNT(1) AS "x__alias__0"
FROM (
  SELECT SUM("dnm"."l_quantity") AS "sum_l_quantity_qk"
  FROM "tpch_min_d" "dnm"
  GROUP BY "dnm"."c_custkey",
    "dnm"."c_name",
    "dnm"."o_orderdate",
    "dnm"."o_orderkey",
    "dnm"."o_totalprice"
) "t0"
HAVING (COUNT(1) > 0);


-- query 18.2
SELECT "dnm"."c_custkey" AS "c_custkey",
  "dnm"."c_name" AS "c_name",
  "dnm"."o_totalprice" AS "min_o_totalprice_o",
  "dnm"."o_orderdate" AS "o_orderdate",
  "dnm"."o_orderkey" AS "o_orderkey",
  "dnm"."o_totalprice" AS "o_totalprice",
  SUM("dnm"."l_quantity") AS "sum_l_quantity_ok"
FROM "tpch_min_d" "dnm"
GROUP BY 1,
  2,
  4,
  5,
  6
HAVING (SUM("dnm"."l_quantity") >= 300.99999999999699);

