-- query 22.1
SELECT CASE WHEN 2 >= 0 THEN SUBSTR("customer"."c_phone",1,CAST(2 AS BIGINT)) ELSE CAST(NULL AS VARCHAR) END AS "country_code"
FROM "customer" "customer"
GROUP BY 1
ORDER BY "country_code" ASC;

-- query 22.2
SELECT CASE WHEN 2 >= 0 THEN SUBSTR("customer"."c_phone",1,CAST(2 AS BIGINT)) ELSE CAST(NULL AS VARCHAR) END AS "country_code",
  COUNT("customer"."c_custkey") AS "cnt_c_custkey_ok",
  SUM("customer"."c_acctbal") AS "sum_c_acctbal_ok"
FROM "customer" "customer"
  LEFT JOIN "orders" "orders" ON ("customer"."c_custkey" = "orders"."o_custkey")
  INNER JOIN (
  SELECT AVG("t0"."x_measure__1") AS "x_measure__2",
    COUNT(1) AS "x__alias__0"
  FROM (
    SELECT MAX("customer"."c_acctbal") AS "x_measure__1"
    FROM "customer" "customer"
    WHERE ((CASE WHEN 2 >= 0 THEN SUBSTR("customer"."c_phone",1,CAST(2 AS BIGINT)) ELSE CAST(NULL AS VARCHAR) END IN ('13', '17', '18', '23', '29', '30', '31')) AND ("customer"."c_acctbal" >= 0.))
    GROUP BY "customer"."c_custkey"
  ) "t0"
  HAVING (COUNT(1) > 0)
) "t1" ON 1=1
WHERE (((CASE WHEN 2 >= 0 THEN SUBSTR("customer"."c_phone",1,CAST(2 AS BIGINT)) ELSE CAST(NULL AS VARCHAR) END IN ('13', '17', '18', '23', '29', '30', '31')) AND ("customer"."c_acctbal" >= 0.) AND ("orders"."o_orderkey" IS NULL)) AND ("customer"."c_acctbal" > "t1"."x_measure__2"))
GROUP BY 1;

