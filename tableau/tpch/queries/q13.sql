-- Query 13.1
SELECT "t0"."x_measure__1" AS "customers_per_orde",
  COUNT(DISTINCT "customer"."c_custkey") AS "ctd_c_custkey_ok"
FROM "customer" "customer"
  INNER JOIN (
  SELECT "customer"."c_custkey" AS "c_custkey",
    COUNT((CASE WHEN (NOT (REGEXP_LIKE("orders"."o_comment", CONCAT('.*', 'special', '.*')) AND (CASE
  WHEN STRPOS( "orders"."o_comment", 'special' )
   < 1 THEN STRPOS( "orders"."o_comment", 'requests' )
  WHEN STRPOS( SUBSTR("orders"."o_comment", STRPOS( "orders"."o_comment", 'special' )
  ), 'requests' ) = 0 THEN 0
  ELSE STRPOS( SUBSTR("orders"."o_comment", STRPOS( "orders"."o_comment", 'special' )
  ), 'requests' ) + STRPOS( "orders"."o_comment", 'special' )
   - 1
  END
   > 0))) THEN "orders"."o_orderkey" ELSE CAST(NULL AS BIGINT) END)) AS "x_measure__1"
  FROM "customer" "customer"
    LEFT JOIN "orders" "orders" ON ("customer"."c_custkey" = "orders"."o_custkey")
  GROUP BY 1
) "t0" ON ((COALESCE("customer"."c_custkey", 0) = COALESCE("t0"."c_custkey", 0)) AND (COALESCE("customer"."c_custkey", 1) = COALESCE("t0"."c_custkey", 1)))
GROUP BY 1;

-- Query 13.2
SELECT "t0"."x_measure__1" AS "customers_per_orde",
  COUNT(DISTINCT "customer"."c_custkey") AS "ctd_c_custkey_ok"
FROM "customer" "customer"
  INNER JOIN (
  SELECT "customer"."c_custkey" AS "c_custkey",
    COUNT((CASE WHEN (NOT (REGEXP_LIKE("orders"."o_comment", CONCAT('.*', 'special', '.*')) AND (CASE
  WHEN STRPOS( "orders"."o_comment", 'special' )
   < 1 THEN STRPOS( "orders"."o_comment", 'requests' )
  WHEN STRPOS( SUBSTR("orders"."o_comment", STRPOS( "orders"."o_comment", 'special' )
  ), 'requests' ) = 0 THEN 0
  ELSE STRPOS( SUBSTR("orders"."o_comment", STRPOS( "orders"."o_comment", 'special' )
  ), 'requests' ) + STRPOS( "orders"."o_comment", 'special' )
   - 1
  END
   > 0))) THEN "orders"."o_orderkey" ELSE CAST(NULL AS BIGINT) END)) AS "x_measure__1"
  FROM "orders" "orders"
    RIGHT JOIN "customer" "customer" ON ("orders"."o_custkey" = "customer"."c_custkey")
  GROUP BY 1
) "t0" ON ((COALESCE("customer"."c_custkey", 0) = COALESCE("t0"."c_custkey", 0)) AND (COALESCE("customer"."c_custkey", 1) = COALESCE("t0"."c_custkey", 1)))
GROUP BY 1;

