-- query 19.1
SELECT "lineitem"."l_shipinstruct" AS "l_shipinstruct"
FROM "lineitem" "lineitem"
GROUP BY 1
ORDER BY "l_shipinstruct" ASC;

-- query 19.2
SELECT SUM(("lineitem"."l_extendedprice" * (1 - "lineitem"."l_discount"))) AS "sum_disc_price_ok",
  COUNT(1) AS "x__alias__0"
FROM "lineitem" "lineitem"
  INNER JOIN "partsupp" "partsupp" ON (("lineitem"."l_partkey" = "partsupp"."ps_partkey") AND ("lineitem"."l_suppkey" = "partsupp"."ps_suppkey"))
  INNER JOIN "part" "part" ON ("partsupp"."ps_partkey" = "part"."p_partkey")
WHERE (((((CASE WHEN ("part"."p_container" IN ('SM BOX', 'SM CASE', 'SM PACK', 'SM PKG')) THEN 1 ELSE 0 END) = 1) AND ("part"."p_brand" = 'Brand#12') AND ("lineitem"."l_quantity" >= 1) AND ("lineitem"."l_quantity" <= 11) AND ("part"."p_size" >= 1) AND ("part"."p_size" <= 5)) OR (((CASE WHEN ("part"."p_container" IN ('MED BAG', 'MED BOX', 'MED PACK', 'MED PKG')) THEN 1 ELSE 0 END) = 1) AND ("part"."p_brand" = 'Brand#23') AND ("lineitem"."l_quantity" >= 10) AND ("lineitem"."l_quantity" <= 20) AND ("part"."p_size" >= 1) AND ("part"."p_size" <= 10)) OR (((CASE WHEN ("part"."p_container" IN ('LG BOX', 'LG CASE', 'LG PACK', 'LG PKG')) THEN 1 ELSE 0 END) = 1) AND ("part"."p_brand" = 'Brand#34') AND ("lineitem"."l_quantity" >= 20) AND ("lineitem"."l_quantity" <= 30) AND ("part"."p_size" >= 1) AND ("part"."p_size" <= 15))) AND ("lineitem"."l_shipinstruct" = 'DELIVER IN PERSON') AND ("lineitem"."l_shipmode" = 'AIR'))
HAVING (COUNT(1) > 0);

