-- Query 9.1
SELECT "part"."p_type" AS "p_type"
FROM "part" "part"
GROUP BY 1
ORDER BY "p_type" ASC;

-- Query 9.2
SELECT "dnm"."n_name" AS "n_name",
  SUM(("dnm"."l_extendedprice" * (1 - "dnm"."l_discount"))) AS "sum_disc_price_ok",
  YEAR("dnm"."o_orderdate") AS "yr_o_orderdate_ok"
FROM "tpch_min_d" "dnm"
WHERE (("dnm"."o_orderdate" >= CAST('1995-01-01' AS DATE)) AND ("dnm"."o_orderdate" <= CAST('1996-12-31' AS DATE)) AND ("dnm"."p_type" = 'ECONOMY ANODIZED STEEL') AND ("dnm"."r_name" = 'AMERICA'))
GROUP BY 1,
  3;


-- Query 9.3
SELECT "dnm"."n_name" AS "n_name",
  SUM((("dnm"."l_extendedprice" * (1 - "dnm"."l_discount")) - ("dnm"."ps_supplycost" * "dnm"."l_quantity"))) AS "sum_amount_ok",
  YEAR("dnm"."o_orderdate") AS "yr_o_orderdate_ok"
FROM "tpch_min_d" "dnm"
WHERE REGEXP_LIKE(LOWER("dnm"."p_name"), CONCAT('.*', 'green', '.*'))
GROUP BY 1,
  3;
