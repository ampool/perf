-- Query 6.1
SELECT MIN("lineitem"."l_discount") AS "temp_none_l_discou",
  MAX("lineitem"."l_discount") AS "temp_none_l_disco1",
  COUNT(1) AS "x__alias__0"
FROM "lineitem" "lineitem"
HAVING (COUNT(1) > 0);

--query 6.2
SELECT MIN("lineitem"."l_quantity") AS "temp_none_l_quanti",
  MAX("lineitem"."l_quantity") AS "temp_none_l_quant1",
  COUNT(1) AS "x__alias__0"
FROM "lineitem" "lineitem"
HAVING (COUNT(1) > 0);

--query 6.3
SELECT SUM(("lineitem"."l_extendedprice" * "lineitem"."l_discount")) AS "sum_revenue_change",
  COUNT(1) AS "x__alias__0"
FROM "lineitem" "lineitem"
WHERE (("lineitem"."l_discount" >= 0.050000000000000003) AND ("lineitem"."l_discount" <= 0.070000000000000007) AND ("lineitem"."l_quantity" <= 23.) AND ("lineitem"."l_shipdate" >= CAST('1994-01-01 00:00:00' AS TIMESTAMP)) AND ("lineitem"."l_shipdate" < CAST('1995-01-01 00:00:00' AS TIMESTAMP)))
HAVING (COUNT(1) > 0);

