-- Query 14
SELECT ((CASE WHEN
(CASE WHEN (1 IS NULL) OR (LENGTH('PROMO') IS NULL) THEN CAST(NULL AS VARCHAR)
      WHEN LENGTH('PROMO') < 1 THEN ''
      WHEN 1 < 1 THEN SUBSTR("part"."p_type",CAST(1 AS BIGINT),CAST(LENGTH('PROMO') AS BIGINT))
      ELSE SUBSTR("part"."p_type",CAST(1 AS BIGINT),CAST(LENGTH('PROMO') AS BIGINT)) END) = 'PROMO' THEN 1 ELSE 0 END) = 1) AS "io_promo_type_nk",
  SUM(("lineitem"."l_extendedprice" * (1 - "lineitem"."l_discount"))) AS "sum_disc_price_ok"
FROM "lineitem" "lineitem"
  INNER JOIN "partsupp" "partsupp" ON (("lineitem"."l_partkey" = "partsupp"."ps_partkey") AND ("lineitem"."l_suppkey" = "partsupp"."ps_suppkey"))
  INNER JOIN "part" "part" ON ("partsupp"."ps_partkey" = "part"."p_partkey")
WHERE (("lineitem"."l_shipdate" >= CAST('1995-09-01 00:00:00' AS TIMESTAMP)) AND ("lineitem"."l_shipdate" < CAST('1995-10-01 00:00:00' AS TIMESTAMP)))
GROUP BY 1;

