-- Query 11.1
SELECT "nation"."n_name" AS "n_name"
FROM "nation" "nation"
GROUP BY 1
ORDER BY "n_name" ASC;

-- Query 11.2
SELECT "partsupp"."ps_partkey" AS "ps_partkey",
  SUM(("partsupp"."ps_supplycost" * "partsupp"."ps_availqty")) AS "sum_value_ok"
FROM "partsupp" "partsupp"
  INNER JOIN "supplier" "supplier" ON ("partsupp"."ps_suppkey" = "supplier"."s_suppkey")
  INNER JOIN "nation" "nation" ON ("supplier"."s_nationkey" = "nation"."n_nationkey")
WHERE ("nation"."n_name" = 'GERMANY')
GROUP BY 1;


