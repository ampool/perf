-- Query 3.1
SELECT "customer"."c_mktsegment" AS "c_mktsegment"
FROM "customer" "customer"
GROUP BY 1
ORDER BY "c_mktsegment" ASC;

-- Query 3.2
SELECT "orders"."o_orderdate" AS "o_orderdate",
  "orders"."o_orderkey" AS "o_orderkey",
  "orders"."o_shippriority" AS "o_shippriority",
  SUM(("lineitem"."l_extendedprice" * (1 - "lineitem"."l_discount"))) AS "sum_disc_price_ok"
FROM "lineitem" "lineitem"
  INNER JOIN "orders" "orders" ON ("lineitem"."l_orderkey" = "orders"."o_orderkey")
  INNER JOIN "customer" "customer" ON ("orders"."o_custkey" = "customer"."c_custkey")
  INNER JOIN (
  SELECT "orders"."o_orderkey" AS "o_orderkey",
    SUM(("lineitem"."l_extendedprice" * (1 - "lineitem"."l_discount"))) AS "x__alias__0"
  FROM "lineitem" "lineitem"
    INNER JOIN "orders" "orders" ON ("lineitem"."l_orderkey" = "orders"."o_orderkey")
    INNER JOIN "customer" "customer" ON ("orders"."o_custkey" = "customer"."c_custkey")
  WHERE (("customer"."c_mktsegment" = 'BUILDING') AND ("lineitem"."l_shipdate" > CAST('1995-03-15' AS DATE)) AND ("orders"."o_orderdate" < CAST('1995-03-15' AS DATE)))
  GROUP BY 1
  ORDER BY "x__alias__0" DESC,
    "o_orderkey" ASC
  LIMIT 10
) "t0" ON ("orders"."o_orderkey" = "t0"."o_orderkey")
WHERE (("orders"."o_orderdate" < CAST('1995-03-15' AS DATE)) AND ("customer"."c_mktsegment" = 'BUILDING') AND ("lineitem"."l_shipdate" > CAST('1995-03-15' AS DATE)))
GROUP BY 1,
  2,
  3;
