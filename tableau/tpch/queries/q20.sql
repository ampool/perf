-- query 20.1
SELECT "n2"."n_name" AS "n_name"
FROM "nation" "n2"
GROUP BY 1
ORDER BY "n_name" ASC;

--query 20.2
SELECT "supplier"."s_address" AS "s_address",
  "supplier"."s_name" AS "s_name"
FROM "partsupp" "partsupp"
  INNER JOIN "supplier" "supplier" ON ("partsupp"."ps_suppkey" = "supplier"."s_suppkey")
  INNER JOIN "nation" "n2" ON ("supplier"."s_nationkey" = "n2"."n_nationkey")
  INNER JOIN "part" "part" ON ("partsupp"."ps_partkey" = "part"."p_partkey")
WHERE (("n2"."n_name" = 'CANADA') AND 
(CASE WHEN (1 IS NULL) OR (LENGTH('forest') IS NULL) THEN CAST(NULL AS VARCHAR)
      WHEN LENGTH('forest') < 1 THEN ''
      WHEN 1 < 1 THEN SUBSTR(LOWER("part"."p_name"),CAST(1 AS BIGINT),CAST(LENGTH('forest') AS BIGINT))
      ELSE SUBSTR(LOWER("part"."p_name"),CAST(1 AS BIGINT),CAST(LENGTH('forest') AS BIGINT)) END) = 'forest')
GROUP BY 1,
  2;


