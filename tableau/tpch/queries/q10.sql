-- Query 10
SELECT "customer"."c_acctbal" AS "c_acctbal",
  "customer"."c_address" AS "c_address",
  "customer"."c_comment" AS "c_comment",
  "customer"."c_custkey" AS "c_custkey",
  "customer"."c_name" AS "c_name",
  "customer"."c_phone" AS "c_phone",
  "n1"."n_name" AS "n_name__nation1_",
  SUM(("lineitem"."l_extendedprice" * (1 - "lineitem"."l_discount"))) AS "sum_disc_price_ok"
FROM "lineitem" "lineitem"
  INNER JOIN "orders" "orders" ON ("lineitem"."l_orderkey" = "orders"."o_orderkey")
  INNER JOIN "customer" "customer" ON ("orders"."o_custkey" = "customer"."c_custkey")
  INNER JOIN "nation" "n1" ON ("customer"."c_nationkey" = "n1"."n_nationkey")
  INNER JOIN (
  SELECT "customer"."c_custkey" AS "c_custkey",
    SUM(("lineitem"."l_extendedprice" * (1 - "lineitem"."l_discount"))) AS "x__alias__0"
  FROM "lineitem" "lineitem"
    INNER JOIN "orders" "orders" ON ("lineitem"."l_orderkey" = "orders"."o_orderkey")
    INNER JOIN "customer" "customer" ON ("orders"."o_custkey" = "customer"."c_custkey")
  WHERE (("lineitem"."l_returnflag" = 'R') AND ("orders"."o_orderdate" >= CAST('1993-10-01 00:00:00' AS TIMESTAMP)) AND ("orders"."o_orderdate" < CAST('1994-01-01 00:00:00' AS TIMESTAMP)))
  GROUP BY 1
  ORDER BY "x__alias__0" DESC,
    "c_custkey" ASC
  LIMIT 20
) "t0" ON ("customer"."c_custkey" = "t0"."c_custkey")
WHERE (("lineitem"."l_returnflag" = 'R') AND ("orders"."o_orderdate" >= CAST('1993-10-01 00:00:00' AS TIMESTAMP)) AND ("orders"."o_orderdate" < CAST('1994-01-01 00:00:00' AS TIMESTAMP)))
GROUP BY 1,
  2,
  3,
  4,
  5,
  6,
  7;

