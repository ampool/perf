 CREATE TABLE `tpch_min_d`(
  `c_name` string COMMENT 'from deserializer', 
  `c_custkey` bigint COMMENT 'from deserializer', 
  `c_nationkey` bigint COMMENT 'from deserializer', 
  `n_name` string COMMENT 'from deserializer', 
  `n_nationkey` bigint COMMENT 'from deserializer', 
  `r_name` string COMMENT 'from deserializer', 
  `p_name` string COMMENT 'from deserializer', 
  `p_type` string COMMENT 'from deserializer', 
  `p_partkey` bigint COMMENT 'from deserializer', 
  `ps_supplycost` double COMMENT 'from deserializer', 
  `ps_suppkey` bigint COMMENT 'from deserializer', 
  `s_name` string COMMENT 'from deserializer', 
  `s_nationkey` bigint COMMENT 'from deserializer', 
  `s_nationname` string COMMENT 'from deserializer', 
  `o_orderkey` bigint COMMENT 'from deserializer', 
  `o_orderdate` date COMMENT 'from deserializer', 
  `o_totalprice` double COMMENT 'from deserializer', 
  `o_orderstatus` string COMMENT 'from deserializer', 
  `o_comment` string COMMENT 'from deserializer', 
  `l_extendedprice` double COMMENT 'from deserializer', 
  `l_discount` double COMMENT 'from deserializer', 
  `l_quantity` double COMMENT 'from deserializer', 
  `l_shipdate` date COMMENT 'from deserializer', 
  `l_commitdate` date COMMENT 'from deserializer', 
  `l_receiptdate` date COMMENT 'from deserializer')
ROW FORMAT SERDE 
  'io.ampool.monarch.hive.MonarchSerDe' 
STORED BY 
  'io.ampool.monarch.hive.MonarchStorageHandler' 
WITH SERDEPROPERTIES ( 
  'serialization.format'='1')
TBLPROPERTIES (
  'ampool.locators'='ip-172-31-8-133.us-west-2.compute.internal[10334],ip-172-31-9-243.us-west-2.compute.internal[10334]', 
  'monarch.block.format'='AMP_COLUMNAR_OFF_HEAP', 
  'monarch.block.size'='10000', 
  'monarch.buckets'='96', 
  'monarch.column.transformations'=',,,,,,,,,,,,,,,,,,,,,,,,', 
  'monarch.load.query'='select c_name as c_name,c_custkey as c_custkey,c_nationkey as c_nationkey,n_name as n_name,n_nationkey as n_nationkey,r_name as r_name,p_name as p_name,p_type as p_type,p_partkey as p_partkey,ps_supplycost as ps_supplycost,ps_suppkey as ps_suppkey,s_name as s_name,s_nationkey as s_nationkey,s_nationname as s_nationname,o_orderkey as o_orderkey,o_orderdate as o_orderdate,o_totalprice as o_totalprice,o_orderstatus as o_orderstatus,o_comment as o_comment,l_extendedprice as l_extendedprice,l_discount as l_discount,l_quantity as l_quantity,l_shipdate as l_shipdate,l_commitdate as l_commitdate,l_receiptdate as l_receiptdate from tpchdatemod_flat_orc_10.tpch_min_d3', 
  'monarch.managed.type'='NON_PARTITION', 
  'monarch.query.tables'='tpch_min_d3', 
  'monarch.table.type'='IMMUTABLE', 
  'remote_data_source_id'='HDP_Source', 
  'remote_data_source_type'='DATA_LAKE', 
  'remote_database_name'='tpchdatemod_flat_orc_10', 
  'remote_table_name'='tpch_min_d3', 
  'sinktarget'='false', 
  'source.selected.columns.ordered'='c_name as c_name,c_custkey as c_custkey,c_nationkey as c_nationkey,n_name as n_name,n_nationkey as n_nationkey,r_name as r_name,p_name as p_name,p_type as p_type,p_partkey as p_partkey,ps_supplycost as ps_supplycost,ps_suppkey as ps_suppkey,s_name as s_name,s_nationkey as s_nationkey,s_nationname as s_nationname,o_orderkey as o_orderkey,o_orderdate as o_orderdate,o_totalprice as o_totalprice,o_orderstatus as o_orderstatus,o_comment as o_comment,l_extendedprice as l_extendedprice,l_discount as l_discount,l_quantity as l_quantity,l_shipdate as l_shipdate,l_commitdate as l_commitdate,l_receiptdate as l_receiptdate', 
  'source.table.columns'='c_name:string:::,c_custkey:bigint:::,c_nationkey:bigint:::,n_name:string:::,n_nationkey:bigint:::,r_name:string:::,p_name:string:::,p_type:string:::,p_partkey:bigint:::,ps_supplycost:double:::,ps_suppkey:bigint:::,s_name:string:::,s_nationkey:bigint:::,s_nationname:string:::,o_orderkey:bigint:::,o_orderdate:date:::,o_totalprice:double:::,o_orderstatus:string:::,o_comment:string:::,l_extendedprice:double:::,l_discount:double:::,l_quantity:double:::,l_shipdate:date:::,l_commitdate:date:::,l_receiptdate:date:::', 
  'monarch.partitioning.column'='c_custkey'
   
)

