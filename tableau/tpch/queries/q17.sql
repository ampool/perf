-- query 17.1
SELECT "part"."p_container" AS "p_container"
FROM "part" "part"
GROUP BY 1
ORDER BY "p_container" ASC;

-- query 17.2
SELECT SUM("lineitem"."l_extendedprice") AS "temp_average_yearl",
  COUNT(1) AS "x__alias__0"
FROM "lineitem" "lineitem"
  INNER JOIN "partsupp" "partsupp" ON (("lineitem"."l_partkey" = "partsupp"."ps_partkey") AND ("lineitem"."l_suppkey" = "partsupp"."ps_suppkey"))
  INNER JOIN "part" "part" ON ("partsupp"."ps_partkey" = "part"."p_partkey")
  INNER JOIN (
  SELECT "lineitem"."l_partkey" AS "l_partkey",
    AVG("lineitem"."l_quantity") AS "x_measure__0"
  FROM "lineitem" "lineitem"
  GROUP BY 1
) "t0" ON ("lineitem"."l_partkey" = "t0"."l_partkey")
WHERE ((("part"."p_brand" = 'Brand#23') AND ("part"."p_container" = 'MED BOX')) AND ((0.20000000000000001 * "t0"."x_measure__0") > "lineitem"."l_quantity"))
HAVING (COUNT(1) > 0);

