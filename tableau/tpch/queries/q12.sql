-- Query 12.1
SELECT "lineitem"."l_shipmode" AS "l_shipmode"
FROM "lineitem" "lineitem"
GROUP BY 1
ORDER BY "l_shipmode" ASC;

-- Query 12.2
SELECT (CASE WHEN ("orders"."o_orderpriority" IN ('1-URGENT', '1-URGENT       ', '2-HIGH', '2-HIGH         ')) THEN 'High' WHEN ("orders"."o_orderpriority" IN ('3-MEDIUM', '3-MEDIUM       ', '4-NOT SPECIFIED', '5-LOW', '5-LOW          ')) THEN 'Low' ELSE "orders"."o_orderpriority" END) AS "o_order_priority__",
  "lineitem"."l_shipmode" AS "l_shipmode",
  SUM(1) AS "sum_number_of_reco"
FROM "lineitem" "lineitem"
  INNER JOIN "orders" "orders" ON ("lineitem"."l_orderkey" = "orders"."o_orderkey")
WHERE (("lineitem"."l_shipmode" IN ('MAIL', 'SHIP')) AND ("lineitem"."l_commitdate" < "lineitem"."l_receiptdate") AND ("lineitem"."l_shipdate" < "lineitem"."l_commitdate") AND ("lineitem"."l_receiptdate" >= CAST('1994-01-01 00:00:00' AS TIMESTAMP)) AND ("lineitem"."l_receiptdate" < CAST('1995-01-01 00:00:00' AS TIMESTAMP)))
GROUP BY 1,
  2;
