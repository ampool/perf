--query 18.1
SELECT MIN("t0"."sum_l_quantity_qk") AS "temp_sum_l_quantit",
  MAX("t0"."sum_l_quantity_qk") AS "temp_sum_l_quanti1",
  COUNT(1) AS "x__alias__0"
FROM (
  SELECT SUM("lineitem"."l_quantity") AS "sum_l_quantity_qk"
  FROM "lineitem" "lineitem"
    INNER JOIN "orders" "orders" ON ("lineitem"."l_orderkey" = "orders"."o_orderkey")
    INNER JOIN "customer" "customer" ON ("orders"."o_custkey" = "customer"."c_custkey")
  GROUP BY "customer"."c_custkey",
    "customer"."c_name",
    "orders"."o_orderdate",
    "orders"."o_orderkey",
    "orders"."o_totalprice"
) "t0"
HAVING (COUNT(1) > 0);

-- query 18.2
SELECT "customer"."c_custkey" AS "c_custkey",
  "customer"."c_name" AS "c_name",
  "orders"."o_totalprice" AS "min_o_totalprice_o",
  "orders"."o_orderdate" AS "o_orderdate",
  "orders"."o_orderkey" AS "o_orderkey",
  "orders"."o_totalprice" AS "o_totalprice",
  SUM("lineitem"."l_quantity") AS "sum_l_quantity_ok"
FROM "lineitem" "lineitem"
  INNER JOIN "orders" "orders" ON ("lineitem"."l_orderkey" = "orders"."o_orderkey")
  INNER JOIN "customer" "customer" ON ("orders"."o_custkey" = "customer"."c_custkey")
GROUP BY 1,
  2,
  4,
  5,
  6
HAVING (SUM("lineitem"."l_quantity") >= 300.99999999999699);

