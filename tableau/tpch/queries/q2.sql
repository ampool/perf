-- Query 2.1
SELECT "region"."r_name" AS "r_name"
FROM "region" "region"
GROUP BY 1
ORDER BY "r_name" ASC;

-- Query 2.2
SELECT "nation"."n_name" AS "n_name",
  "part"."p_mfgr" AS "p_mfgr",
  "partsupp"."ps_partkey" AS "ps_partkey",
  "partsupp"."ps_suppkey" AS "ps_suppkey",
  "supplier"."s_address" AS "s_address",
  "supplier"."s_comment" AS "s_comment",
  "supplier"."s_name" AS "s_name",
  "supplier"."s_phone" AS "s_phone",
  "supplier"."s_acctbal" AS "sum_s_acctbal_ok"
FROM "partsupp" "partsupp"
  INNER JOIN "part" "part" ON ("partsupp"."ps_partkey" = "part"."p_partkey")
  INNER JOIN "supplier" "supplier" ON ("partsupp"."ps_suppkey" = "supplier"."s_suppkey")
  INNER JOIN "nation" "nation" ON ("supplier"."s_nationkey" = "nation"."n_nationkey")
  INNER JOIN "region" "region" ON ("nation"."n_regionkey" = "region"."r_regionkey")
  INNER JOIN (
  SELECT "part"."p_partkey" AS "p_partkey",
    MIN("partsupp"."ps_supplycost") AS "x_measure__0"
  FROM "partsupp" "partsupp"
    INNER JOIN "part" "part" ON ("partsupp"."ps_partkey" = "part"."p_partkey")
    INNER JOIN "supplier" "supplier" ON ("partsupp"."ps_suppkey" = "supplier"."s_suppkey")
    INNER JOIN "nation" "nation" ON ("supplier"."s_nationkey" = "nation"."n_nationkey")
    INNER JOIN "region" "region" ON ("nation"."n_regionkey" = "region"."r_regionkey")
  WHERE ("region"."r_name" = 'EUROPE')
  GROUP BY 1
) "t0" ON ("part"."p_partkey" = "t0"."p_partkey")
WHERE ((("region"."r_name" = 'EUROPE') AND ("part"."p_size" = 15) AND
(CASE WHEN ((CASE WHEN (LENGTH(LOWER("part"."p_type")) - LENGTH('brass')) < 0 THEN 1
      ELSE LENGTH(LOWER("part"."p_type")) - LENGTH('brass') + 1 END)
 IS NULL) OR (LENGTH('brass') IS NULL) THEN CAST(NULL AS VARCHAR)
      WHEN LENGTH('brass') < 1 THEN ''
      WHEN (CASE WHEN (LENGTH(LOWER("part"."p_type")) - LENGTH('brass')) < 0 THEN 1
      ELSE LENGTH(LOWER("part"."p_type")) - LENGTH('brass') + 1 END)
 < 1 THEN SUBSTR(RTRIM(LOWER("part"."p_type")),CAST(1 AS BIGINT),CAST(LENGTH('brass') AS BIGINT))
      ELSE SUBSTR(RTRIM(LOWER("part"."p_type")),CAST((CASE WHEN (LENGTH(LOWER("part"."p_type")) - LENGTH('brass')) < 0 THEN 1
      ELSE LENGTH(LOWER("part"."p_type")) - LENGTH('brass') + 1 END)
 AS BIGINT),CAST(LENGTH('brass') AS BIGINT)) END) = 'brass') AND ("partsupp"."ps_supplycost" = "t0"."x_measure__0"));

