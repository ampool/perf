-- Query 9.1
SELECT "part"."p_type" AS "p_type"
FROM "part" "part"
GROUP BY 1
ORDER BY "p_type" ASC;

-- Query 9.2
SELECT "n2"."n_name" AS "n_name",
  SUM(("lineitem"."l_extendedprice" * (1 - "lineitem"."l_discount"))) AS "sum_disc_price_ok",
  YEAR("orders"."o_orderdate") AS "yr_o_orderdate_ok"
FROM "lineitem" "lineitem"
  INNER JOIN "orders" "orders" ON ("lineitem"."l_orderkey" = "orders"."o_orderkey")
  INNER JOIN "customer" "customer" ON ("orders"."o_custkey" = "customer"."c_custkey")
  INNER JOIN "nation" "n1" ON ("customer"."c_nationkey" = "n1"."n_nationkey")
  INNER JOIN "region" "region" ON ("n1"."n_regionkey" = "region"."r_regionkey")
  INNER JOIN "partsupp" "partsupp" ON (("lineitem"."l_partkey" = "partsupp"."ps_partkey") AND ("lineitem"."l_suppkey" = "partsupp"."ps_suppkey"))
  INNER JOIN "supplier" "supplier" ON ("partsupp"."ps_suppkey" = "supplier"."s_suppkey")
  INNER JOIN "nation" "n2" ON ("supplier"."s_nationkey" = "n2"."n_nationkey")
  INNER JOIN "part" "part" ON ("partsupp"."ps_partkey" = "part"."p_partkey")
WHERE (("orders"."o_orderdate" >= CAST('1995-01-01' AS DATE)) AND ("orders"."o_orderdate" <= CAST('1996-12-31' AS DATE)) AND ("part"."p_type" = 'ECONOMY ANODIZED STEEL') AND ("region"."r_name" = 'AMERICA'))
GROUP BY 1,
  3;

-- Query 9.3
SELECT "n2"."n_name" AS "n_name",
  SUM((("lineitem"."l_extendedprice" * (1 - "lineitem"."l_discount")) - ("partsupp"."ps_supplycost" * "lineitem"."l_quantity"))) AS "sum_amount_ok",
  YEAR("orders"."o_orderdate") AS "yr_o_orderdate_ok"
FROM "lineitem" "lineitem"
  INNER JOIN "orders" "orders" ON ("lineitem"."l_orderkey" = "orders"."o_orderkey")
  INNER JOIN "partsupp" "partsupp" ON (("lineitem"."l_partkey" = "partsupp"."ps_partkey") AND ("lineitem"."l_suppkey" = "partsupp"."ps_suppkey"))
  INNER JOIN "supplier" "supplier" ON ("partsupp"."ps_suppkey" = "supplier"."s_suppkey")
  INNER JOIN "nation" "n2" ON ("supplier"."s_nationkey" = "n2"."n_nationkey")
  INNER JOIN "part" "part" ON ("partsupp"."ps_partkey" = "part"."p_partkey")
WHERE REGEXP_LIKE(LOWER("part"."p_name"), CONCAT('.*', 'green', '.*'))
GROUP BY 1,
  3;

