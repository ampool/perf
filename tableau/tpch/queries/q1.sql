-- query 1.1
SELECT AVG("lineitem"."l_discount") AS "avg_l_discount_ok",
  AVG("lineitem"."l_extendedprice") AS "avg_l_extendedpric",
  AVG("lineitem"."l_quantity") AS "avg_l_quantity_ok",
  "lineitem"."l_linestatus" AS "l_linestatus",
  "lineitem"."l_returnflag" AS "l_returnflag",
  SUM((("lineitem"."l_extendedprice" * (1 - "lineitem"."l_discount")) * (1 + "lineitem"."l_tax"))) AS "sum_charge_ok",
  SUM(("lineitem"."l_extendedprice" * (1 - "lineitem"."l_discount"))) AS "sum_disc_price_ok",
  SUM(1) AS "sum_number_of_reco",
  SUM("lineitem"."l_extendedprice") AS "sum_l_extendedpric",
  SUM("lineitem"."l_quantity") AS "sum_l_quantity_ok"
FROM "lineitem" "lineitem"
WHERE ("lineitem"."l_shipdate" <= CAST('1998-09-02 00:00:00' AS TIMESTAMP))
GROUP BY 4,
  5;

-- query 1.2
SELECT "part"."p_size" AS "p_size"
FROM "part" "part"
GROUP BY 1;

