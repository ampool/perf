-- Query 15
SELECT "lineitem"."l_suppkey" AS "l_suppkey",
  "supplier"."s_address" AS "s_address",
  "supplier"."s_name" AS "s_name",
  "supplier"."s_phone" AS "s_phone",
  SUM(("lineitem"."l_extendedprice" * (1 - "lineitem"."l_discount"))) AS "sum_disc_price_ok"
FROM "lineitem" "lineitem"
  INNER JOIN "partsupp" "partsupp" ON (("lineitem"."l_partkey" = "partsupp"."ps_partkey") AND ("lineitem"."l_suppkey" = "partsupp"."ps_suppkey"))
  INNER JOIN "supplier" "supplier" ON ("partsupp"."ps_suppkey" = "supplier"."s_suppkey")
  INNER JOIN (
  SELECT "lineitem"."l_suppkey" AS "l_suppkey",
    SUM(("lineitem"."l_extendedprice" * (1 - "lineitem"."l_discount"))) AS "x_measure__0"
  FROM "lineitem" "lineitem"
  WHERE (("lineitem"."l_shipdate" >= CAST('1996-01-01 00:00:00' AS TIMESTAMP)) AND ("lineitem"."l_shipdate" < CAST('1996-04-01 00:00:00' AS TIMESTAMP)))
  GROUP BY 1
) "t0" ON ("lineitem"."l_suppkey" = "t0"."l_suppkey")
  INNER JOIN (
  SELECT MAX("t1"."x_measure__0") AS "x_measure__2",
    COUNT(1) AS "x__alias__0"
  FROM (
    SELECT SUM(("lineitem"."l_extendedprice" * (1 - "lineitem"."l_discount"))) AS "x_measure__0"
    FROM "lineitem" "lineitem"
    WHERE (("lineitem"."l_shipdate" >= CAST('1996-01-01 00:00:00' AS TIMESTAMP)) AND ("lineitem"."l_shipdate" < CAST('1996-04-01 00:00:00' AS TIMESTAMP)))
    GROUP BY "lineitem"."l_suppkey"
  ) "t1"
  HAVING (COUNT(1) > 0)
) "t2" ON 1=1
WHERE ((("lineitem"."l_shipdate" >= CAST('1996-01-01 00:00:00' AS TIMESTAMP)) AND ("lineitem"."l_shipdate" < CAST('1996-04-01 00:00:00' AS TIMESTAMP))) AND (ROUND("t0"."x_measure__0",2) = ROUND("t2"."x_measure__2",2)))
GROUP BY 1,
  2,
  3,
  4;
