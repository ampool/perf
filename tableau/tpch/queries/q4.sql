-- Query 4
SELECT COUNT(DISTINCT "orders"."o_orderkey") AS "ctd_o_orderkey_ok",
  "orders"."o_orderpriority" AS "o_orderpriority"
FROM "lineitem" "lineitem"
  INNER JOIN "orders" "orders" ON ("lineitem"."l_orderkey" = "orders"."o_orderkey")
WHERE (("lineitem"."l_commitdate" < "lineitem"."l_receiptdate") AND ("orders"."o_orderdate" >= CAST('1993-07-01 00:00:00' AS TIMESTAMP)) AND ("orders"."o_orderdate" < CAST('1993-10-01 00:00:00' AS TIMESTAMP)))
GROUP BY 2;

