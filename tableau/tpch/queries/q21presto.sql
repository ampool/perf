-- query 21.2
SELECT "supplier"."name" AS "name"
FROM "lineitem" "lineitem"
  INNER JOIN "orders" "orders" ON ("lineitem"."orderkey" = "orders"."orderkey")
  INNER JOIN "partsupp" "partsupp" ON (("lineitem"."partkey" = "partsupp"."partkey") AND ("lineitem"."suppkey" = "partsupp"."suppkey"))
  INNER JOIN "supplier" "supplier" ON ("partsupp"."suppkey" = "supplier"."suppkey")
  INNER JOIN "nation" "n2" ON ("supplier"."nationkey" = "n2"."nationkey")
  INNER JOIN (
  SELECT "lineitem"."orderkey" AS "none_orderkey__c",
    COUNT(DISTINCT (CASE WHEN ("lineitem"."commitdate" < "lineitem"."receiptdate") THEN "lineitem"."suppkey" ELSE CAST(NULL AS BIGINT) END)) AS "x_measure__0"
  FROM "lineitem" "lineitem"
  GROUP BY 1
  HAVING (COUNT(DISTINCT (CASE WHEN ("lineitem"."commitdate" < "lineitem"."receiptdate") THEN "lineitem"."suppkey" ELSE CAST(NULL AS BIGINT) END)) = 1)
) "t0" ON ("lineitem"."orderkey" = "t0"."none_orderkey__c")
  INNER JOIN (
  SELECT "lineitem"."orderkey" AS "none_orderkey_ok",
    COUNT(DISTINCT "lineitem"."suppkey") AS "x_measure__1"
  FROM "lineitem" "lineitem"
  GROUP BY 1
  HAVING (COUNT(DISTINCT "lineitem"."suppkey") > 1)
) "t1" ON ("lineitem"."orderkey" = "t1"."none_orderkey_ok") 
WHERE (("lineitem"."commitdate" < "lineitem"."receiptdate") AND ("n2"."name" = 'SAUDI ARABIA') AND ("orders"."orderstatus" = 'F') AND ("supplier"."name" IS NULL))
LIMIT 1;

--query 21.2
SELECT "supplier"."name" AS "name",
  SUM(1) AS "sum_numbeof_reco"
FROM "lineitem" "lineitem"
  INNER JOIN "orders" "orders" ON ("lineitem"."orderkey" = "orders"."orderkey")
  INNER JOIN "partsupp" "partsupp" ON (("lineitem"."partkey" = "partsupp"."partkey") AND ("lineitem"."suppkey" = "partsupp"."suppkey"))
  INNER JOIN "supplier" "supplier" ON ("partsupp"."suppkey" = "supplier"."suppkey")
  INNER JOIN "nation" "n2" ON ("supplier"."nationkey" = "n2"."nationkey")
  INNER JOIN (
  SELECT "lineitem"."orderkey" AS "none_orderkey__c",
    COUNT(DISTINCT (CASE WHEN ("lineitem"."commitdate" < "lineitem"."receiptdate") THEN "lineitem"."suppkey" ELSE CAST(NULL AS BIGINT) END)) AS "x_measure__0"
  FROM "lineitem" "lineitem"
  GROUP BY 1
  HAVING (COUNT(DISTINCT (CASE WHEN ("lineitem"."commitdate" < "lineitem"."receiptdate") THEN "lineitem"."suppkey" ELSE CAST(NULL AS BIGINT) END)) = 1)
) "t0" ON ("lineitem"."orderkey" = "t0"."none_orderkey__c")
  INNER JOIN (
  SELECT "lineitem"."orderkey" AS "none_orderkey_ok",
    COUNT(DISTINCT "lineitem"."suppkey") AS "x_measure__1"
  FROM "lineitem" "lineitem"
  GROUP BY 1
  HAVING (COUNT(DISTINCT "lineitem"."suppkey") > 1)
) "t1" ON ("lineitem"."orderkey" = "t1"."none_orderkey_ok") 
WHERE (("lineitem"."commitdate" < "lineitem"."receiptdate") AND ("n2"."name" = 'SAUDI ARABIA') AND ("orders"."orderstatus" = 'F'))
GROUP BY 1
ORDER BY "sum_numbeof_reco" DESC,
  "name" ASC
LIMIT 100;

