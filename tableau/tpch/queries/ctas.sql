create table tpch_min_d as select customer.c_name as c_name, customer.c_custkey as c_custkey,customer.c_nationkey as c_nationkey, nation.n_name as n_name, nation.n_nationkey as n_nationkey, region.r_name as r_name, part.p_name as p_name, part.p_type as p_type, part.p_partkey as p_partkey, partsupp.ps_supplycost as ps_supplycost, partsupp.ps_suppkey as ps_suppkey, supplier.s_name as s_name, supplier.s_nationkey as s_nationkey,n2.n_name as s_nationname, orders.o_orderkey as o_orderkey, orders.o_orderdate as o_orderdate, orders.o_totalprice as o_totalprice, orders.o_orderstatus as o_orderstatus, orders.o_comment as o_comment, lineitem.l_extendedprice as l_extendedprice, lineitem.l_discount as l_discount, lineitem.l_quantity as l_quantity, lineitem.l_shipdate as l_shipdate, lineitem.l_commitdate as l_commitdate, lineitem.l_receiptdate as l_receiptdate
FROM lineitem lineitem
  INNER JOIN orders orders ON (lineitem.l_orderkey = orders.o_orderkey)
  INNER JOIN customer customer ON (orders.o_custkey = customer.c_custkey)
  INNER JOIN nation nation ON (customer.c_nationkey = nation.n_nationkey)
  INNER JOIN region region ON (nation.n_regionkey = region.r_regionkey)
  INNER JOIN partsupp partsupp ON ((lineitem.l_partkey = partsupp.ps_partkey) AND (lineitem.l_suppkey = partsupp.ps_suppkey))
  INNER JOIN supplier supplier ON (partsupp.ps_suppkey = supplier.s_suppkey)
  INNER JOIN nation n2 ON (supplier.s_nationkey = n2.n_nationkey)
  INNER JOIN part part ON (partsupp.ps_partkey = part.p_partkey);
