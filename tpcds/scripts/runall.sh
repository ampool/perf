#!/usr/bin/env bash
SERVERURL=34.214.160.71:9291
CATALOG=glue
SCHEMA=std_tpcds_bin_partitioned_orc_100
OUTDIR=/shankar/temp
TIMIMGFILE=$OUTDIR/glueqf10.timings
RESULTDIR=$OUTDIR/shankar/temp/resultsgluetpcdsqf10
QUERYDIR=/shankar/git/perf/tpcds/queries/prestotpcds
rm -rf $RESULTDIR
mkdir -p $RESULTDIR
bash -x ./runtpcdsqueries.sh $SERVERURL $CATALOG  $SCHEMA $QUERYDIR $RESULTDIR | xargs -r -0 echo > $TIMIMGFILE
sed -i 's/ Q/\nQ/g' $TIMIMGFILE
